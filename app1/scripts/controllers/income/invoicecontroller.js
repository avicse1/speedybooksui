'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:invoiceController
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
 

.controller("invoiceCtrl", ["$scope", "$window", "userServices", "$route", "$routeParams", "$modal", "$filter", "validator", "invoiceServices", "$cookieStore", "commonServices", "accountServices", "$location", "itemServices", "$ngBootbox", "taxRateServices", "dashboardServices", function($scope, $window, userServices, $route, $routeParams, $modal, $filter, validator, invoiceServices, $cookieStore, commonServices, accountServices, $location, itemServices, $ngBootbox, taxRateServices, dashboardServices) {
	$scope.selected = void 0;
    $scope.redundDetailShow = false;
    $scope.globals = $cookieStore.get('globals');
	$scope.users = {};
    $scope.items = {};
    $scope.item_list = {};
    $scope.module_name = 'invoice';

    $scope.searchKeywords = "";
    $scope.filteredStores = [];
    $scope.row = "";
    $scope.currentPage = 1;
    $scope.invoiceList = [];
    $scope.itemDetails = {};
    $scope.typeInvoices = {};
    $scope.invoiceData = {};

    $scope.param_id = $location.search().id;
    $('.username .text').text($scope.globals.currentUser.username);

    if($scope.param_id != undefined && $scope.param_id != null && $scope.param_id != '') {
         invoiceServices.getInvoiceDetails($scope.param_id).then(function(res){
            $scope.invoiceData = res.response.businessInvoices;
        });
    }

    $scope.getInvoiceList = function() {
        invoiceServices.getInvoiceList().then(function(res){
            $scope.invoiceList = res.response.businessInvoices;
        });
    }

    $scope.getTypeInvoices = function (type) {
        invoiceServices.getTypeInvoices(type).then(function (res) {
            $scope.typeInvoices = res.response.businessInvoices;
            $scope.search();
            $scope.select($scope.currentPage);
        })
    }

    $scope.getUpcomingPayment = function () {
        invoiceServices.getUpcomingPayment().then(function (res) {
            $scope.typeInvoices = res.response.invoices;
            $scope.search();
            $scope.select($scope.currentPage);
        })
    }

    $scope.getOpenDuePaidInvoices = function () {
        invoiceServices.getOpenDuePaidInvoices($routeParams.type).then(function (res) {
            $scope.typeInvoices = res.response.businessInvoices;
            $scope.search();
            $scope.select($scope.currentPage);
        })
    }

    $scope.onFilterChange = function() {
        return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
    };

    $scope.onNumPerPageChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };

    $scope.onOrderChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };

    $scope.search = function() {
        return $scope.filteredStores = $filter("filter")($scope.typeInvoices, $scope.searchKeywords), $scope.onFilterChange()
    };

    $scope.order = function(rowName) {
        return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.typeInvoices, rowName), $scope.onOrderChange()) : void 0
    };

    $scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        $scope.currentPageStores = $scope.filteredStores.slice(start, end);
    }

    $scope.numPerPageOpt = [3, 5, 10, 20];
    $scope.numPerPage = $scope.numPerPageOpt[2];
    $scope.currentPage = 1;
    $scope.currentPageStores = [];
    $scope.main.error_message = '';
    $scope.accounts = {};
	$scope.payment_term_list = {};
    $scope.invoiceNo = "";
    $scope.emailError = 0;
    $scope.paymentMethodError = 0;

    //reveice payment account variable
    $scope.receivePaymentAccountLists = {};

    /*-------*/
    $scope.invoice = {'toUser':'', 'form': 'invoice', 'type' : 'INVOICE', 'invoiceNo' :  '', 'estimates' :  'ES' + Math.floor((Math.random() * 100000)), 'date': new Date(), 'reference' : '', 'dueTo' : new Date(), 'paymentTerm' : '', 'customDateOptions': 'Day(s)', 'startOn' : new Date(), 'endsOn' : new Date(), 'schedule' : '', 'customDate' : '', 'description': '', 'invoiceId': '', 'status' : '', 'account_id': '', 'tax_rate_id': '', 'payment_method': '', 'payment_method_reference': '', 'credit_card_no': ''};
    invoiceServices.getInvoiceNumber().then(function(res){
        $scope.invoice.invoiceNo = res;
    });
	
	$scope.invoice.toUser = "";
    $scope.invoice.notes = [];
    $scope.invoice.totalAmount = 0;
    $scope.invoice.discount = 0;
    $scope.invoice.tax = 0;
    $scope.invoice.sendEmailTo = '';
    $scope.businessProfileId = $scope.globals.currentUser.businessProfile;

    $scope.resendInvoice = {'form': 'resendInvoice'};
    $scope.resendInvoice.sendEmailTo = '';
    $scope.taxRates = {};

    if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
        $scope.main.error_message = "Please complete your business profile before creating invoice.";
        window.location.href = "#/business_profile";
    } else {
        userServices.getCustomerList().then(function(res) {
            $scope.users = res.response.customers;          
        });
        itemServices.getItems().then(function(res) {
            $scope.items = res.response.businessItems;          
        });
        accountServices.getAccountList().then(function(res) {
            $scope.accounts = res.response.accounts;
        });

        accountServices.getParentAccountList().then(function (res) {
            $scope.parentAccountLists = res.response.parentAccounts;
        })

        accountServices.getReceivePaymentAccountList().then(function (res) {
            $scope.receivePaymentAccountLists = res.response.receivePaymentAccountLists;
        })

        taxRateServices.getTaxRatesList().then(function (res) {
            $scope.taxRates = res.response.taxRates;
        })
    }

    $scope.getInvoice = function () {
        invoiceServices.getInvoiceDetails($routeParams.id).then(function (res) {
            if(res.code == 200) {
                $scope.invoiceDetail = res.response.businessInvoice;
                $scope.invoice.invoiceId = $scope.invoiceDetail.id
                $scope.getInvoiceDetails($scope.invoiceDetail.id);
            }
        })
    }

    $scope.delete = function (id) {
        $scope.main.error_message = "";
        invoiceServices.deleteInvoice(id).then(function (res) {
            if(res.code == 200) {
                $scope.main.alert = res.message;
                $ngBootbox.alert($scope.main.alert)
                    .then(function() {
                        setTimeout(function(){
                            //$window.scrollTo(0, 0);
                            $scope.main.alert = "";
                            $scope.main.error_message = "";
                            // window.location.href = "#" + $location.path();
                        }, 500);
                    });
            } else {
                //$window.scrollTo(0, 0);
                $scope.main.error_message = res.message;
            }
        })
    }

	$scope.item_list = [{ 
        'business_item_id': '',
        'name': "",
        'description': "",
        "quantity" : 0,
        "price" : 0,
        "account_id" : "",
        "discount" : 0,
        "amount" : 0
    }];
    $scope.addNew = function(){
        $scope.item_list.push({
            'business_item_id': '',
            'name': "",
            'description': "",
            "quantity" : 0,
            "price" : 0,
            "account_id" : "",
            "discount" : 0,
            "amount" : 0
        });
    };

    $scope.addNewNotes = function() {
        $scope.invoice.notes.push({'Notes':''});
    };

    $scope.addNewUser = function() {
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: "views/user/add_new_customer.html",
            controller: "userCtrlPopup"
        }), modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {
            userServices.getCustomerList().then(function(res) {
                $scope.users = res.response.customers;
            });
        })
    };

    $scope.addNewVendor = function() {
        userServices.addNewVendor($scope.vendor).then(
        function(res) {
         if(res.code == 200) {
                $location.path('/add_expenses');
            } else {
                $scope.main.error_message = "All fields are required.";
            }
        }, function(res) {
            $scope.main.error_message = "All fields are required.";
        });
    };

     $scope.cancel = function() {
         $scope.globals.currentUser.modalInstance.dismiss("cancel");
     };

     $scope.setDueDate = function() {
        if($scope.invoice.paymentTerm == "Paid") {
            $scope.invoice.dueTo =  moment($scope.invoice.date, "YYYY MM DD");
            accountServices.getAccountList().then(function(res) {
                $scope.accounts = res.response.accounts;
            });
        } else if($scope.invoice.paymentTerm == "Due on Invoice") {
            var endDate = new Date($scope.invoice.date || moment($scope.invoice.date, "YYYY MM DD"));
            var days = 0;
            endDate.setDate(endDate.getDate() + days);
            $scope.invoice.dueTo = endDate;
        } else if($scope.invoice.paymentTerm == "Due 4 Weeks") {
            var endDate = new Date($scope.invoice.date || moment($scope.invoice.date, "YYYY MM DD"));
            var days = 27;
            endDate.setDate(endDate.getDate() + days);
            $scope.invoice.dueTo = endDate;
        } else if($scope.invoice.paymentTerm == "Due 8 Weeks") {
            var endDate = new Date($scope.invoice.date || moment($scope.invoice.date, "YYYY MM DD"));
            var days = 55;
            endDate.setDate(endDate.getDate() + days);
            $scope.invoice.dueTo = endDate;
        } else {
            $scope.invoice.dueTo = new Date();
        }
     };

     $scope.setExpiryDate = function () {
         if($scope.invoice.paymentTerm == "2 Weeks") {
             var endDate = new Date($scope.invoice.date || moment($scope.invoice.date, "YYYY MM DD"));
             var days = 14;
             endDate.setDate(endDate.getDate() + days);
             $scope.invoice.dueTo = endDate;
         } else if($scope.invoice.paymentTerm == "4 Weeks") {
             var endDate = new Date($scope.invoice.date || moment($scope.invoice.date, "YYYY MM DD"));
             var days = 28;
             endDate.setDate(endDate.getDate() + days);
             $scope.invoice.dueTo = endDate;
         } else if($scope.invoice.paymentTerm == "1 Month") {
             var endDate = new Date($scope.invoice.date || moment($scope.invoice.date, "YYYY MM DD"));
             var days = 30;
             endDate.setDate(endDate.getDate() + days);
             $scope.invoice.dueTo = endDate;
         } else {
             $scope.invoice.dueTo = new Date();
         }
     }

     $scope.setTypes = function(type) {
        $scope.invoice.type = type;
     };

     $scope.remove = function() {
        var newDataList=[];
        $scope.selectedAll = false;
        angular.forEach($scope.item_list, function(selected){
            if(!selected.selected){
                newDataList.push(selected);
            }
        }); 
        $scope.item_list = newDataList;
         $scope.calculateTotalAmount();
    };

    $scope.saveInvoice = function() {
        $scope.main.error_message = "";       
        validator.checkValidation($scope.invoice).then(function(data) {
            if($scope.invoice.status == "") {
               $scope.invoice.status = "DRAFT"; 
            }
            if(Object.keys(data.error).length === 0) {
                invoiceServices.saveInvoice($scope.invoice, $scope.item_list, $scope.businessProfileId).then(function(res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        var $id = res.response.businessInvoice.id;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                            setTimeout(function() {
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/invoice_details/" + $id;
                            }, 500);
                        });
                    } else {
                        //$window.scrollTo(0, 0);
                        $scope.main.error_message = res.message;
                    }
                });
            } else {
                $scope.main.error_message = data.error;
            }
        });
    }

    $scope.cancel = function() {
        $scope.main.error_message = "";
        window.location.href = "#/dashboard";
    };

    $scope.approve = function() {
         $scope.invoice.status = "APPROVED";
         if($scope.invoice.type == 'REFUND') {
             $scope.updateInvoice();
         } else {
             $scope.saveInvoice();
         }
    };

    $scope.openEmailPopup = function () {
        validator.checkValidation($scope.invoice).then(function(data) {
            console.log(data);
            if(Object.keys(data.error).length === 0) {
                $scope.invoice.status = "SEND EMAIL";
                $('#sendEmail').modal('show');
            } else {
                $scope.main.error_message = data.error;
                $scope.invoice.status = "";
            }
        });
    }

    $scope.cancelEmail = function() {
        $scope.main.error_message = "";
        $scope.invoice.sendEmailTo = "";
        $scope.resendInvoice.sendEmailTo = "";
        $scope.invoice.status = "";
        $("#sendEmail").modal('hide');
    }

    $scope.sendEmail = function() {
        if($scope.invoice.sendEmailTo.length === 0) {
            $scope.emailError = 1;
        } else {
            $("#sendEmail").modal('hide');
            $scope.invoice.status = "SEND EMAIL";
            if($scope.invoice.type == 'REFUND') {
                $scope.updateInvoice();
            } else {
                $scope.saveInvoice();
            }
        }
    }

    $scope.calculateAmount = function(item) {
        item.amount = (item.price * item.quantity);
        if(item.discount != 0) {
            item.amount = item.amount - ((item.amount * item.discount) / 100);
        }
        $scope.calculateTotalAmount();
    }

    $scope.calculateTotalAmount = function() {
         $scope.invoice.totalAmount = 0;
         angular.forEach($scope.item_list, function(selected){
            $scope.invoice.totalAmount = parseInt($scope.invoice.totalAmount) + parseInt(selected.amount);
        });

        if($scope.invoice.discount != 0 || $scope.invoice.discount != "") {
            $scope.invoice.totalAmount = $scope.invoice.totalAmount - ($scope.invoice.totalAmount*($scope.invoice.discount)/100);
        }

        if($scope.invoice.tax_rate_id != '') {
            angular.forEach($scope.taxRates, function (value, key) {
                if(value.id == $scope.invoice.tax_rate_id) {
                    $scope.invoice.tax = value.tax_rates;
                }
            })
        }

        if($scope.invoice.tax != 0 || $scope.invoice.tax != "") {
            $scope.invoice.taxAmount = $scope.invoice.totalAmount*($scope.invoice.tax)/100;
            $scope.invoice.totalAmount = $scope.invoice.totalAmount + $scope.invoice.taxAmount;
        }
    }

    $scope.getItemList = function (index) {
        if($scope.item_list[index].business_item_id != '') {
            itemServices.getItemDetails($scope.item_list[index].business_item_id).then(function (res) {
                $scope.itemDetails = res.response.businessItem;
                $scope.item_list[index].description = $scope.itemDetails.description;
                $scope.item_list[index].quantity = 1;
                $scope.item_list[index].price = $scope.itemDetails.selling_price;
                $scope.item_list[index].account_id = parseInt($scope.itemDetails.account_id);

                $scope.calculateAmount($scope.item_list[index]);
            });
        }
    }

    $scope.getInvoiceDetails = function (invoiceId) {
        $scope.ItemEmpty();
        $scope.invoice.notes = [];
        invoiceServices.getInvoiceDetails(invoiceId).then(function(res){
            $scope.invoiceData = res.response.businessInvoice;
            $scope.invoice.toUser = $scope.invoiceData.customer[0].id;
            $scope.invoice.invoiceNo = $scope.invoiceData.invoice_no;
            $scope.invoice.date = $scope.invoiceData.date;
            $scope.invoice.dueTo = $scope.invoiceData.due_date;
            $scope.invoice.reference = $scope.invoiceData.reference;
            $scope.invoice.paymentTerm = $scope.invoiceData.payment_term;
            $scope.invoice.account_id = parseInt($scope.invoiceData.account_id);
            $scope.invoice.tax_rate_id = parseInt($scope.invoiceData.tax_rate_id);
            $scope.invoice.tax_rate = $scope.invoiceData.tax_rate
            $scope.itemLength = $scope.invoiceData.business_invoice_item.length;
            if($scope.invoiceData.business_invoice_configuration != null) {
                var $configuration = $scope.invoiceData.business_invoice_configuration;
                $scope.invoice.schedule = $configuration.schedule;
                $scope.invoice.startOn = $configuration.start_on;
                if($configuration.end_on != null) {
                    $scope.invoice.endsOn = $configuration.end_on;
                }
                if($configuration.is_never_expires == 1) {
                    $scope.invoice.neverExpires = true;
                    $scope.neverExpires();
                } else {
                    $scope.invoice.neverExpires = false;
                    $scope.neverExpires();
                }
            }

            $scope.setDueDate();

            angular.forEach($scope.invoiceData.business_invoice_item, function (business_item, index) {
                $scope.item_list[index].business_item_id = parseInt(business_item.business_item_id);
                $scope.item_list[index].name = business_item.item_name;
                $scope.item_list[index].description = business_item.description;
                $scope.item_list[index].quantity = business_item.quantity;
                $scope.item_list[index].price = business_item.price;
                $scope.item_list[index].amount = business_item.amount;
                $scope.item_list[index].discount = parseInt(business_item.discount);
                $scope.item_list[index].account_id = parseInt(business_item.account_id);
                $scope.item_list[index].account_name = business_item.account_name;

                if($scope.itemLength > (index + 1) ){
                    $scope.addNew();
                }
            });

            if($scope.invoiceData.notes.length > 0) {
                angular.forEach($scope.invoiceData.notes, function (note, index) {
                    $scope.invoice.notes.push({'Notes': note.title});
                })
            } else {
                $scope.invoice.notes.push({'Notes':''});
            }

            if($scope.invoice.tax_rate_id != '') {
                angular.forEach($scope.taxRates, function (value, key) {
                    if(value.id == $scope.invoice.tax_rate_id) {
                        $scope.invoice.tax = value.tax_rates;
                    }
                })
            }

            $scope.invoice.discount = parseInt($scope.invoiceData.discount);
            $scope.invoice.taxAmount = parseInt($scope.invoiceData.tax_amount);
            $scope.invoice.totalAmount = parseInt($scope.invoiceData.amount);
            $scope.redundDetailShow = true;
        });
    }

    $scope.updateInvoice = function() {
        $scope.main.error_message = "";
        validator.checkValidation($scope.invoice).then(function(data) {
            if($scope.invoice.status == "") {
                $scope.invoice.status = "DRAFT";
            }

            if(Object.keys(data.error).length === 0) {
                invoiceServices.updateInvoice($scope.invoice.invoiceId, $scope.invoice, $scope.item_list, $scope.businessProfileId).then(function(res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    //$window.scrollTo(0, 0);
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.href = "#/dashboard";
                                }, 500);
                            });
                    } else {
                        //$window.scrollTo(0, 0);
                        $scope.main.error_message = res.message;
                    }
                });
            } else {
                $scope.main.error_message = data.error;
            }
        });
    }

    $scope.ItemEmpty = function () {
        $scope.item_list = {};
        $scope.item_list = [{
            'business_item_id': '',
            'name': "",
            'description': "",
            "quantity" : 0,
            "price" : 0,
            "account_id" : "",
            "discount" : 0,
            "amount" : 0
        }];
    }

    $scope.openResendInvoice = function (invoiceId) {
        $scope.resendInvoice.id = invoiceId;
        $('#sendEmail').modal('show');
    }

    $scope.sendInvoice = function () {
        if($scope.resendInvoice.sendEmailTo.length === 0) {
            $scope.emailError = 1;
        } else {
            $("#sendEmail").modal('hide');
            invoiceServices.sendInvoice($scope.resendInvoice).then(function(res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/dashboard";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            });
        }
    }

    $scope.receivePayment = function () {
        invoiceServices.getInvoiceDetails($routeParams.id).then(function (res) {
            if(res.code == 200) {
                $scope.invoice = res.response.businessInvoice;
                $scope.invoice.paymentTerm = $scope.invoice.payment_term;
            }
        })
    }

    $scope.paymentMethodError = 0;
    $scope.sendPayment = function () {
        if($scope.invoice.account_id == '' && $scope.invoice.account_id != undefined) {
            $scope.emailError = 1;
        } else if($scope.invoice.payment_method == '' || $scope.invoice.payment_method == null) {
            $scope.paymentMethodError = 1;
        } else {
            $scope.invoice.paymentTerm = 'Paid';
            invoiceServices.receivePayment($scope.invoice).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/dashboard";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }
    }

    $scope.neverExpires = function () {
        if($scope.invoice.neverExpires == true) {
            $('#ends_on .ng-datepicker-input.form-control').attr('disabled', true);
        } else {
            $('#ends_on .ng-datepicker-input.form-control').attr('disabled', false);
        }
    }


    $scope.activityInvoice = true;
    $scope.activityEstimates = false;
    $scope.activityExpense = false;
    $scope.showAllActivity = false;

    $scope.getActivity = function () {
        dashboardServices.getActivity().then(function (res) {
            $scope.invoiceActivity = res.response.invoices;
            $scope.estimatesActivity = res.response.estimates;
            $scope.expensesActivity = res.response.expenses;
        });
    }

    $scope.changeActivity = function (type) {
        if(type == 'INVOICE') {
            $scope.activityInvoice = true;
            $scope.activityEstimates = false;
            $scope.activityExpense = false;
            $scope.showAllActivity = false;
        }

        if(type == 'ESTIMATES') {
            $scope.activityInvoice = false;
            $scope.activityEstimates = true;
            $scope.activityExpense = false;
            $scope.showAllActivity = false;
        }

        if(type == 'EXPENSES') {
            $scope.activityInvoice = false;
            $scope.activityEstimates = false;
            $scope.activityExpense = true;
            $scope.showAllActivity = false;
        }
    }

    $scope.getAllActivity = function () {
        $scope.showAllActivity = true;
        dashboardServices.getAllActivity().then(function (res) {
            $scope.invoiceActivity = res.response.invoices;
            $scope.estimatesActivity = res.response.estimates;
            $scope.expensesActivity = res.response.expenses;
        });
    }

    $scope.uploadFile = function(files) {
        if(files != null) {
            $scope.file = files[0];
            userServices.uploadFile($scope.file).then(function (res) {
                $scope.invoice.attachment = res.attachment;
            })
        }
    };
}]);