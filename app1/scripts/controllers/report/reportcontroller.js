'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Report Controller
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("reportsCtrl", ["$scope", "$window", "reportServices", "$location", "$cookieStore", "$routeParams", "accountServices", function($scope, $window, reportServices, $location, $cookieStore, $routeParams, accountServices) {

        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);

        $scope.profitLossInvoices = {};
        $scope.profitLossExpenses = {};

        /*accountServices.getSubAccounts().then(function(res) {
            $scope.accounts = res.response.accounts;
        });*/

        var date = new Date();

        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        $scope.convert =  function(date) {
            var date = new Date(date),
                mnth = ("0" + (date.getMonth()+1)).slice(-2),
                day  = ("0" + date.getDate()).slice(-2);
            return [ date.getFullYear(), mnth, day ].join("-");
        }

        // Customer Invoice report

        $scope.invoice = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'date_type': 'invoice_date', 'status': 'All', 'order_by': 'invoice_no', 'user_type': 'USER', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.invoiceReportData = {};
        $scope.invoiceReportTotal = 0;
        $scope.invoiceReportPdfPath = '';
        $scope.invoiceReportCsvPath = '';
        $scope.invoiceReportShow = 0;

        $scope.customerInvoiceReport = function () {
            reportServices.cusomterInvoiceReport($scope.invoice).then(function (res) {
                if(res.code == 200) {
                    $scope.invoiceReportData = res.response.invoices;
                    $scope.invoiceReportTotal = res.response.total;
                    $scope.invoiceReportDate = res.response.date;
                    $scope.invoiceReportPdfPath = res.response.pdfPath;
                    $scope.invoiceReportCsvPath = res.response.csvPath;
                    $scope.invoiceReportShow = 1;
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        },

        //Sale by Items report

        $scope.items = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.salebyItemsData = {};
        $scope.salebyItemsTotal = 0;
        $scope.salebyItemsQuantity = 0;
        $scope.salebyItemsAverage = 0;
        $scope.salesbyItemReportPath = '';
        $scope.salesbyItemCsvPath = '';
        $scope.salebyItemsShow = 0;

        $scope.saleByItems = function () {
            reportServices.saleByItems($scope.items).then(function (res) {
                if(res.code == 200) {
                    $scope.salebyItemsData = res.response.items;
                    $scope.salebyItemsTotal = res.response.totalAmount;
                    $scope.salebyItemsQuantity = res.response.totalQuantity;
                    $scope.salesbyItemReportPath = res.response.pdfPath;
                    $scope.salesbyItemCsvPath = res.response.csvPath;
                    $scope.salesItemsDate = res.response.date;

                    $scope.salebyItemsAverage = $scope.salebyItemsTotal / $scope.salebyItemsQuantity;
                    $scope.salebyItemsShow = 1;
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }

        //Profit and loss report

        $scope.profitLoss = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.profitLoss_expense = {};
        $scope.profitLoss_income = {};
        $scope.totalIncome = 0;
        $scope.totalExpense = 0;
        $scope.profitandLossShow = 0;
        $scope.profitandLossList = 0;
        $scope.profitandLossReport = 0;
        $scope.type = $routeParams.type;
        $scope.account = $routeParams.account;
        $scope.profitandLossPdfPath = '';
        $scope.profitandLossCsvPath = '';

        $scope.profitandLoss = function () {
            reportServices.profitandLoss($scope.profitLoss).then(function (res) {
                if(res.code == 200) {
                    $scope.profitLoss_income = res.response.profitandLoss.income;
                    $scope.profitLoss_expense = res.response.profitandLoss.expense
                    $scope.profitLoss_date = res.response.date;
                    $scope.totalIncome = res.response.totalIncome;
                    $scope.totalExpense = res.response.totalExpense;
                    $scope.profitLossInvoices = res.response.invoiceList;
                    $scope.profitLossExpenses = res.response.expenseList;
                    $scope.profitandLossPdfPath = res.response.pdfPath
                    $scope.profitandLossCsvPath = res.response.csvPath
                    $scope.profitandLossShow = 1;
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }

        $scope.getProfitandLossList = function (type, account) {
            $scope.total = 0;
            $scope.balance = 0;
            $scope.profitandLossDetails = [];

            if(type == 'invoice') {
                angular.forEach($scope.profitLossInvoices, function (invoice, index) {
                    if(invoice.account == account) {
                        $scope.balance += parseInt(invoice.amount);
                        invoice.balance = $scope.balance;
                        $scope.total += parseInt(invoice.amount);
                        $scope.profitandLossDetails.push(invoice);
                    }
                })
            }

            if(type == 'expense') {
                angular.forEach($scope.profitLossExpenses, function (expense, index) {
                    if(expense.account == account) {
                        $scope.balance += parseInt(expense.amount);
                        expense.balance = $scope.balance;
                        $scope.total += parseInt(expense.amount);
                        $scope.profitandLossDetails.push(expense);
                    }
                })
            }
            $scope.profitandLossList = 1;
            $scope.profitandLossReport = 1;
        }

        $scope.back = function () {
            $scope.profitandLossReport = 0;
            $scope.profitandLossList = 0;
        }

        // Balance sheet Report
        $scope.balanceSheet = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'basis': '', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.balanceSheetAssets = {};
        $scope.balanceSheetDate = {};
        $scope.balanceSheetShow = 0;
        $scope.totalAssets = 0;
        $scope.totalLiabilities = 0;
        $scope.basisError = 0;
        $scope.netProfit = 0;
        $scope.balanceSheetOwners = {};
        $scope.totalOwners = 0;
        $scope.balanceSheetShowDetails = 0;
        $scope.balanceSheetReport = 0;
        $scope.balanceSheetPdfPath = '';
        $scope.balanceSheetCsvPath = '';

        $scope.getNetProfit = function () {
            reportServices.profitandLoss($scope.balanceSheet).then(function (res) {
                if(res.code == 200) {
                    $scope.totalIncome = res.response.totalIncome;
                    $scope.totalExpense = res.response.totalExpense;
                    $scope.netProfit = parseInt($scope.totalIncome) - parseInt($scope.totalExpense);
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }

        $scope.getBalanceSheet = function () {
            if($scope.balanceSheet.basis != '') {
                reportServices.getBalanceSheet($scope.balanceSheet).then(function (res) {
                    if (res.code == 200) {
                        $scope.balanceSheetAssets = res.response.balanceSheetAssets;
                        $scope.balanceSheetDate = res.response.date;
                        $scope.totalAssets = res.response.totalAssets;

                        $scope.balanceSheetLiabilities = res.response.balanceSheetLiabilities;
                        $scope.totalLiabilities = res.response.totalLiabilities;

                        $scope.balanceSheetOwners = res.response.balanceSheetOwners;
                        $scope.totalOwners = res.response.totalOwners + $scope.netProfit;

                        $scope.balanceSheetPdfPath = res.response.pdfPath;
                        $scope.balanceSheetCsvPath = res.response.csvPath;

                        $scope.balanceSheetShow = 1;
                    } else {
                        //$window.scrollTo(0, 0);
                        $scope.main.error_message = res.message;
                    }
                })
            } else {
                $scope.basisError = 1;
            }
        }
        
        $scope.getBalanceSheetDetails = function (type) {
            $scope.accountType = type;
            reportServices.getBalanceSheetDetails(type, $scope.balanceSheet).then(function (res) {
                if(res.code == 200) {
                    $scope.balanceSheetDetails = res.response.balanceSheetDetails;
                    $scope.balanceSheet_date = res.response.date;
                    $scope.debitTotal = res.response.debitTotal;
                    $scope.creditTotal = res.response.creditTotal;
                    $scope.balanceSheetShowDetails = 1;
                    $scope.balanceSheetReport = 1;
                }
            })
        }
        
        $scope.balanceSheetback = function () {
           $scope.balanceSheetShowDetails = 0;
           $scope.balanceSheetReport = 0;
        }


        // Journal Report

        $scope.journalReport = {};
        $scope.journal = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.journalReportShow = 0;
        $scope.journalReportPdfPath = '';
        $scope.journalReportCsvPath = '';
        $scope.journalReportTotalDebit = 0;
        $scope.journalReportTotalCredit = 0;

        $scope.getJournalReport = function () {
            reportServices.getJournalReport($scope.journal).then(function (res) {
                if(res.code == 200) {
                    $scope.journalReport = res.response.journalReport;
                    $scope.journal_date = res.response.date;
                    $scope.journalReportPdfPath = res.response.pdfPath;
                    $scope.journalReportCsvPath = res.response.csvPath;
                    $scope.journalReportTotalDebit = res.response.totalDebit;
                    $scope.journalReportTotalCredit = res.response.totalCredit;
                    $scope.journalReportShow = 1;
                }
            })
        }

        // Aged Receviables Report
        $scope.agedReceviables = {};
        $scope.totalReceviables = 0;
        $scope.agedReceviablesPdfPath = '';
        $scope.agedReceviablesCsvPath = '';

        $scope.getAgedReceviables = function () {
            reportServices.getAgedReceviables().then(function (res) {
                if(res.code == 200) {
                    $scope.agedReceviables = res.response.agedReceviables;
                    $scope.totalReceviables = res.response.totalReceviables;
                    $scope.agedReceviablesPdfPath = res.response.pdfPath;
                    $scope.agedReceviablesCsvPath = res.response.csvPath;
                    $scope.currentMonth = res.response.currentMonth;
                }

            })
        }


        // Aged payables Report
        $scope.agedPayables = {};
        $scope.totalPayables = 0;
        $scope.agedPayablesPdfPath = '';
        $scope.agedPayablesCsvPath = '';

        $scope.getAgedPayables = function () {
            reportServices.getAgedPayables().then(function (res) {
                if(res.code == 200) {
                    $scope.agedPayables = res.response.agedPayables;
                    $scope.totalPayables = res.response.totalPayables;
                    $scope.currentMonth = res.response.currentMonth;
                    $scope.agedPayablesPdfPath = res.response.pdfPath;
                    $scope.agedPayablesCsvPath = res.response.csvPath;
                }

            })
        }

        //Trail Balance Report
        $scope.trailBalance = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.trailBalanceData = {};
        $scope.trailBalanceShow = 0;
        $scope.totalDebit = 0;
        $scope.totalCredit = 0;
        $scope.trailBalancePdfPath = '';
        $scope.trailBalanceCsvPath = '';

        $scope.getTrailBalance = function () {
            reportServices.getTrailBalance($scope.trailBalance).then(function (res) {
                if(res.code == 200) {
                    $scope.trailBalanceData = res.response.trailBalanceData;
                    $scope.totalDebit = res.response.totalDebit;
                    $scope.totalCredit = res.response.totalCredit;
                    $scope.trailBalanceDate = res.response.date;
                    $scope.trailBalancePdfPath = res.response.pdfPath;
                    $scope.trailBalanceCsvPath = res.response.csvPath;
                    $scope.trailBalanceShow = 1;
                }
            })
        }

        // Accounting Transaction Report
        $scope.accountingTransaction = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.accountingTransactionShow = 0;
        $scope.accountingTransactionData = {};
        $scope.accountingTransactionPdfPath = '';
        $scope.accountingTransactionCsvPath = '';
        $scope.accountingTotalAmount = 0;
        $scope.accountingGrossTotalAmount = 0;

        $scope.getAccountingTransaction = function () {
            reportServices.getAccountingTransaction($scope.accountingTransaction).then(function (res) {
                if(res.code == 200) {
                    $scope.accountingTransactionData = res.response.accountingTransactions;
                    $scope.accountingTransactionDate = res.response.date;
                    $scope.accountingTransactionPdfPath = res.response.pdfPath;
                    $scope.accountingTransactionCsvPath = res.response.csvPath;
                    $scope.accountingTotalAmount = res.response.totalAmount;
                    $scope.accountingGrossTotalAmount = res.response.totalGrossAmount;
                    $scope.accountingTransactionShow = 1;
                }
            })
        }

        // Invoice Receivables Report
        $scope.invoiceReceivables = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.invoiceReceivablesData = {};
        $scope.invoiceReceivablesShow = 0;
        $scope.invoiceReceivablesPdfPath = '';
        $scope.invoiceReceivablesCsvPath = '';
        $scope.invoiceReceivablesTotalAmount = 0;

        $scope.getInvoiceReceivables = function () {
            reportServices.getInvoiceReceivables($scope.invoiceReceivables).then(function (res) {
                if(res.code == 200) {
                    $scope.invoiceReceivablesData = res.response.invoiceReceivables;
                    $scope.invoiceReceivablesDate = res.response.date;
                    $scope.invoiceReceivablesPdfPath = res.response.pdfPath;
                    $scope.invoiceReceivablesCsvPath = res.response.csvPath;
                    $scope.invoiceReceivablesTotalAmount = res.response.totalAmount;
                    $scope.invoiceReceivablesShow = 1;
                }

            })
        }

        //Tax Report

        $scope.taxReport = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.taxReportData = {};
        $scope.taxReportShow = 0;
        $scope.taxReportPdfPath = '';
        $scope.taxReportCsvPath = '';

        $scope.getTaxReport = function () {
            reportServices.getTaxReport($scope.taxReport).then(function (res) {
                if(res.code == 200) {
                    $scope.taxReportData = res.response.taxReport;
                    $scope.taxReportDate = res.response.date;
                    $scope.taxReportPdfPath = res.response.pdfPath;
                    $scope.taxReportCsvPath = res.response.csvPath;
                    $scope.taxReportShow = 1;
                }
            })
        }

        //Cash Summary Report
        $scope.cashSummary = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.incomeCashSummary = {};
        $scope.expenseCashSummary = {};
        $scope.cashSummaryShow = 0;
        $scope.totalIncome = 0;
        $scope.totalExpense = 0;
        $scope.invoiceList = {};
        $scope.expenseList = {};
        $scope.cashSummaryList = 0;
        $scope.cashSummaryReport = 0;
        $scope.cashSummaryReportPdfPath = '';
        $scope.cashSummaryReportCsvPath = '';

        $scope.getCashSummary = function () {
            reportServices.getCashSummary($scope.cashSummary).then(function (res) {
                if(res.code == 200) {
                    $scope.incomeCashSummary = res.response.incomeCashSummary;
                    $scope.expenseCashSummary = res.response.expenseCashSummary;
                    $scope.totalIncome = res.response.totalIncome;
                    $scope.totalExpense = res.response.totalExpense;
                    $scope.invoiceList = res.response.invoiceList;
                    $scope.expenseList = res.response.expenseList;
                    $scope.cashSummaryDate = res.response.date;
                    $scope.cashSummaryReportPdfPath = res.response.pdfPath;
                    $scope.cashSummaryReportCsvPath = res.response.csvPath;
                    $scope.cashSummaryShow = 1;
                }
            })
        }
        
        $scope.getCashSummaryDetail = function (type, account) {
            $scope.total = 0;
            $scope.balance = 0;
            $scope.cashSummaryDetails = [];

            if(type == 'invoice') {
                angular.forEach($scope.invoiceList, function (invoice, index) {
                    if(invoice.account == account) {
                        $scope.balance += parseInt(invoice.amount);
                        invoice.balance = $scope.balance;
                        $scope.total += parseInt(invoice.amount);
                        $scope.cashSummaryDetails.push(invoice);
                    }
                })
            }

            if(type == 'expense') {
                angular.forEach($scope.expenseList, function (expense, index) {
                    if(expense.account == account) {
                        $scope.balance += parseInt(expense.amount);
                        expense.balance = $scope.balance;
                        $scope.total += parseInt(expense.amount);
                        $scope.cashSummaryDetails.push(expense);
                    }
                })
            }
            $scope.cashSummaryList = 1;
            $scope.cashSummaryReport = 1;
        }

        $scope.cashSummaryBack = function () {
            $scope.cashSummaryList = 0;
            $scope.cashSummaryReport = 0;
        }


        //Cash Account Report
        $scope.cashAccount = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.cashAccountData = {};
        $scope.cashAccountShow = 0;
        $scope.cashAccountPdfPath = '';
        $scope.cashAccountCsvPath = '';

        $scope.getCashAccountReport = function () {
            reportServices.getCashAccountReport($scope.cashAccount).then(function (res) {
                if(res.code == 200) {
                    $scope.cashAccountData = res.response.cashAccount;
                    $scope.cashAccountDate = res.response.date;
                    $scope.totalDebit = res.response.totalDebit;
                    $scope.totalCredit = res.response.totalCredit;
                    $scope.cashAccountPdfPath = res.response.pdfPath;
                    $scope.cashAccountCsvPath = res.response.csvPath;
                    $scope.cashAccountShow = 1;
                }
            })
        }

        //General Ledger Report
        $scope.generalLedger = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER',  'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.generalLedgerData = {};
        $scope.generalLedgerShow = 0;
        $scope.generalLedgerPdfPath = '';
        $scope.generalLedgerCsvPath = '';
        $scope.generalLedgerDetails = {};
        $scope.generalLedgerReport = 0;
        $scope.generalLedgerShowDetails = 0;

        $scope.getGeneralLedger = function () {
            reportServices.getGeneralLedger($scope.generalLedger).then(function (res) {
                if(res.code == 200) {
                    $scope.generalLedgerData = res.response.generalLedgerData;
                    $scope.generalLedgerDate = res.response.date;
                    $scope.totalDebit = res.response.totalDebit;
                    $scope.totalCredit = res.response.totalCredit;
                    $scope.generalLedgerCsvPath = res.response.csvPath;
                    $scope.generalLedgerPdfPath = res.response.pdfPath;
                    $scope.generalLedgerDetails = res.response.generalLedgerDetails;
                    $scope.generalLedgerShow = 1;
                }
            })
        }

        $scope.getLedgerDetails = function (accountType) {
            $scope.accountType = accountType;
            $scope.generalLedgerList = [];
            $scope.debitTotal = 0;
            $scope.creditTotal = 0;

            angular.forEach($scope.generalLedgerDetails, function (ledgerDetails, index) {
                if(ledgerDetails.account == accountType) {
                    $scope.debitTotal += parseInt(ledgerDetails.debit) || 0;
                    $scope.creditTotal += parseInt(ledgerDetails.credit) || 0;
                    $scope.generalLedgerList.push(ledgerDetails);
                }
            })

            $scope.generalLedgerShowDetails = 1;
            $scope.generalLedgerReport = 1;
        }

        $scope.generalReportback = function () {
            $scope.generalLedgerShowDetails = 0;
            $scope.generalLedgerReport = 0;
        }


        // get cash Flow statement report
        $scope.cashFlow = {'from': $scope.convert(firstDay), 'to' : $scope.convert(lastDay), 'user_type': 'USER',  'basis': 'cash', 'businessProfileId': $scope.globals.currentUser.businessProfile};
        $scope.cashFlowReport = {};
        $scope.cashFlowShow = 0;

        $scope.getCashFlow = function () {
            reportServices.getCashFlow($scope.cashFlow).then(function (res) {
                if(res.code == 200) {
                    $scope.cashFlowReport = res.response.cashFlowReport;
                    $scope.netIncome = $scope.cashFlowReport.netIncome;
                    $scope.adjustmentNonCash = $scope.cashFlowReport.adjustmentNonCash;
                    $scope.totalAdjustment = $scope.cashFlowReport.totalAdjustment;
                    $scope.investingActivites = $scope.cashFlowReport.investingActivites;
                    $scope.totalInvesting = $scope.cashFlowReport.totalInvesting;
                    $scope.financingActivites = $scope.cashFlowReport.financingActivites;
                    $scope.totalFinancial = $scope.cashFlowReport.totalFinancial;
                    $scope.netCashOperating = $scope.netIncome + $scope.totalAdjustment
                    $scope.netCash = $scope.netCashOperating - $scope.totalInvesting + $scope.totalFinancial;
                    $scope.cashFlowShow = 1;
                }
            })
        }
        
        $scope.printReport = function (printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
            popupWinindow.document.close();
        }
}])