"use strict";
var projectAppName = "speedyBooksApp";
var webServerUrl = "http://local.api.speedybooks.biz/";
angular.module(projectAppName, ["ngRoute", "ngAnimate", "ngCookies", "ui.bootstrap","jkuri.datepicker","ngBootbox", projectAppName+".localization", projectAppName+".directives"]).config(["$routeProvider", function($routeProvider) {
    return $routeProvider.when("/", {
        redirectTo: "/signin"
    }).when("/dashboard", {
        templateUrl: "views/user/dashboard.html",
        controller: "userCtrl"
    }).when("/accountant_dashboard", {
        templateUrl: "views/CPA/dashboard.html",
        controller: "cpaCtrl"
    }).when("/createpassword", {
        templateUrl: "views/accounts/createpassword.html",
        controller: "createPasswordCtrl"
    }).when("/changePassword", {
        templateUrl: "views/accounts/changePassword.html",
        controller: "changePasswordCtrl"
    }).when("/business_profile", {
        templateUrl: "views/user/business_profile.html",
        controller: "userCtrl"
    }).when("/skipped", {
        templateUrl: "views/user/skipped.html",
        controller: "userCtrl"
    }).when("/add_invoice", {
        templateUrl: "views/income/add_invoice.html",
        controller: "invoiceCtrl"
    }).when("/user", {
        templateUrl: "views/user/user.html",
        controller: "userCtrl"
    }).when("/user/manage", {
        templateUrl: "views/user/manage_user.html",
        controller: "userCtrl"
    }).when("/invite/user", {
        templateUrl: "views/user/invite_user.html",
        controller: "inviteUserCtrl"
    }).when("/invite/user/edit/:id", {
        templateUrl: "views/user/edit_invite_user.html",
        controller: "inviteUserCtrl"
    }).when("/add_new_user", {
        templateUrl: "views/user/add_new_user.html",
        controller: "userCtrl"
    }).when("/add_new_customer", {
        templateUrl: "views/user/add_new_customer.html",
        controller: "userCtrl"
    }).when("/user/edit/:id", {
        templateUrl: "views/user/user_edit.html",
        controller: "userCtrl"
    }).when("/add_new_vendor", {
        templateUrl: "views/user/add_new_vendor.html",
        controller: "userCtrl"
    }).when("/vendor/edit/:id", {
        templateUrl: "views/user/vendor_edit.html",
        controller: "userCtrl"
    }).when("/vendor", {
        templateUrl: "views/user/vendor.html",
        controller: "userCtrl"
    }).when("/expenses", {
        templateUrl: "views/income/expense_list.html",
        controller: "expenseCtrl"
    }).when("/expenses/view/:id", {
        templateUrl: "views/income/expense_detail.html",
        controller: "expenseCtrl"
    }).when("/add_expenses", {
        templateUrl: "views/income/add_expense.html",
        controller: "expenseCtrl"
    }).when("/expenses/edit/:id", {
        templateUrl: "views/income/expense_edit.html",
        controller: "expenseCtrl"
    }).when("/convert/expense/:id", {
        templateUrl: "views/income/convert_expense.html",
        controller: "expenseCtrl"
    }).when("/expense_recurring", {
        templateUrl: "views/income/expense_recurring_list.html",
        controller: "expenseCtrl"
    }).when("/add_expense_recurring", {
        templateUrl: "views/income/add_expense_recurring.html",
        controller: "expenseCtrl"
    }).when("/expense/make/payment/:id", {
        templateUrl: "views/income/expense_make_payment.html",
        controller: "expenseCtrl"
    }).when("/estimates", {
        templateUrl: "views/income/estimate_list.html",
        controller: "invoiceCtrl"
    }).when("/add_estimates", {
        templateUrl: "views/income/add_estimates.html",
        controller: "invoiceCtrl"
    }).when("/refund", {
        templateUrl: "views/income/refund_list.html",
        controller: "invoiceCtrl"
    }).when("/add_refund", {
        templateUrl: "views/income/add_refund.html",
        controller: "invoiceCtrl"
    }).when("/sales_recurring", {
        templateUrl: "views/income/sales_recurring_list.html",
        controller: "invoiceCtrl"
    }).when("/add_sales_recurring", {
        templateUrl: "views/income/add_sales_recurring.html",
        controller: "invoiceCtrl"
    }).when("/invoice_details/:id", {
        templateUrl: "views/income/invoice_detail.html",
        controller: "invoiceCtrl"
    }).when("/invoice", {
        templateUrl: "views/income/invoice_list.html",
        controller: "invoiceCtrl"
    }).when("/invoice/receive/payment/:id", {
        templateUrl: "views/income/receive_payment.html",
        controller: "invoiceCtrl"
    }).when("/invoice/edit/:id", {
        templateUrl: "views/income/invoice_edit.html",
        controller: "invoiceCtrl"
    }).when("/estimates/edit/:id", {
        templateUrl: "views/income/estimate_edit.html",
        controller: "invoiceCtrl"
    }).when("/sales_recurring/edit/:id", {
        templateUrl: "views/income/sales_recurring_edit.html",
        controller: "invoiceCtrl"
    }).when("/invoice/type/:type", {
        templateUrl: "views/income/invoice_type_list.html",
        controller: "invoiceCtrl"
    }).when("/upcoming/payment/list", {
        templateUrl: "views/income/upcoming_payment_list.html",
        controller: "invoiceCtrl"
    }).when("/recent/activity", {
        templateUrl: "views/income/recent_activity_list.html",
        controller: "invoiceCtrl"
    }).when("/purchase_order", {
        templateUrl: "views/income/purchase_order_list.html",
        controller: "expenseCtrl"
    }).when("/purchase_order_details/:id", {
        templateUrl: "views/income/purchase_order_detail.html",
        controller: "expenseCtrl"
    }).when("/purchase_order/edit/:id", {
        templateUrl: "views/income/purchase_order_edit.html",
        controller: "expenseCtrl"
    }).when("/add_purchase_order", {
        templateUrl: "views/income/add_purchase_order.html",
        controller: "expenseCtrl"
    }).when("/accounts", {
        templateUrl: "views/chartAccount/account_list.html",
        controller: "accountCtrl"
    }).when("/account/edit/:id", {
        templateUrl: "views/chartAccount/account_edit.html",
        controller: "accountCtrl"
    }).when("/inventory", {
        templateUrl: "views/items/item_list.html",
        controller: "itemCtrl"
    }).when("/add_inventory", {
        templateUrl: "views/items/add_new_item.html",
        controller: "itemCtrl"
    }).when("/inventory/edit/:id", {
        templateUrl: "views/items/edit_item.html",
        controller: "itemCtrl"
    }).when("/payroll", {
        templateUrl: "views/payroll/payroll.html",
        controller: "payrollCtrl"
    }).when("/add_payroll", {
        templateUrl: "views/payroll/add_payroll.html",
        controller: "payrollCtrl"
    }).when("/SetDates", {
        templateUrl: "views/payroll/set_dates.html",
        controller: "payrollCtrl"
    }).when("/PayItems", {
        templateUrl: "views/payroll/pay_items.html",
        controller: "payrollCtrl"
    }).when("/paychecks_details/:id", {
        templateUrl: "views/payroll/paychecks_details.html",
        controller: "payrollCtrl"
    }).when("/employee/view/:id", {
        templateUrl: "views/payroll/employee_details.html",
        controller: "payrollCtrl"
    }).when("/employee/edit/:id", {
        templateUrl: "views/payroll/employee_edit.html",
        controller: "payrollCtrl"
    }).when("/reimbursement", {
        templateUrl: "views/reimbursement/reimbursement.html",
        controller: "reimbursementCtrl"
    }).when("/add_reimbursement", {
        templateUrl: "views/reimbursement/add_reimbursement.html",
        controller: "reimbursementCtrl"
    }).when("/reimbursement_details/:id", {
        templateUrl: "views/reimbursement/reimbursement_details.html",
        controller: "reimbursementCtrl"
    }).when("/reimbursement/convert/expense/:id", {
        templateUrl: "views/reimbursement/convert_to_expense.html",
        controller: "reimbursementCtrl"
    }).when("/reports", {
        templateUrl: "views/reports/reports.html",
        controller: "reportsCtrl"
    }).when("/customer_invoice_report", {
        templateUrl: "views/reports/customer_invoice_report.html",
        controller: "reportsCtrl"
    }).when("/sale_by_items", {
        templateUrl: "views/reports/sale_by_items.html",
        controller: "reportsCtrl"
    }).when("/profit_and_loss", {
        templateUrl: "views/reports/profit_and_loss.html",
        controller: "reportsCtrl"
    }).when("/balance_sheet", {
        templateUrl: "views/reports/balance_sheet.html",
        controller: "reportsCtrl"
    }).when("/journal-report", {
        templateUrl: "views/reports/journal_report.html",
        controller: "reportsCtrl"
    }).when("/aged-receivables", {
        templateUrl: "views/reports/aged_receviables.html",
        controller: "reportsCtrl"
    }).when("/aged-payables", {
        templateUrl: "views/reports/aged_payables.html",
        controller: "reportsCtrl"
    }).when("/trial_balance", {
        templateUrl: "views/reports/trail_balance.html",
        controller: "reportsCtrl"
    }).when("/accounting_transaction", {
        templateUrl: "views/reports/accounting_transaction.html",
        controller: "reportsCtrl"
    }).when("/invoice_receivables", {
        templateUrl: "views/reports/invoice_receivables.html",
        controller: "reportsCtrl"
    }).when("/tax_report", {
        templateUrl: "views/reports/tax_report.html",
        controller: "reportsCtrl"
    }).when("/cash_summary", {
        templateUrl: "views/reports/cash_summary.html",
        controller: "reportsCtrl"
    }).when("/cash_account_report", {
        templateUrl: "views/reports/cash_account.html",
        controller: "reportsCtrl"
    }).when("/general_ledger_report", {
        templateUrl: "views/reports/general_ledger.html",
        controller: "reportsCtrl"
    }).when("/cash_flow_report", {
        templateUrl: "views/reports/cash_flow.html",
        controller: "reportsCtrl"
    }).when("/cpa/reports/:id", {
        templateUrl: "views/CPA/reports.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/customer_invoice_report/:id", {
        templateUrl: "views/CPA/customer_invoice_report.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/sale_by_items/:id", {
        templateUrl: "views/CPA/sale_by_items.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/profit-and-loss/:id", {
        templateUrl: "views/CPA/profit_and_loss.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/balance-sheet/:id", {
        templateUrl: "views/CPA/balance_sheet.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/journal-report/:id", {
        templateUrl: "views/CPA/journal_report.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/aged-payables/:id", {
        templateUrl: "views/CPA/aged_payables.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/aged-receivables/:id", {
        templateUrl: "views/CPA/aged_receivables.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/trial_balance/:id", {
        templateUrl: "views/CPA/trial_balance.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/accounting_transaction/:id", {
        templateUrl: "views/CPA/accounting_transaction.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/invoice_receivables/:id", {
        templateUrl: "views/CPA/invoice_receivables.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/tax-report/:id", {
        templateUrl: "views/CPA/tax_report.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/cash-summary/:id", {
        templateUrl: "views/CPA/cash_summary.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/cash-account/:id", {
        templateUrl: "views/CPA/cash_account.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/general-ledger/:id", {
        templateUrl: "views/CPA/general_ledger.html",
        controller: "cpaReportsCtrl"
    }).when("/cpa/cash_flow_report/:id", {
        templateUrl: "views/CPA/cash_flow_report.html",
        controller: "cpaReportsCtrl"
    }).when("/banking", {
        templateUrl: "views/banking/banking.html",
        controller: "bankingCtrl"
    }).when("/banking/upload/csv", {
        templateUrl: "views/banking/upload_csv.html",
        controller: "bankingCtrl"
    }).when("/taxRates", {
        templateUrl: "views/taxRates/taxRates.html",
        controller: "taxRatesCtrl"
    }).when("/journals", {
        templateUrl: "views/journal/journal.html",
        controller: "journalCtrl"
    }).when("/add_journal", {
        templateUrl: "views/journal/add_journal.html",
        controller: "journalCtrl"
    }).when("/journal_details/:id", {
        templateUrl: "views/journal/journal_details.html",
        controller: "journalCtrl"
    }).when("/check", {
        templateUrl: "views/cheque/cheque.html",
        controller: "chequeCtrl"
    }).when("/check_details/:id", {
        templateUrl: "views/cheque/cheque_details.html",
        controller: "chequeCtrl"
    }).when('/add_check', {
        templateUrl: "views/cheque/add_check.html",
        controller: "chequeCtrl"
    }).when('/reconcile', {
        templateUrl: "views/reconcile/reconcile.html",
        controller: "reconcileCtrl"
    }).when('/reconcile/report/:id', {
        templateUrl: "views/reconcile/reconcile_report.html",
        controller: "reconcileCtrl"
    }).when("/companyAccount", {
        templateUrl: "views/user/company_account.html",
        controller: "companyAccountCtrl"
    }).when("/privacy-policy", {
        templateUrl: "views/static/privacy-policy.html"
    }).when("/tables/dynamic", {
        templateUrl: "views/tables/dynamic.html"
    }).when("/charts/others", {
        templateUrl: "views/charts/charts.html"
    }).when("/charts/morris", {
        templateUrl: "views/charts/morris.html"
    }).when("/charts/flot", {
        templateUrl: "views/charts/flot.html"
    }).when("/mail/inbox", {
        templateUrl: "views/mail/inbox.html"
    }).when("/mail/compose", {
        templateUrl: "views/mail/compose.html"
    }).when("/mail/single", {
        templateUrl: "views/mail/single.html"
    }).when("/pages/features", {
        templateUrl: "views/pages/features.html"
    }).when("/signin", {
        templateUrl: "views/accounts/signin.html"
    }).when("/signup", {
        templateUrl: "views/accounts/signup.html"
    }).when("/signup_accountant", {
        templateUrl: "views/CPA/signup_accountant.html"
    }).when("/forgot", {
        templateUrl: "views/accounts/forgot-password.html"
    }).when("/pages/lock-screen", {
        templateUrl: "views/pages/lock-screen.html"
    }).when("/pages/profile", {
        templateUrl: "views/pages/profile.html"
    }).when("/404", {
        templateUrl: "views/404.html"
    }).when("/pages/500", {
        templateUrl: "views/pages/500.html"
    }).when("/pages/blank", {
        templateUrl: "views/pages/blank.html"
    }).when("/pages/invoice", {
        templateUrl: "views/pages/invoice.html"
    }).when("/pages/services", {
        templateUrl: "views/pages/services.html"
    }).when("/pages/about", {
        templateUrl: "views/pages/about.html"
    }).when("/pages/contact", {
        templateUrl: "views/pages/contact.html"
    }).when("/tasks", {
        templateUrl: "views/tasks/tasks.html"
    }).when("/logout", {
        templateUrl: "views/accounts/logout.html"
    }).otherwise({
        redirectTo: "/404"
    })
}]);

angular.module(projectAppName+".localization", []).factory("localize", ["$http", "$rootScope", "$window", function($http, $rootScope, $window) {
            var localize;
            return localize = {
                language: "",
                url: void 0,
                resourceFileLoaded: !1,
                successCallback: function(data) {
                    return localize.dictionary = data, localize.resourceFileLoaded = !0, $rootScope.$broadcast("localizeResourcesUpdated")
                },
                setLanguage: function(value) {
                    return localize.language = value.toLowerCase().split("-")[0], localize.initLocalizedResources()
                },
                setUrl: function(value) {
                    return localize.url = value, localize.initLocalizedResources()
                },
                buildUrl: function() {
                    return localize.language || (localize.language = ($window.navigator.userLanguage || $window.navigator.language).toLowerCase(), localize.language = localize.language.split("-")[0]), "i18n/resources-locale_" + localize.language + ".js"
                },
                initLocalizedResources: function() {
                    var url;
                    return url = localize.url || localize.buildUrl(), $http({
                        method: "GET",
                        url: url,
                        cache: !1
                    }).success(localize.successCallback).error(function() {
                        return $rootScope.$broadcast("localizeResourcesUpdated")
                    })
                },
                getLocalizedString: function(value) {
                    var result, valueLowerCase;
                    return result = void 0, localize.dictionary && value ? (valueLowerCase = value.toLowerCase(), result = "" === localize.dictionary[valueLowerCase] ? value : localize.dictionary[valueLowerCase]) : result = value, result
                }
            }
        }]).directive("i18n", ["localize", function(localize) {
            var i18nDirective;
            return i18nDirective = {
                restrict: "EA",
                updateText: function(ele, input, placeholder) {
                    var result;
                    return result = void 0, "i18n-placeholder" === input ? (result = localize.getLocalizedString(placeholder), ele.attr("placeholder", result)) : input.length >= 1 ? (result = localize.getLocalizedString(input), ele.text(result)) : void 0
                },
                link: function(scope, ele, attrs) {
                    return scope.$on("localizeResourcesUpdated", function() {
                        return i18nDirective.updateText(ele, attrs.i18n, attrs.placeholder)
                    }), attrs.$observe("i18n", function(value) {
                        return i18nDirective.updateText(ele, value, attrs.placeholder)
                    })
                }
            }
        }]).controller("LangCtrl", ["$scope", "localize", function($scope, localize) {
            return $scope.lang = "English", $scope.setLang = function(lang) {
                switch (lang) {
                    case "English":
                        localize.setLanguage("EN-US");
                        break;
                    case "Español":
                        localize.setLanguage("ES-ES");
                        break;
                    case "日本語":
                        localize.setLanguage("JA-JP");
                        break;
                    case "中文":
                        localize.setLanguage("ZH-TW");
                        break;
                    case "Deutsch":
                        localize.setLanguage("DE-DE");
                        break;
                    case "français":
                        localize.setLanguage("FR-FR");
                        break;
                    case "Italiano":
                        localize.setLanguage("IT-IT");
                        break;
                    case "Portugal":
                        localize.setLanguage("PT-BR");
                        break;
                    case "Русский язык":
                        localize.setLanguage("RU-RU");
                        break;
                    case "한국어":
                        localize.setLanguage("KO-KR")
                }
                return $scope.lang = lang
            }, $scope.getFlag = function() {
                var lang;
                switch (lang = $scope.lang) {
                    case "English":
                        return "flags-american";
                    case "Español":
                        return "flags-spain";
                    case "日本語":
                        return "flags-japan";
                    case "中文":
                        return "flags-china";
                    case "Deutsch":
                        return "flags-germany";
                    case "français":
                        return "flags-france";
                    case "Italiano":
                        return "flags-italy";
                    case "Portugal":
                        return "flags-portugal";
                    case "Русский язык":
                        return "flags-russia";
                    case "한국어":
                        return "flags-korea"
                }
            }
        }]);

angular.module(projectAppName).

config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

