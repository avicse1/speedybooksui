'use strict';

angular.module(projectAppName)

.factory('validator', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	var service = {};
    	service.checkValidationsString = function(authFormField, fieldLabel) {
    		var deferred = $q.defer();
    		var data = {}
        	if(authFormField == "" || authFormField == null) {
        		data.code = 508;
        		data.message = "Please enter value for " + fieldLabel;
        	} else {
        		data.code = 200;
        	}
        	deferred.resolve(data);
        	return deferred.promise;
        };

        service.checkValidationsSelect = function(authFormField, fieldLabel) {
        	var deferred = $q.defer();
    		var data = {"code" : "200", "message" : ""};
        	if(authFormField == "" || authFormField == null || authFormField == 0) {
        		data.code = 508;
        		data.message = "Please select a value for " + fieldLabel;
        	}
        	deferred.resolve(data);
        	return deferred.promise;
        };

        service.checkValidationsNumbers = function(authFormField) {
        	var deferred = $q.defer();
    		var data = "";
        	if(authFormField == "" || authFormField == null) {

        	}
        	deferred.resolve(data);
        	return deferred.promise;
        };

        service.checkValidation = function(authForm) {
            var deferred = $q.defer();
            var error = {};
            var data = [];
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(authForm.form == "invoice") {
                if(authForm.toUser != undefined && authForm.toUser == "") {
                    error.toUser_error = 'Please select Customer';
                    alert('Please select Customer');
                }
                /*if(authForm.reference != undefined && authForm.reference == "") {
                    error.reference_error = 'Please enter Reference';
                }*/
                if(authForm.type == "ESTIMATES") {
                    if(authForm.paymentTerm != undefined && authForm.paymentTerm == "") {
                        error.paymentTerm_error = 'Please select Expiry Date';
                        alert('Please select Expiry Date');
                    }
                    if(authForm.date != undefined && authForm.date == "") {
                        error.date_error = 'Please select Estimate Date';
                        alert('Please select Estimate Date');
                    }
                    if(authForm.dueTo != undefined && authForm.dueTo == "") {
                        error.dueTo_error = 'Please select Expiry Date';
                        alert('Please select Expiry Date');
                    }
                } else if(authForm.type == "INVOICE") {
                    if(authForm.date != undefined && authForm.date == "") {
                        error.date_error = 'Please select Date';
                        alert('Please select Date');
                    }
                    if(authForm.paymentTerm != undefined && authForm.paymentTerm == "") {
                        error.paymentTerm_error = 'Please select Payment Term';
                        alert('Please select Payment Term');
                    }
                    if(authForm.dueTo != undefined && authForm.dueTo == "") {
                        error.dueTo_error = 'Please select Due Date';
                        alert('Please select Due Date');
                    }
                    if(authForm.paymentTerm == 'Paid') {
                        if(authForm.account_id != undefined && authForm.account_id == "") {
                            error.account_id_error = 'Please select account';
                            alert('Please select account');
                        }
                    }

                } else if(authForm.type == "RECURRING"){
                    if(authForm.date != undefined && authForm.date == "") {
                        error.date_error = 'Please select Date';
                        alert('Please select Date');
                    }
                    if(authForm.dueTo != undefined && authForm.dueTo == "") {
                        error.dueTo_error = 'Please select Due Date';
                        alert('Please select Due Date');
                    }
                    if(authForm.paymentTerm != undefined && authForm.paymentTerm == "") {
                        error.paymentTerm_error = 'Please select Payment Term';
                        alert('Please select Payment Term');
                    }
                    if(authForm.schedule != undefined && authForm.schedule == "") {
                        error.schedule_error = 'Please select Schedule';
                        alert('Please select Schedule');
                    } 
                    if(authForm.schedule != undefined && authForm.schedule == 5) {
                        if(authForm.customDate != undefined && authForm.customDate == "") {
                            error.customDate_error = 'Please enter Schedule Days';
                            alert('Please enter Schedule Days');
                        }
                        if(authForm.customDateOptions != undefined && authForm.customDateOptions == "") {
                            error.customDateOptions_error = 'Please select Schedule Options';
                            alert('Please select Schedule Options');
                        }
                    }
                    if(authForm.startOn != undefined && authForm.startOn == "") {
                        error.startOn_error = 'Please select Start Date';
                        alert('Please select Start Date');
                    }
                    if(authForm.neverExpires != undefined && authForm.neverExpires == "false" && authForm.endsOn == "") {
                        error.endsOn_error = 'Please select End Date';
                        alert('Please select End Date');
                    }
                } else if (authForm.type == "REFUND") {
                    if(authForm.invoiceId != undefined && authForm.invoiceId == "") {
                        error.invoiceId_error = 'Please select invoice no';
                        alert('Please select invoice no');
                    }
                    if(authForm.date != undefined && authForm.date == "") {
                        error.date_error = 'Please select Date';
                        alert('Please select Date');
                    }
                    if(authForm.paymentTerm != undefined && authForm.paymentTerm == "") {
                        error.paymentTerm_error = 'Please select Payment Term';
                        alert('Please select Payment Term');
                    }
                    if(authForm.dueTo != undefined && authForm.dueTo == "") {
                        error.dueTo_error = 'Please select Due Date';
                        alert('Please select Due Date');
                    }
                    if(authForm.description != undefined && authForm.description == "") {
                        error.description_error = 'Please enter description';
                        alert('Please enter description');
                    }
                }
            } else if(authForm.form == "expense") {
               
                if(authForm.payee != undefined && authForm.payee == "") {
                    error.payee_error = 'Please select a Payee';
                    alert('Please select a Payee');
                }

                if(authForm.paymentTerm != undefined && authForm.paymentTerm == "") {
                    error.paymentTerm_error = 'Please select Payment Term';
                    alert('Please select Payment Term');
                }

                if(authForm.type == "EXPENSE") {
                    if(authForm.paymentTerm == 'Paid') {
                        if(authForm.account_id != undefined && authForm.account_id == "") {
                            error.account_id_error = 'Please select account';
                            alert('Please select account');
                        }
                    }
                }

                if(authForm.type == "RECURRING") {
                    if(authForm.date != undefined && authForm.date == "") {
                        error.date_error = 'Please select Estimate Date';
                        alert('Please select Estimate Date');
                    }
                    if(authForm.dueTo != undefined && authForm.dueTo == "") {
                        error.dueTo_error = 'Please select Expiry Date';
                        alert('Please select Expiry Date');
                    }
                    if(authForm.schedule != undefined && authForm.schedule == "") {
                        error.schedule_error = 'Please select Schedule';
                        alert('Please select Schedule');
                    }
                    if(authForm.schedule != undefined && authForm.schedule == 5) {
                        if(authForm.customDate != undefined && authForm.customDate == "") {
                            error.customDate_error = 'Please enter Schedule Days';
                            alert('Please enter Schedule Days');
                        }
                        if(authForm.customDateOptions != undefined && authForm.customDateOptions == "") {
                            error.customDateOptions_error = 'Please select Schedule Options';
                            alert('Please select Schedule Options');
                        }
                    }
                    if(authForm.startOn != undefined && authForm.startOn == "") {
                        error.startOn_error = 'Please select Start Date';
                        alert('Please select Start Date');
                    }
                    if(authForm.neverExpires != undefined && authForm.neverExpires == "false" && authForm.endsOn == "") {
                        error.endsOn_error = 'Please select End Date';
                        alert('Please select End Date');
                    }
                }

            } else if(authForm.form == "purchaseOrder") {
                if(authForm.payee != undefined && authForm.payee == "") {
                    error.payee_error = 'Please select a Payee';
                    alert('Please select a Payee');
                }
            } else if(authForm.form == "payroll") {

                if(authForm.payFrequency != undefined && authForm.payFrequency == "") {
                    error.payFrequency_error = 'Please select a Pay Frequency';
                    alert('Please select a Pay Frequency');
                }
                if(authForm.periodFrom != undefined && authForm.periodFrom == "") {
                    error.periodFrom_error = 'Please select a Period From';
                    alert('Please select a Period From');
                }
                if(authForm.periodTo != undefined && authForm.periodTo == "") {
                    error.periodTo_error = 'Please select a Period To';
                    alert('Please select a Period To');
                }
                if(authForm.paymentDate != undefined && authForm.paymentDate == "") {
                    error.paymentDate_error = 'Please select a Payment date';
                    alert('Please select a Payment date');
                }
            } else if(authForm.form == "reimbursement") {

                if(authForm.toUser != undefined && authForm.toUser == "") {
                    error.toUser_error = 'Please select Customer';
                    alert('Please select Customer');
                }
                if(authForm.description != undefined && authForm.description == "") {
                    error.description_error = 'Please enter description';
                    alert('Please enter description');
                }
            } else if(authForm.form == "taxRate") {

                if(authForm.name != undefined && authForm.name == "") {
                    error.name_error = 'Please enter tax name';
                    alert('Please enter tax name');
                }
                if(authForm.component != undefined && authForm.component == "") {
                    error.component_error = 'Please enter component';
                    alert('Please enter component');
                }
                if(authForm.tax_rates != undefined && authForm.tax_rates == "") {
                    error.tax_rates_error = 'Please enter tax rates';
                    alert('Please enter tax rates');
                }
            } else if(authForm.form == "resendInvoice") {
            }
            else if(authForm.form == "cheque") {
                if(authForm.cheque_no != undefined && authForm.cheque_no == "") {
                    error.cheque_no_error = 'Please enter cheque no';
                    alert('Please enter cheque no');
                }
                if(authForm.vendor_id != undefined && authForm.vendor_id == "") {
                    error.vendor_id_error = 'Please select vendor';
                    alert('Please select vendor');
                }

                if(authForm.item_account_id != undefined && authForm.item_account_id == "") {
                    error.item_account_id_error = 'Please select account';
                    alert('Please select account');
                }
            }
            else if(authForm.form == "journal") {
                if(authForm.name != undefined && authForm.name == "") {
                    error.name_error = 'Please enter narration';
                    alert('Please enter narration');
                }
            }

                if(authForm.sendEmailTo != undefined && authForm.sendEmailTo.length > 0) {
                    if(authForm.sendEmailTo == "" || !re.test(authForm.sendEmailTo)) {
                        error.sendEmailTo_error = 'Please enter a valid email address';
                        alert('Please enter a valid email address');
                    }
                }
                var count = 1;
                angular.forEach(authForm.items, function(selected){
                    if(selected.name != undefined && selected.name == "") {
                        //error.toUser_error = 'Please select Customer';
                        error.push('Please select Item for Item list ' + count);
                    }
                    if(selected.description != undefined && selected.description == "") {
                        //error.toUser_error = 'Please select Customer';
                        error.push('Please enter Description for Item list ' + count);
                    }
                    if(selected.quantity != undefined && selected.quantity == "") {
                        //error.toUser_error = 'Please select Customer';
                        error.push('Please enter Quantity for Item list ' + count);
                    }
                    if(selected.price != undefined && selected.price == "") {
                        //error.toUser_error = 'Please select Customer';
                        error.push('Please enter Price for Item list ' + count);
                    }
                    if(selected.account != undefined && selected.account == "") {
                        //error.toUser_error = 'Please select Customer';
                        error.push('Please select Account for Item list ' + count);
                    }
                    count++;
                }); 
                data.error = error;
            deferred.resolve(data);
            return deferred.promise;
        };
        return service;
        
 }])
 