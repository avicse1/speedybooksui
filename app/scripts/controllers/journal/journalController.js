'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Journal Controller
 * @description
 * #journalCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("journalCtrl", ["$scope", "$window", "$filter", "$routeParams", "validator", "$modal", "journalServices", "$cookieStore", "accountServices", "$location", "$ngBootbox", function($scope, $window, $filter, $routeParams, validator, $modal, journalServices, $cookieStore, accountServices, $location, $ngBootbox) {

        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);

        $scope.businessJournals = {};
        $scope.accounts = {};
        $scope.businessProfileId = $scope.globals.currentUser.businessProfile;
        $scope.journal = {'form': 'journal', 'date': new Date(), 'name' : '', 'status':''};

        // Journal Details Listings Start
        journalServices.getJournalList().then(function(res){
            $scope.businessJournals = res.response.businessJournals;
            $scope.search();
            $scope.select($scope.currentPage)
        });

        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.businessJournals, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.businessJournals, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];
        $scope.taxRates = {};

        // Journal Details Listings End
        if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
            $scope.main.error_message = "Please complete your business profile before creating Journal.";
            window.location.href = "#/business_profile";
        } else {
            accountServices.getAccountList().then(function(res) {
                $scope.accounts = res.response.accounts;
            });

            accountServices.getParentAccountList().then(function (res) {
                $scope.parentAccountLists = res.response.parentAccounts;
            })
        }

        $scope.journal_item_list = [{
            'description': "",
            "account_id" : "",
            "debit" : 0,
            "credit" : 0
        }];

        $scope.default_journal_entry = function() {
            for(var i=1; i<6; i++) {
                $scope.journal_item_list.push({
                    'description': "",
                    "account_id" : "",
                    "debit" : 0,
                    "credit" : 0
                });
            }
        }

        $scope.addNew = function() {
            $scope.journal_item_list.push({
                'description': "",
                "account_id" : "",
                "debit" : 0,
                "credit" : 0
            });
        };

        $scope.remove = function(){
            var newDataList=[];
            $scope.totalDebit = 0;
            $scope.totalCredit = 0;
            $scope.selectedAll = false;
            angular.forEach($scope.journal_item_list, function(selected){
                if(!selected.selected) {
                    $scope.totalDebit += selected.debit;
                    $scope.totalCredit += selected.credit;
                    newDataList.push(selected);
                }
            });
            $scope.journal_item_list = newDataList;
        };

        $scope.calculateAmount = function(item) {
            $scope.totalDebit = 0;
            $scope.totalCredit = 0;
            angular.forEach($scope.journal_item_list, function(selected){
                $scope.totalDebit += selected.debit;
                $scope.totalCredit += selected.credit;
            });

            isValid();
        }
    

        $scope.approve = function() {
            
            $scope.journal.status = "APPROVED";
            $scope.saveJournal();
        };

        function isValid(){
            var isValidData = {};
            var totalCreditAmt = 0;
            var totalDebitAmt = 0;
            for(var i=0; i<$scope.journal_item_list.length; i++){
                totalDebitAmt += $scope.journal_item_list[i].debit;
                totalCreditAmt += $scope.journal_item_list[i].credit;
            }
            if(totalCreditAmt != totalDebitAmt){
                isValidData.message = "Journal out of balance! Debit and Credit amount must be equal.";
                isValidData.status = false;
            }
            return isValidData;
        }
        
        $scope.saveJournal = function () {
            var isValidResult = isValid();
            if(isValidResult.status == false){
                alert(isValidResult.message);
                $scope.main.error_message = isValidResult.message;
                return;
            }
        
            validator.checkValidation($scope.journal).then(function(data) {
                if($scope.journal.status == "") {
                    $scope.journal.status = "DRAFT";
                }
                if(Object.keys(data.error).length === 0) {
                    journalServices.saveJournal($scope.journal, $scope.journal_item_list, $scope.businessProfileId).then(function(res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            var $id = res.response.businessJournal.id;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function(){
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.href = "#/journal_details/" + $id;
                                    }, 500);
                                });
                        } else {
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    $scope.main.error_message = data.error;
                }
            });
        }
        
        // Get Journal Details
        $scope.getJournalDetails = function () {
            if($routeParams.id){
                journalServices.getJournalDetails($routeParams.id).then(function (res) {
                $scope.journal_item_list = res.response.businessJournal.business_journal_item;
                for(var i = 0; i< $scope.journal_item_list.length; i++){
                    $scope.journal_item_list[i].credit = parseFloat($scope.journal_item_list[i].credit);
                    $scope.journal_item_list[i].debit = parseFloat($scope.journal_item_list[i].debit);
                }
                $scope.journal = res.response.businessJournal;
                $scope.journal.total_credit = parseFloat($scope.journal.total_credit);
                $scope.journal.total_debit = parseFloat($scope.journal.total_debit);
                $scope.totalCredit = $scope.journal.total_credit;
                $scope.totalDebit = $scope.journal.total_debit;
            })   
            }
        }
        $scope.getJournalDetails();

        // Delete journal
        $scope.delete = function (id) {
            $scope.main.error_message = "";
            journalServices.delete(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.reload();
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }

        // Update journal
        $scope.updateJournal = function () {
            var isValidResult = isValid();
            if(isValidResult.status == false){
                alert(isValidResult.message);
                $scope.main.error_message = isValidResult.message;
                return;
            }
            $scope.main.error_message = "";
            journalServices.updateJournal($scope.journal, $scope.journal_item_list).then(function (res) {
                console.log(res);
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/journals";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }




    }]);