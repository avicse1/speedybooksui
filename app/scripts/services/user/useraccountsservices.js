'use strict';

angular.module(projectAppName)

.factory('accountServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	var service = {};
        service.getAccountList = function () {
        	var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
        	 return deferred.promise;
        };

        service.changePassword = function(user){
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'users/change/password',
                method: "POST",
                dataType: "json",
                data: {email: user.email, password: user.newPassword, currentPassword: user.currentPassword},
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
             return deferred.promise;
        }

        service.addAccount = function(account) {
            var globals = $cookieStore.get('globals');
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts',
                method: "POST",
                data: { parent_id: account.parent_id, code:account.code, name: account.name, description: account.description, is_subaccount:account.is_subaccount, detail_type_id:account.detail_type_id, balance:account.balance,
                    track_depreciation:account.track_depreciation, original_cost:account.original_cost, depreciation_cost:account.depreciation_cost, depreciation_asof:account.depreciation_asof, depreciation_at:account.depreciation_at, user_id: account.user_id },
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            })
                .error(function(data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        service.getAccountDetails = function (accountId) {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts/' + accountId,
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        service.updateAccount = function(accountId, account) {
            var globals = $cookieStore.get('globals');
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts/' + accountId,
                method: "PUT",
                data: { parent_id: account.parent_id, code:account.code, name: account.name, description: account.description, detail_type:account.detail_type_id, balance:account.balance },
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            })
                .error(function(data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        service.deleteAccount = function (accountId) {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts/' + accountId,
                method: "DELETE",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        service.getParentAccountList = function () {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts/parent/list',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };


        service.getReceivePaymentAccountList = function () {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts/receive/payment/list',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        service.getDetailTypeList = function (parentAccountId) {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'accounts/detail/type/list/' + parentAccountId,
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };
         
        return service;
        
 }])
 