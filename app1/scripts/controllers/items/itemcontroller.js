'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Item Controller
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)

.controller("itemCtrl", ["$scope", "$modal", "$log", "$filter", "itemServices", "accountServices", "$cookieStore", "$ngBootbox", "$route", "$routeParams", function($scope, $modal, $log, $filter, itemServices, accountServices, $cookieStore, $ngBootbox, $route, $routeParams) {

    $scope.itemsList = {};
    $scope.searchKeywords = "";
    $scope.filteredStores = [];
    $scope.row = "";
    $scope.currentPage = 1;
    $scope.accounts = {};

    $scope.Item = {
        id: "",
        item_code: "",
        name: "",
        account_id: "",
        amount: "",
        description: "",
        unit: "",
        selling_price: ""
    };

    $scope.globals = $cookieStore.get('globals');
    $('.username .text').text($scope.globals.currentUser.username);

    if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
        $scope.main.error_message = "Please complete your business profile before creating invoice.";
        window.location.href = "#/business_profile";
    }

    $scope.open = function() {
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: "addItemPopup.html",
            controller: "addItemPopupCtrl"
        }), modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function() {
        })
    }

    $scope.addNewItem = function() {
                console.log("DDDFFF")

        itemServices.addNewItem($scope.addItem).then(function(res) {
            if(res.code == 200) {
                $scope.alert_message = "Item Added Successfully!!";
                setTimeout(function() {
                    itemServices.getItems().then(function(res) {
                        $scope.items = res.response.businessItems;
                    });
                    window.location.href = "#/inventory";
                }, 2000);
            }
        }, function(res) {
            $scope.error_message = res.message;
        });
    };

    $scope.cancel = function() {
        window.location.href = "#/inventory";
    }

    itemServices.getPayItems().then(function(res) {
        $scope.payItems = res.response.businessPayItems;
    });

    $scope.openPayItem = function () {
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: "views/items/add_new_pay_item.html",
            controller: "addItemPopupCtrl",
            resolve: { // This fires up before controller loads and templates rendered
                payItem_id : function() {
                    return null;
                }
            }
        }), modalInstance.result.then(function(selectedItem) {
            $scope.selected = selectedItem
        }, function () {
            itemServices.getPayItems().then(function(res) {
                $scope.payItems = res.response.businessPayItems;
            });
            itemServices.getPayItemTypes().then(function (res) {
                $scope.payItemTypes = res.response.businessPayItemTypes;
            });
        })
    }

    itemServices.getItems().then(function(res) {
        $scope.itemsList = res.response.businessItems;
        $scope.search();
        $scope.select($scope.currentPage)
    });

    accountServices.getAccountList().then(function (res) {
        $scope.accounts = res.response.accounts;
    })

    $scope.onFilterChange = function() {
        return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
    };

    $scope.onNumPerPageChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };

    $scope.onOrderChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };

    $scope.search = function() {
        return $scope.filteredStores = $filter("filter")($scope.itemsList, $scope.searchKeywords), $scope.onFilterChange()
    };

    $scope.order = function(rowName) {
        return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.itemsList, rowName), $scope.onOrderChange()) : void 0
    };

    $scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        $scope.currentPageStores = $scope.filteredStores.slice(start, end);
    }

    $scope.numPerPageOpt = [3, 5, 10, 20];
    $scope.numPerPage = $scope.numPerPageOpt[2];
    $scope.currentPage = 1;
    $scope.currentPageStores = [];

    $scope.delete = function (id) {
        $scope.main.error_message = "";
        itemServices.deleteItem(id).then(function (res) {
            if(res.code == 200) {
                $scope.main.alert = res.message;
                $ngBootbox.alert($scope.main.alert)
                    .then(function() {
                        setTimeout(function(){
                            //$window.scrollTo(0, 0);
                            $scope.main.alert = "";
                            $scope.main.error_message = "";
                            $route.reload();
                        }, 500);
                    });
            } else {
                //$window.scrollTo(0, 0);
                $scope.main.error_message = res.message;
            }
        })
    }

    $scope.getItem = function () {
        itemServices.getItem($routeParams.id).then(function (res) {
            if(res.code == 200) {
                $scope.Item.id = res.response.businessItem.id;
                $scope.Item.item_code = res.response.businessItem.item_code;
                $scope.Item.name = res.response.businessItem.name;
                $scope.Item.account_id = parseInt(res.response.businessItem.account_id);
                $scope.Item.amount = res.response.businessItem.amount;
                $scope.Item.description = res.response.businessItem.description;
                $scope.Item.unit = res.response.businessItem.unit;
                $scope.Item.selling_price = res.response.businessItem.selling_price;
            }
        })
    }

    $scope.updateItem = function () {
        $scope.main.error_message = "";
        itemServices.updateItem($scope.Item).then(function (res) {
            if(res.code == 200) {
                $scope.main.alert = res.message;
                $ngBootbox.alert($scope.main.alert)
                    .then(function() {
                        setTimeout(function(){
                            //$window.scrollTo(0, 0);
                            $scope.main.alert = "";
                            $scope.main.error_message = "";
                            window.location.href = "#/inventory";
                        }, 500);
                    });
            } else {
                //$window.scrollTo(0, 0);
                $scope.main.error_message = res.message;
            }
        })
    }
}])

.controller("addItemPopupCtrl", ["$scope", "$modalInstance", "itemServices", "accountServices", function($scope, $modalInstance, itemServices, accountServices) {
    $scope.addItem = {
        item_code: "",
        name: "",
        account_id: "",
        amount: "",
        description: "",
        unit: "",
        selling_price: ""
    };

    $scope.items = {};

    $scope.accounts = {};
    accountServices.getAccountList().then(function(res) {
        $scope.accounts = res.response.accounts;
    });

    $scope.addItem.account_id = "";
    $scope.addNewItem = function() {
        console.log("DDD")
        itemServices.addNewItem($scope.addItem).then(function(res) {
            $modalInstance.dismiss("ok");
            itemServices.getItems().then(function(res) {
                $scope.items = res.response.businessItems;
            });
        }, function(res) {
            $scope.error_message = res.message;
        });
    }, $scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }

    // add Pay items
    $scope.addPayItem = {
        business_pay_item_type_id :"",
        account_id:"",
        name:"",
        description:"",
    };

    $scope.payItemTypes = {};
    $scope.addPayItem.business_pay_item_type_id = "";

    itemServices.getPayItemTypes().then(function (res) {
       $scope.payItemTypes = res.response.businessPayItemTypes;
    });

    $scope.addNewPayItem = function() {
        itemServices.addNewPayItem($scope.addPayItem).then(function(res) {
            if(res.code == 200) {
                $modalInstance.dismiss("ok");
            }
        }, function(res) {
            $scope.error_message = "Please fill all fields with valid values.";
        });
    }, $scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }
}]);