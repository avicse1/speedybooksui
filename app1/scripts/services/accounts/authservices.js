"use strict";

angular.module(projectAppName)

.factory('AuthenticationService',
    ['Base64', '$cookieStore', '$http', '$rootScope', '$q', '$timeout',
    function (Base64, $cookieStore, $http, $rootScope, $q, $timeout) {
        var service = {};
        service.Login = function (username, password, callback) {
            $('#mask').show();
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'users/authenticate',
                method: "POST",
                data: { username: username, password: password},
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
               deferred.resolve(data);
            })
            .error(function(data) {
                deferred.reject(data);
            });
            return deferred.promise;
        };
        
        service.createPassword = function(password, token, email) {
            $('#mask').show();
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'users/password',
                method: "POST",
                data: { password: password, token: token, email: email },
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).success(function(data){
                $('#mask').hide();
               deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data);
            });
            return deferred.promise;
        };

        service.sendForgotPasswordRequest = function(email) {
            $('#mask').show();
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'forgot/password',
                method: "POST",
                data: { email: email },
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
               deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data);
            });
            return deferred.promise;
        };

        service.SignUp = function(formData){
            $('#mask').show();
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'users',
                method: "POST",
                data: { first_name : formData.first_name, last_name : formData.last_name, name : formData.first_name, email : formData.email, phone : formData.phone, country_id : formData.country_id, state_id : formData.state_id, city_id : formData.city_id, role : "USERS", password : "hello1244", roles:formData.roles},
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
               deferred.resolve(data);
            }).error(function(data) {
                deferred.reject(data);
            });
            return deferred.promise;
        };
 
        service.SetCredentials = function (response) {
            var authdata = response.token;
 
            $rootScope.globals = {
                currentUser: {
                    username: response.firstName + ' ' + response.lastName,
                    userId: response.id,
                    role: response.role,
                    email: response.email,
                    accountStatus:response.accountStatus,
                    expiryDate:response.expiryDate,
                    manageUsers:response.inviteUserCount,
                    businessProfile: response.businessProfile.id,
                    businessName: response.businessProfile.name,
                    businessStatus: response.businessProfile.status,
                    companyId: response.businessProfile.company_id,
                    userPermission: response.userPermission,
                    authdata: authdata,
                    modalInstance: null
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Bearer ' + authdata; // jshint ignore:line
            if(response.id != null) {
                $cookieStore.put('globals', $rootScope.globals);
            }
        };
 
        service.ClearCredentials = function () {
            $rootScope.globals = null;
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        };

        service.getInviteUser = function (token) {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + '/user-permissions/signup/' + token,
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                deferred.resolve(data);
            }).error(function(data) {
                deferred.resolve(data);
            });
            return deferred.promise;
        };
 
        return service;
    }])
 
.factory('Base64', function () {
    /* jshint ignore:start */
 
    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
 
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
 
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
 
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
 
                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
 
            return output;
        },
 
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
 
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
 
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
 
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
 
                output = output + String.fromCharCode(chr1);
 
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
 
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
 
            } while (i < input.length);
 
            return output;
        }
    };
 
    /* jshint ignore:end */
});