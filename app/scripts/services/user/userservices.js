'use strict';

angular.module(projectAppName)

.factory('userServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	$rootScope.globals = $cookieStore.get('globals');
    	$rootScope.userLoggedIn = false;
        var service = {};
        
        service.getUserProfileData = function () {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl + 'users/' + $rootScope.globals.currentUser.userId,
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
	            	$('#mask').hide();
	                deferred.resolve(data);
	            }).error(function(data){
	            	deferred.reject(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

		service.userBusinessProfileData = function (profileId) {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl + 'business/profiles/' + profileId,
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
					  $('#mask').hide();
	                deferred.resolve(data);              
	            }).error(function(data){
	            	deferred.reject(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.skippedBusineesProfile = function () {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl,
	                method: "POST",
	                data: { userId: $rootScope.globals.currentUser.userId, form: 'skipped' },
	                dataType: "json",
	                withCredentials: true,
	                headers: {
	                	'Content-Type': 'application/json; charset=utf-8'
	                }
	            }).
	            success(function(data){
	            	$('#mask').hide();
	               deferred.resolve(data.data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.saveBusinessProfile = function (b_profile, is_skipped) {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	        		if(b_profile.businessProfileId != 0) {
	        			$http({
			                url: webServerUrl + 'business/profiles/'+b_profile.businessProfileId,
			                method: "PUT",
			                data: { name: b_profile.name, is_skipped: is_skipped, country_id: b_profile.country_id, state_id: b_profile.state_id, financial_year_id: b_profile.financial_year_id, currency_id: b_profile.currency_id,
									business_nature_id: b_profile.business_nature_id, registered_address: b_profile.registered_address, logo:b_profile.logo,
									physical_address:b_profile.physical_address, industry:b_profile.industry, registration_no:b_profile.registration_no, description:b_profile.description,
									telephone_no:b_profile.telephone_no, email:b_profile.email, website:b_profile.website},
			                dataType: "json",
			                withCredentials: false,
			                headers: {
								'Content-Type': 'application/json; charset=utf-8',
								'Accept' : 'application/vnd.speedybooks.v1+json'
			                }
			            }).
			            success(function(data){
							$('#mask').hide();
			               deferred.resolve(data);
			            }).error(function(data){
			            	deferred.reject(data);
			            });

	        		} else {
	        			$http({
			                url: webServerUrl + 'business/profiles',
			                method: "POST",
			                data: { name: b_profile.name, country_id: b_profile.country_id, state_id: b_profile.state_id, financial_year_id: b_profile.financial_year_id, currency_id: b_profile.currency_id, business_nature_id: b_profile.business_nature_id },
			                dataType: "json",
			                withCredentials: false,
			                headers: {
			                            'Content-Type': 'application/json; charset=utf-8',
			                            'Accept' : 'application/vnd.speedybooks.v1+json'
			                }
			            }).
			            success(function(data) {
							$('#mask').hide();
			               deferred.resolve(data);
			            }).error(function(data){
			            	deferred.reject(data);
			            });
					}
	            } else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.getCustomerList = function () {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl + 'customers',
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
					  $('#mask').hide();
	               deferred.resolve(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.getVendorsList = function () {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl + 'vendors',
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
					  $('#mask').hide();
	               deferred.resolve(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.getUserList = function () {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl + 'users',
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
					  $('#mask').hide();
	               deferred.resolve(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

		service.addNewCustomer = function (userdetails, businessProfileId) {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl + 'customers',
	                method: "POST",
	                data: { business_profile_id: businessProfileId, title:userdetails.title, first_name: userdetails.first_name, last_name: userdetails.last_name, email: userdetails.email, phone: userdetails.phone, mobile:userdetails.mobile, company_name:userdetails.company_name,
							website:userdetails.website, billing_street:userdetails.billing_street, billing_zip:userdetails.billing_zip, billing_city:userdetails.billing_city, billing_state:userdetails.billing_state, billing_country:userdetails.billing_country,
							notes:userdetails.notes, shipping_street:userdetails.shipping_street, shipping_zip:userdetails.shipping_zip, shipping_city:userdetails.shipping_city, shipping_state:userdetails.shipping_state, shipping_country:userdetails.shipping_country,
							attachment:userdetails.attachment
					},
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
					  $('#mask').hide();
	               deferred.resolve(data);
	            }).error(function(data){
	            	deferred.reject(data);
	            });;
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.addNewVendor = function (userdetails, businessProfileId) {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl + 'vendors',
	                method: "POST",
					  data: { business_profile_id: businessProfileId, title:userdetails.title, first_name: userdetails.first_name, last_name: userdetails.last_name, email: userdetails.email, phone: userdetails.phone, mobile:userdetails.mobile, company_name:userdetails.company_name,
						  website:userdetails.website, billing_street:userdetails.billing_street, billing_zip:userdetails.billing_zip, billing_city:userdetails.billing_city, billing_state:userdetails.billing_state, billing_country:userdetails.billing_country,
						  notes:userdetails.notes, shipping_street:userdetails.shipping_street, shipping_zip:userdetails.shipping_zip, shipping_city:userdetails.shipping_city, shipping_state:userdetails.shipping_state, shipping_country:userdetails.shipping_country,
						  attachment:userdetails.attachment, suppliers:userdetails.suppliers
					  },
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
					  $('#mask').hide();
	               deferred.resolve(data);
	            }).error(function(data){
	            	deferred.reject(data);
	            });;
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

		service.uploadFile = function (file) {
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				$http({
					url: webServerUrl + 'files',
					method: "POST",
					withCredentials: false,
					headers: {'Content-Type': undefined,
						'Accept' : 'application/vnd.speedybooks.v1+json'
					},
					transformRequest: function () {
						var formdata = new FormData();
						formdata.append("file", file);
						return formdata;
					},
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				}).error(function(data){
					deferred.reject(data);
				});;
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		};

		service.deleteCustomer = function (customerId) {
			$('#mask').show();
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				$http({
					url: webServerUrl + 'customers/' + customerId,
					method: "DELETE",
					dataType: "json",
					withCredentials: false,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Accept' : 'application/vnd.speedybooks.v1+json'
					}
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				});
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		};

        service.getCustomerDetails = function(id) {
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'customers/'+id,
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.updateCustomer = function (userdetails, businessProfileId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'customers/' + userdetails.id,
                    method: "PUT",
					data: { business_profile_id: businessProfileId, title:userdetails.title, first_name: userdetails.first_name, last_name: userdetails.last_name, email: userdetails.email, phone: userdetails.phone, mobile:userdetails.mobile, company_name:userdetails.company_name,
						website:userdetails.website, billing_street:userdetails.billing_street, billing_zip:userdetails.billing_zip, billing_city:userdetails.billing_city, billing_state:userdetails.billing_state, billing_country:userdetails.billing_country,
						notes:userdetails.notes, shipping_street:userdetails.shipping_street, shipping_zip:userdetails.shipping_zip, shipping_city:userdetails.shipping_city, shipping_state:userdetails.shipping_state, shipping_country:userdetails.shipping_country,
						attachment:userdetails.attachment
					},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.deleteVendor = function (vendorId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'vendors/' + vendorId,
                    method: "DELETE",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.getVendorDetails = function(id) {
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'vendors/'+id,
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.updateVendor = function (userdetails, businessProfileId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'vendors/' + userdetails.id,
                    method: "PUT",
					data: { business_profile_id: businessProfileId, title:userdetails.title, first_name: userdetails.first_name, last_name: userdetails.last_name, email: userdetails.email, phone: userdetails.phone, mobile:userdetails.mobile, company_name:userdetails.company_name,
						website:userdetails.website, billing_street:userdetails.billing_street, billing_zip:userdetails.billing_zip, billing_city:userdetails.billing_city, billing_state:userdetails.billing_state, billing_country:userdetails.billing_country,
						notes:userdetails.notes, shipping_street:userdetails.shipping_street, shipping_zip:userdetails.shipping_zip, shipping_city:userdetails.shipping_city, shipping_state:userdetails.shipping_state, shipping_country:userdetails.shipping_country,
						attachment:userdetails.attachment, suppliers:userdetails.suppliers
					},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data) {
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        return service;
 }])
 
