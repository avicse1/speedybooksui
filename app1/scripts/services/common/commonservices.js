'use strict';

angular.module(projectAppName)

.factory('commonServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	var service = {};
        service.getCountries = function () {
        	var deferred = $q.defer();
              $http({
                url: webServerUrl + 'location/country',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data.response.countries);
            });
        	 return deferred.promise;
        };
        service.getAccounts = function () {
            var deferred = $q.defer();
            var data = {"code": "200", "response": {"accounts": [{"id": "1","name": "Sales"},{"id": "2","name": "Interest"}, {"id": "3","name": "Income"}]}};
             /* $http({
                url: webServerUrl + 'location/country',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){ */
            deferred.resolve(data);
            //});
             return deferred.promise;
        };
         service.getStates = function (country_id) {
        	var deferred = $q.defer();
              $http({
                url: webServerUrl + 'location/country/' + country_id,
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data.response.states);
            });
        	 return deferred.promise;
        };
         service.getCities = function (state_id) {
        	var deferred = $q.defer();
              $http({
                url: webServerUrl + 'location/state/' + state_id,
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
               	deferred.resolve(data.response.cities);
            });
        	 return deferred.promise;
        };
         service.getCurrency = function () {
        	var deferred = $q.defer();
              $http({
                url: webServerUrl + 'currency',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                	deferred.resolve(data.response.currencies);
            });
        	 return deferred.promise;
        };

        service.getBusinessNature = function () {
            var deferred = $q.defer();
              $http({
                url: webServerUrl + 'businessNatures',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                    deferred.resolve(data.response.businessNatures);
            });
            return deferred.promise;
        };

            service.getFY = function () {
            var deferred = $q.defer();
              $http({
                url: webServerUrl + 'financialYears',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                    deferred.resolve(data.response.financialYears);
            });
             return deferred.promise;
        };

        service.getDateFun = function() {
        var deferred = $q.defer();
        var monthNames = [
          "January", "February", "March",
          "April", "May", "June", "July",
          "August", "September", "October",
          "November", "December"
        ];

        var date = new Date();
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        deferred.resolve(day + ' ' + monthNames[monthIndex] + ' ' + year);
        return deferred.promise;
    };

        return service;
        
 }])
 