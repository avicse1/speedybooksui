'use strict';

angular.module(projectAppName)
    .factory('reimbursementServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};
            service.getPaymentTermList = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl,
                        method: "POST",
                        data: { form: 'term_list' },
                        dataType: "json",
                        withCredentials: true,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data.data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getReimbursementList = function() {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reimbursement',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getReimbursementDetails = function(reimbursement_id) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reimbursement/'+reimbursement_id,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getReimbursementNumber = function() {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reimbursement/generate',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.saveReimbursement = function (reimbursementData, itemList, businessProfileId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    var Notes = [];
                    angular.forEach(reimbursementData.notes, function(selected){
                        if(selected.Notes != "") {
                            Notes.push(selected.Notes);
                        }
                    });
                    $http({
                        url: webServerUrl + 'business/reimbursement',
                        method: "POST",
                        data: { discount: reimbursementData.discount, description:reimbursementData.description, tax_amount: reimbursementData.taxAmount, tax_rate_id:reimbursementData.tax_rate_id, status: reimbursementData.status, send_email_to: reimbursementData.sendEmailTo, Notes: Notes, amount: reimbursementData.totalAmount, customer_id: reimbursementData.toUser, business_profile_id: businessProfileId, reimbursement_no: reimbursementData.reimbursementNo, date: reimbursementData.date, reference: reimbursementData.reference, Items: itemList, attachment:reimbursementData.attachment},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.delete = function (reimbursementId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reimbursement/' + reimbursementId,
                        method: "DELETE",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            return service;
    }])

