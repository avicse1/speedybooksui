'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:userCtrl
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
 

.controller("userCtrl", ["$scope", "$modal", "$rootScope", "userServices", "$routeParams", "$cookieStore", "$location", "commonServices", "$filter", "$ngBootbox", function($scope, $modal, $rootScope, userServices, $routeParams, $cookieStore, $location, commonServices, $filter, $ngBootbox) {
    $scope.b_profile = {};
    $scope.globals = $cookieStore.get('globals');
    $scope.businessProfileId = $scope.globals.currentUser.businessProfile;
    $('.username .text').text($scope.globals.currentUser.username);

    $scope.searchKeywords = "";
    $scope.filteredStores = [];
    $scope.row = "";
    $scope.currentPage = 1;
    $scope.customer = {};
    $scope.vendor = {};


    $scope.getVendorList = function () {
        userServices.getVendorsList().then(function (res) {
            $scope.users = res.response.vendors;
            $scope.search();
            $scope.select($scope.currentPage)
        })
    }

    $scope.getCustomerList = function () {
        userServices.getCustomerList().then(function (res) {
            $scope.users = res.response.customers;
            $scope.search();
            $scope.select($scope.currentPage);
        })
    }

    $scope.getUserList = function () {
        userServices.getUserList().then(function (res) {
            $scope.users = res.response.users;
            $scope.search();
            $scope.select($scope.currentPage);
        })
    }

    $scope.onFilterChange = function() {
        return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
    };
    $scope.onNumPerPageChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };
    $scope.onOrderChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };
    $scope.search = function() {
        return $scope.filteredStores = $filter("filter")($scope.users, $scope.searchKeywords), $scope.onFilterChange()
    };

    $scope.order = function(rowName) {
        return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.users, rowName), $scope.onOrderChange()) : void 0
    };

    $scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        $scope.currentPageStores = $scope.filteredStores.slice(start, end);
    }

    $scope.numPerPageOpt = [3, 5, 10, 20];
    $scope.numPerPage = $scope.numPerPageOpt[2];
    $scope.currentPage = 1;
    $scope.currentPageStores = [];


    $scope.$watch('customer.country_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.states = null;
            $scope.cities = null;
            commonServices.getStates(changed).then(function(res) {
                $scope.states = res;
            });
        }
    });

    $scope.addNewCustomer = function() {
        userServices.addNewCustomer($scope.customer, $scope.businessProfileId).then(
        function(res) {
         if(res.code == 200) {
                $scope.alert_message = "New Customer Added Successfully!!";
                $ngBootbox.alert($scope.alert_message)
                     .then(function() {
                         setTimeout(function(){
                             //$window.scrollTo(0, 0);
                             $scope.alert_message = "";
                             window.location.reload();
                         }, 500);
                     });
            } else {
                $scope.main.error_message = "All fields are required.";
            }
        }, function(res) {
            $scope.main.error_message = "All fields are required.";
        });
    };

    $scope.setShippingAddress = function () {
        if($scope.customer.same_as_billing == true) {
            $scope.customer.shipping_street = $scope.customer.billing_street;
            $scope.customer.shipping_zip = $scope.customer.billing_zip;
            $scope.customer.shipping_city = $scope.customer.billing_city;
            $scope.customer.shipping_state = $scope.customer.billing_state;
            $scope.customer.shipping_country = $scope.customer.billing_country;
        } else {
            $scope.customer.shipping_street = '';
            $scope.customer.shipping_zip = '';
            $scope.customer.shipping_city = '';
            $scope.customer.shipping_state = '';
            $scope.customer.shipping_country = '';
        }
    }

    $scope.addNewVendor = function() {
        userServices.addNewVendor($scope.customer, $scope.businessProfileId).then(
            function(res) {
                if(res.code == 200) {
                   $scope.alert_message = "New Vendor Added Successfully!!";
                    $ngBootbox.alert($scope.alert_message)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.alert_message = "";
                                window.location.reload();
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = "All fields are required.";
                }
            }, function(res) {
                $scope.main.error_message = "All fields are required.";
            });
    };
    
    $scope.deleteCustomer = function (id) {
        $scope.main.error_message = "";
        userServices.deleteCustomer(id).then(function (res) {
            if(res.code == 200) {
                $scope.main.alert = res.message;
                $ngBootbox.alert($scope.main.alert)
                    .then(function() {
                        setTimeout(function(){
                            //$window.scrollTo(0, 0);
                            $scope.main.alert = "";
                            $scope.main.error_message = "";
                        }, 500);
                    });
            } else {
                //$window.scrollTo(0, 0);
                $scope.main.error_message = res.message;
            }
        })
    }
    
    $scope.getCustomerDetails = function () {
        userServices.getCustomerDetails($routeParams.id).then(function (res) {
            if(res.code == 200) {
                $scope.customer = res.response.customer;
            }
        })
    }
    
    $scope.editCustomer = function () {
        userServices.updateCustomer($scope.customer, $scope.businessProfileId).then(
            function(res) {
                if(res.code == 200) {
                    $scope.alert_message = "Customer updated Successfully!!";
                    $ngBootbox.alert($scope.alert_message)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.alert_message = "";
                                window.location.reload();
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = "All fields are required.";
                }
            }, function(res) {
                $scope.main.error_message = "All fields are required.";
            });
    }

    $scope.deleteVendor = function (id) {
        $scope.main.error_message = "";
        userServices.deleteVendor(id).then(function (res) {
            if(res.code == 200) {
                $scope.main.alert = res.message;
                $ngBootbox.alert($scope.main.alert)
                    .then(function() {
                        setTimeout(function(){
                            //$window.scrollTo(0, 0);
                            $scope.main.alert = "";
                            $scope.main.error_message = "";
                        }, 500);
                    });
            } else {
                //$window.scrollTo(0, 0);
                $scope.main.error_message = res.message;
            }
        })
    }

    $scope.getVendorDetails = function () {
        userServices.getVendorDetails($routeParams.id).then(function (res) {
            if(res.code == 200) {
                $scope.customer = res.response.vendor;
            }
        })
    }

    $scope.editVendor = function () {
        userServices.updateVendor($scope.customer, $scope.businessProfileId).then(
            function(res) {
                if(res.code == 200) {
                    $scope.alert_message = "Vendor updated Successfully!!";
                    $ngBootbox.alert($scope.alert_message)
                        .then(function() {
                            setTimeout(function(){
                               //$window.scrollTo(0, 0);
                                $scope.alert_message = "";
                                window.location.reload();
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = "All fields are required.";
                }
            }, function(res) {
                $scope.main.error_message = "All fields are required.";
            });
    }

    $scope.$watch('customer.state_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.cities = null;
            commonServices.getCities(changed).then(function(res) {
                $scope.cities = res;
            });
        }
    });

    $scope.$watch('vendor.country_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.states = null;
            $scope.cities = null;
            commonServices.getStates(changed).then(function(res) {
                $scope.states = res;
            });
        }
    });

    $scope.$watch('vendor.state_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.cities = null;
            commonServices.getCities(changed).then(function(res) {
                $scope.cities = res;
            });
        }
    });

    $scope.userProfileData = function () {
        $rootScope.globals = $cookieStore.get('globals');
        $scope.userData = {};

        userServices.getUserProfileData().then(
            function(res) {
                if(res.response.user.businessProfile.status == 'PENDING') {
                    window.location.href = "#/business_profile";
                }
                $rootScope.globals.currentUser.businessName = res.response.user.businessProfile.name;
                $rootScope.globals.currentUser.businessProfile = res.response.user.businessProfile.id;
                $rootScope.globals.currentUser.businessStatus = res.response.user.businessProfile.status;
                $cookieStore.put('globals', $rootScope.globals);
            }, function(res){
                $scope.main.error_message = res.message;
            });
    };

    $scope.userBusinessProfileData = function () {
        $rootScope.globals = $cookieStore.get('globals');
        commonServices.getCurrency().then(function(res) {
            $scope.currencies = res;
        });
        commonServices.getBusinessNature().then(function(res) {
            $scope.businessNatures = res;
        });
         commonServices.getFY().then(function(res) {
            $scope.financialYears = res;
        });
        if($rootScope.globals.currentUser.businessProfile != 0) {
            userServices.userBusinessProfileData($rootScope.globals.currentUser.businessProfile).then(
                function(res) {
                    if(res.code == 200) {
                        $scope.b_profile.name = res.response.businessProfile.name;
                        $scope.b_profile.registered_address = res.response.businessProfile.registered_address;
                        $scope.b_profile.physical_address = res.response.businessProfile.physical_address;
                        $scope.b_profile.industry = res.response.businessProfile.industry;
                        $scope.b_profile.registration_no = res.response.businessProfile.registration_no;
                        $scope.b_profile.description = res.response.businessProfile.description;
                        $scope.b_profile.telephone_no = res.response.businessProfile.telephone_no;
                        $scope.b_profile.email = res.response.businessProfile.email;
                        $scope.b_profile.website = res.response.businessProfile.website;
                        $scope.b_profile.logo = res.response.businessProfile.logo;
                        $scope.b_profile.logourl = res.response.businessProfile.logourl;
                        $scope.b_profile.country_id = res.response.businessProfile.country_id == 0?"":res.response.businessProfile.country_id;
                        $scope.b_profile.state_id = res.response.businessProfile.state_id == 0?"":res.response.businessProfile.state_id;
                        $scope.b_profile.financial_year_id = res.response.businessProfile.financial_year_id == 0?"":res.response.businessProfile.financial_year_id;
                        $scope.b_profile.currency_id = res.response.businessProfile.currency_id == 0?"":res.response.businessProfile.currency_id;
                        $scope.b_profile.business_nature_id = res.response.businessProfile.business_nature_id == 0?"":res.response.businessProfile.business_nature_id;
                    }
                });
        }
    }

    $scope.$watch('b_profile.country_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.b_states = null;
            var state_id = $scope.b_profile.state_id;
            commonServices.getStates(changed).then(function(res) {
                $scope.b_states = res;
                setTimeout(function(){
                    $scope.b_profile.state_id = state_id;
                }, 1000);
            });
        }
    });
        	
    $scope.skipped = function() {
        $scope.main.error_message = "";
        $scope.main.alert = "";
        $rootScope.globals = $cookieStore.get('globals');
        $scope.b_profile.businessProfileId = $rootScope.globals.currentUser.businessProfile;
        userServices.saveBusinessProfile($scope.b_profile, 1).then(
        function(res) {
            if(res.code == 200) {
                $scope.userProfileData();
                $scope.userData = res;
                window.location.href = "#/dashboard";
            }
        }, function(res){
               $scope.main.error_message = "All fields are required.";
        });
    };

    $scope.uploadFile = function(files) {
        if(files != null) {
            $scope.file = files[0];
            userServices.uploadFile($scope.file).then(function (res) {
                $scope.b_profile.logo = res.attachment;
            })
        }
    };

    $scope.attachmentUpload = function(files) {
        if(files != null) {
            $scope.file = files[0];
            userServices.uploadFile($scope.file).then(function (res) {
                $scope.customer.attachment = res.attachment;
            })
        }
    };

    $scope.setPhysicalAddress = function () {
        if($scope.b_profile.same_as_registered == true) {
            $scope.b_profile.physical_address = $scope.b_profile.registered_address;
        } else {
            $scope.b_profile.physical_address = '';
        }
    }

    $scope.saveBusinessProfile = function() {
        $scope.main.error_message = "";
        $scope.main.alert = "";
        $rootScope.globals = $cookieStore.get('globals');
        $scope.b_profile.businessProfileId = $rootScope.globals.currentUser.businessProfile;
        userServices.saveBusinessProfile($scope.b_profile, 0).then(
        function(res) {
             if(res.code == 200) {
                 $scope.userProfileData();
                 $scope.userData = res;
                 window.location.href = "#/dashboard";
            } else {
                 $scope.main.error_message = "All fields are required.";
            }

        }, function(res) {
               $scope.main.error_message = "All fields are required.";
        });
    };
}])

.controller("userCtrlPopup", ["$scope", "$modalInstance", "$location", "$cookieStore", "userServices", "commonServices", function($scope, $modalInstance, $location, $cookieStore, userServices, commonServices) {
    $scope.customer = {};
    $scope.vendor = {};
    $scope.globals = $cookieStore.get('globals');
    $scope.businessProfileId = $scope.globals.currentUser.businessProfile;

    $scope.addNewCustomer = function() {
        userServices.addNewCustomer($scope.customer, $scope.businessProfileId).then(
            function(res) {
                if(res.code == 200) {
                    $modalInstance.dismiss("ok");
                } else {
                    $scope.main.error_message = "All fields are required.";
                }
        }, function(res) {
            $scope.main.error_message = "All fields are required.";
        });
    };

    $scope.addNewVendor = function() {
        userServices.addNewVendor($scope.vendor, $scope.businessProfileId).then(
            function(res) {
                if(res.code == 200) {
                    $modalInstance.dismiss("ok");
                } else {
                    $scope.main.error_message = "All fields are required.";
                }
            }, function(res) {
                $scope.main.error_message = "All fields are required.";
            });
    };

    $scope.getCountry = function() {
        commonServices.getCountries().then(function(res) {
            $scope.countries = res;
        });
    };

    $scope.cancel = function() {
        $modalInstance.dismiss("cancel");
    };

    $scope.$watch('customer.country_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.states = null;
            $scope.cities = null;
            commonServices.getStates(changed).then(function(res) {
                $scope.states = res;
            });
        }
    });

    $scope.$watch('customer.state_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.cities = null;
            commonServices.getCities(changed).then(function(res) {
                $scope.cities = res;
            });
        }
    });

    $scope.$watch('vendor.country_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.states = null;
            $scope.cities = null;
            commonServices.getStates(changed).then(function(res) {
                $scope.states = res;
            });
        }
    });

    $scope.$watch('vendor.state_id', function(changed) {
        if(changed != undefined && changed != "") {
            $scope.cities = null;
            commonServices.getCities(changed).then(function(res) {
                $scope.cities = res;
            });
        }
    });
}])
.controller("companyAccountCtrl", ["$scope", "$location", "$rootScope", "$cookieStore", function($scope, $location, $rootScope, $cookieStore) {
    $scope.globals = $cookieStore.get('globals');
    $('.username .text').text($scope.globals.currentUser.username);

    if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
        $scope.main.error_message = "Please complete your business profile before creating invoice.";
        window.location.href = "#/business_profile";
    }
}]);
           