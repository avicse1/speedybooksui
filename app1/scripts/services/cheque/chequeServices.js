/**
 * Created by root on 4/4/17.
 */
'use strict';

angular.module(projectAppName)
    .factory('chequeServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};

            service.getChequeList = function() {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/cheques',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getChequeDetails = function(journalId) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/cheques/' + journalId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.saveCheque = function (chequeData, businessProfileId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/cheques',
                        method: "POST",
                        data: { cheque_no: chequeData.cheque_no, date: chequeData.date, vendor_id:chequeData.vendor_id, account_id: chequeData.account_id, business_profile_id: businessProfileId,
                               payment_term:chequeData.payment_term, status:chequeData.status, description:chequeData.description, amount:chequeData.amount, item_account_id: chequeData.item_account_id
                        },
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.delete = function (chequeId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/cheques/' + chequeId,
                        method: "DELETE",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            return service;
        }])

