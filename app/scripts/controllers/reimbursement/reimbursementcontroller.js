'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Reimbursement Controller
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("reimbursementCtrl", ["$scope", "$window", "$filter", "$routeParams", "userServices", "validator", "$modal", "reimbursementServices", "$cookieStore", "accountServices", "commonServices", "$location", "itemServices", "$ngBootbox", "taxRateServices", "expenseServices", function($scope, $window, $filter, $routeParams, userServices, validator, $modal, reimbursementServices, $cookieStore, accountServices, commonServices, $location, itemServices, $ngBootbox, taxRateServices, expenseServices) {
        $scope.selected = void 0;
        $scope.globals = $cookieStore.get('globals');
        $scope.users = {};
        $scope.accountList = {};
        $scope.reimbursement = {'toUser':'', 'form': 'reimbursement', 'date': new Date(), 'reference' : '', 'reimbursementNo' : 'RM' + Math.floor((Math.random() * 100000)), 'description' : '', 'status':'', 'tax_rate_id': ''};
        $scope.items = {};
        $scope.item_list = {};
        $scope.reimbursement.totalAmount = 0;
        $scope.reimbursement.discount = 0;
        $scope.reimbursement.tax = 0;
        $scope.reimbursement.notes = [];
        $scope.reimbursement.sendEmailTo = '';
        $scope.emailError = 0;
        $scope.reimbursementList = [];
        $scope.itemDetails = {};
        $scope.default_user = 0;
        $scope.current_user = $scope.globals.currentUser.userId;
        $('.username .text').text($scope.globals.currentUser.username);

        $scope.getReimbursementList = function(){
            reimbursementServices.getReimbursementList().then(function(res){
                $scope.reimbursementList = res.response.businessReimbursements;
                $scope.search();
                $scope.select($scope.currentPage)
            });
        }

        $scope.getReimbursementDetails = function () {
            reimbursementServices.getReimbursementDetails($routeParams.id).then(function (res) {
                $scope.reimbursementDetail = res.response.businessReimbursement;
                $scope.convertToExpense($scope.reimbursementDetail);
            })
        }

        $scope.delete = function (id) {
            reimbursementServices.delete(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }

        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.reimbursementList, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.reimbursementList, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];
        $scope.taxRates = {};


        $scope.businessProfileId = $scope.globals.currentUser.businessProfile;

        if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
            $scope.main.error_message = "Please complete your business profile before creating reimbursement.";
            window.location.href = "#/business_profile";
        } else {
            userServices.getCustomerList().then(function(res) {
                $scope.users = res.response.customers;
            });
            itemServices.getItems().then(function(res) {
                $scope.items = res.response.businessItems;
            });
            accountServices.getAccountList().then(function(res) {
                 $scope.accounts = [];
                $scope.accountList = res.response.accounts;
                    for(var i=0; i<$scope.accountList.length;i++){
                        if($scope.accountList[i].user_id == $scope.default_user || $scope.accountList[i].user_id == $scope.current_user)
                            $scope.accounts.push($scope.accountList[i]);
                    }
            });

            accountServices.getParentAccountList().then(function (res) {
                $scope.parentAccountLists = res.response.parentAccounts;
            })
            taxRateServices.getTaxRatesList().then(function (res) {
                $scope.taxRates = res.response.taxRates;
            })
        }

        $scope.item_list = [{
            'business_item_id': '',
            'name': "",
            'description': "",
            "quantity" : 0,
            "price" : 0,
            "account_id" : "",
            "discount" : 0,
            "amount" : 0
        }];

        $scope.addNew = function() {
            $scope.item_list.push({
                'business_item_id': '',
                'name': "",
                'description': "",
                "quantity" : 0,
                "price" : 0,
                "account_id" : "",
                "discount" : 0,
                "amount" : 0
            });
        };

        $scope.addNewUser = function(){
            console.log("uaqsidddddddkdsuiatgdfaskjfgsakjfgdsakjg")
            var modalInstance;
            modalInstance = $modal.open({
                templateUrl: "views/user/add_new_user.html",
                controller: "userCtrlPopup"
            }), modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem
            }, function() {
                userServices.getCustomerList().then(function(res) {
                    $scope.users = res.response.customers;
                });
            })
        }

        $scope.remove = function(){
            var newDataList=[];
            $scope.selectedAll = false;
            angular.forEach($scope.item_list, function(selected){
                if(!selected.selected){
                    newDataList.push(selected);
                }
            });
            $scope.item_list = newDataList;
        };

        $scope.saveReimbursement = function() {
            $scope.main.error_message = "";
            validator.checkValidation($scope.reimbursement).then(function(data) {
                if($scope.reimbursement.status == "") {
                    $scope.reimbursement.status = "DRAFT";
                }
                if(Object.keys(data.error).length === 0) {
                    reimbursementServices.saveReimbursement($scope.reimbursement, $scope.item_list, $scope.businessProfileId).then(function(res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            var $id = res.response.businessReimbursement.id;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function(){
                                        //$window.scrollTo(0, 0);
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.href = "#/reimbursement_details/" + $id;
                                    }, 500);
                                });
                        } else {
                            //$window.scrollTo(0, 0);
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    $scope.main.error_message = data.error;
                }
            });
        }

        $scope.cancel = function() {
            $scope.main.error_message = "";
            window.location.href = "#/dashboard";
        };

        $scope.approve = function(type) {
            $scope.reimbursement.status = "APPROVED";
            $scope.saveReimbursement();
        };

        $scope.openEmailPopup = function () {
            validator.checkValidation($scope.reimbursement).then(function(data) {
                if(Object.keys(data.error).length === 0) {
                    $scope.reimbursement.status = "SEND EMAIL";
                    $('#sendEmail').modal('show');
                } else {
                    $scope.main.error_message = data.error;
                    $scope.reimbursement.status = "";
                }
            });
        }

        $scope.cancelEmail = function() {
            $scope.main.error_message = "";
            $scope.reimbursement.sendEmailTo = "";
            $scope.reimbursement.status = "";
            $("#sendEmail").modal('hide');
        }

        $scope.sendEmail = function(type) {
            if($scope.reimbursement.sendEmailTo.length === 0) {
                $scope.emailError = 1;
            } else {
                $("#sendEmail").modal('hide');
                $scope.reimbursement.status = "SEND EMAIL";
                $scope.saveReimbursement();
            }
        }

        $scope.calculateAmount = function(item) {
            item.amount = (item.price * item.quantity);
            if(item.discount != 0 || item.discount != "") {
                item.amount = item.amount - ((item.amount * item.discount) / 100);
            }
            $scope.calculateTotalAmount();
            $scope.calculateExpenseTotalAmount();
        }

        $scope.calculateTotalAmount = function() {
            $scope.reimbursement.totalAmount = 0;
            angular.forEach($scope.item_list, function(selected){
                $scope.reimbursement.totalAmount = $scope.reimbursement.totalAmount + selected.amount;
            });
            if($scope.reimbursement.discount != 0 || $scope.reimbursement.discount != "") {
                $scope.reimbursement.totalAmount = $scope.reimbursement.totalAmount - ($scope.reimbursement.totalAmount*($scope.reimbursement.discount)/100);
            }

            if($scope.reimbursement.tax_rate_id != '') {
                angular.forEach($scope.taxRates, function (value, key) {
                    if(value.id == $scope.reimbursement.tax_rate_id) {
                        $scope.reimbursement.tax = value.tax_rates;
                    }
                })
            }

            if($scope.reimbursement.tax != 0 || $scope.reimbursement.tax != "") {
                $scope.reimbursement.taxAmount = $scope.reimbursement.totalAmount*($scope.reimbursement.tax)/100;
                $scope.reimbursement.totalAmount = $scope.reimbursement.totalAmount + $scope.reimbursement.taxAmount;
            }
        }

        $scope.addNewNotes = function() {
            $scope.reimbursement.notes.push({'Notes':''});
        };

        $scope.getItemList = function (index) {
            if($scope.item_list[index].business_item_id != '') {
                itemServices.getItemDetails($scope.item_list[index].business_item_id).then(function (res) {
                    $scope.itemDetails = res.response.businessItem;
                    $scope.item_list[index].description = $scope.itemDetails.description;
                    $scope.item_list[index].quantity = 1;
                    $scope.item_list[index].price = $scope.itemDetails.selling_price;
                    $scope.item_list[index].account_id = parseInt($scope.itemDetails.account_id);

                    $scope.calculateAmount($scope.item_list[index]);
                });
            }
        }

        $scope.uploadFile = function(files) {
            if(files != null) {
                $scope.file = files[0];
                userServices.uploadFile($scope.file).then(function (res) {
                    $scope.reimbursement.attachment = res.attachment;
                })
            }
        };

        //Convert to Expense

        $scope.expense = {'payee':'', 'form': 'expense', 'date': new Date(), 'reference' : '', 'dueTo' : new Date(), 'paymentTerm' : '', 'purchase_order_no' : 'PO' + Math.floor((Math.random() * 100000)), 'deliveryDate' : new Date(), 'customDateOptions': 'Day(s)', 'startOn' : new Date(), 'endsOn' : new Date(), 'schedule' : '', 'customDate' : '', 'status': '', 'tax_rate_id': ''};
        $scope.expense.totalAmount = 0;
        $scope.expense.discount = 0;
        $scope.expense.tax = 0;
        $scope.expense.notes = [];
        $scope.expense.sendEmailTo = '';

        $scope.ItemEmpty = function () {
            $scope.item_list = {};
            $scope.item_list = [{
                'business_item_id': '',
                'name': "",
                'description': "",
                "quantity" : 0,
                "price" : 0,
                "account_id" : "",
                "discount" : 0,
                "amount" : 0
            }];
        }

        $scope.convertToExpense = function (reimbursementDetail) {
            $scope.ItemEmpty();
            $scope.expense.notes = [];
            $scope.expense.date = reimbursementDetail.date;
            $scope.expense.reference = reimbursementDetail.reference;
            $scope.expense.type = 'EXPENSE';
            $scope.expense.tax_rate_id = reimbursementDetail.tax_rate_id;
            $scope.expense.tax_rate = reimbursementDetail.tax_rate;
            $scope.itemLength = reimbursementDetail.business_reimbursement_item.length;

            angular.forEach(reimbursementDetail.business_reimbursement_item, function (business_item, index) {
                $scope.item_list[index].business_item_id = parseInt(business_item.business_item_id);
                $scope.item_list[index].name = business_item.item_name;
                $scope.item_list[index].description = business_item.description;
                $scope.item_list[index].quantity = business_item.quantity;
                $scope.item_list[index].price = business_item.price;
                $scope.item_list[index].amount = business_item.amount;
                $scope.item_list[index].discount = parseInt(business_item.discount);
                $scope.item_list[index].account_id = business_item.account_id;
                $scope.item_list[index].account_name = business_item.account_name;

                if ($scope.itemLength > (index + 1)) {
                    $scope.addNew();
                }
            });

            if(reimbursementDetail.notes.length > 0) {
                angular.forEach(reimbursementDetail.notes, function (note, index) {
                    $scope.expense.notes.push({'Notes': note.title});
                })
            } else {
                $scope.expense.notes.push({'Notes':''});
            }

            if($scope.expense.tax_rate_id != '') {
                angular.forEach($scope.taxRates, function (value, key) {
                    if(value.id == $scope.expense.tax_rate_id) {
                        $scope.expense.tax = value.tax_rates;
                    }
                })
            }

            $scope.expense.discount = parseInt(reimbursementDetail.discount);
            $scope.expense.taxAmount = parseInt(reimbursementDetail.tax_amount);
            $scope.expense.totalAmount = parseInt(reimbursementDetail.amount);
        }

        $scope.saveExpense = function() {
            $scope.main.error_message = "";
            validator.checkValidation($scope.expense).then(function(data) {
                if($scope.expense.status == "") {
                    $scope.expense.status = "DRAFT";
                }
                if(Object.keys(data.error).length === 0) {
                    expenseServices.saveExpense($scope.expense, $scope.item_list, $scope.businessProfileId).then(function(res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            console.log($scope.main.alert);
                            var $id = res.response.businessExpense.id;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function(){
                                        //$window.scrollTo(0, 0);
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.href = "#/expenses/view/" + $id;
                                    }, 500);
                                });
                        } else {
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = data.error;
                }
            });
        }

        $scope.openExpenseEmailPopup = function () {
            validator.checkValidation($scope.expense).then(function(data) {
                if(Object.keys(data.error).length === 0) {
                    $scope.expense.status = "SEND EMAIL";
                    $('#sendEmail').modal('show');
                } else {
                    $scope.main.error_message = data.error;
                    $scope.expense.status = "";
                }
            });
        }

        $scope.cancelExpenseEmail = function() {
            $scope.main.error_message = "";
            $scope.expense.sendEmailTo = "";
            $scope.expense.status = "";
            $("#sendEmail").modal('hide');
        }

        $scope.sendExpenseEmail = function(type) {
            if($scope.expense.sendEmailTo.length === 0) {
                $scope.emailError = 1;
            } else {
                $("#sendEmail").modal('hide');
                $scope.expense.status = "SEND EMAIL";
                $scope.saveExpense();
            }
        }

        $scope.calculateExpenseTotalAmount = function() {
            $scope.expense.totalAmount = 0;
            angular.forEach($scope.item_list, function(selected){
                $scope.expense.totalAmount = $scope.expense.totalAmount + selected.amount;
            });
            if($scope.expense.discount != 0 || $scope.expense.discount != "") {
                $scope.expense.totalAmount = $scope.expense.totalAmount - ($scope.expense.totalAmount*($scope.expense.discount)/100);
            }

            if($scope.expense.tax_rate_id != '') {
                angular.forEach($scope.taxRates, function (value, key) {
                    if(value.id == $scope.expense.tax_rate_id) {
                        $scope.expense.tax = value.tax_rates;
                    }
                })
            }

            if($scope.expense.tax != 0 || $scope.expense.tax != "") {
                $scope.expense.taxAmount = $scope.expense.totalAmount*($scope.expense.tax)/100;
                $scope.expense.totalAmount = $scope.expense.totalAmount + $scope.expense.taxAmount;
            }
        }
    }]);