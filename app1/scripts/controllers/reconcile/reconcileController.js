/**
 * Created by root on 5/4/17.
 */
'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Reconcile Controller
 * @description
 * #reconcileCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("reconcileCtrl", ["$scope", "$window", "$filter", "$routeParams", "validator", "$modal", "reconcileServices", "$cookieStore", "accountServices", "$location", "$ngBootbox", function($scope, $window, $filter, $routeParams, validator, $modal, reconcileServices, $cookieStore, accountServices, $location, $ngBootbox) {

        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);
        $scope.businessProfileId = $scope.globals.currentUser.businessProfile;

        $scope.reconcile = {'account_id' : '', 'endingDate' : new Date(), 'endingBalance' : 0,
                            'serviceCharge': '', 'serviceChargeDate': new Date(), 'serviceChargeAccount': '',
                            'interestEarned': '', 'interestEarnedDate': new Date(), 'interestEarnedAccount': '',
                            'beginningBalance': 0, 'chequePayments': 0, 'depositsPayments': 0, 'clearedBalance': 0, 'differenceBalance': 0,'auto_adjustment': 0
        };
        $scope.accountError = 0;
        $scope.reconcilePayments = {};
        $scope.reconcileDeposits = {};
        $scope.account = {};
        $scope.date = {};
        $scope.reconcileShow = 1;
        $scope.reconcileDataShow = 0;
        $scope.reconcileLists = {};
        $scope.businessReconcile = {};
        $scope.reconcileReportData = {};

        if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
            $scope.main.error_message = "Please complete your business profile before creating reconcile.";
            window.location.href = "#/business_profile";
        } else {
            accountServices.getAccountList().then(function(res) {
                $scope.accounts = res.response.accounts;
            });

            accountServices.getParentAccountList().then(function (res) {
                $scope.parentAccountLists = res.response.parentAccounts;
            })
        }

        $scope.getReconcileList = function (accountId) {
            reconcileServices.getReconcileList(accountId).then(function (res) {
                if(res.code == 200) {
                    $scope.reconcileLists = res.response.businessReconciles;
                }
            })

            reconcileServices.getReconcileLatest(accountId).then(function (res) {
                if(res.code == 200) {
                    $scope.businessReconcile = res.response.businessReconcile;
                    if($scope.businessReconcile != null) {
                        $scope.reconcile.beginningBalance = $scope.businessReconcile.ending_balance;
                    }

                    if($scope.businessReconcile == null) {
                        accountServices.getAccountDetails(accountId).then(function (res) {
                            if (res.code == 200) {
                                $scope.reconcile.beginningBalance = res.response.account.account_balance[0].balance;
                            }
                        })
                    }
                }
            })
        }


        $scope.openReconcileForm = function () {
            if($scope.reconcile.account_id == '' && $scope.reconcile.account_id != undefined) {
                $scope.accountError = 1;
            } else {
                $scope.accountError = 0;
                $('#reconcileForm').modal('show');
            }
        }

        $scope.cancelReconcileForm = function () {
            $('#reconcileForm').modal('hide');
        }

        $scope.getReconcile = function () {
            if($scope.reconcile.account_id == '' && $scope.reconcile.account_id != undefined) {
                $scope.accountError = 1;
            } else {
                $scope.cancelReconcileForm();
                $scope.reconcile.clearedBalance = parseInt($scope.reconcile.beginningBalance);
                $scope.reconcile.differenceBalance = parseInt($scope.reconcile.endingBalance) - parseInt($scope.reconcile.beginningBalance);
                reconcileServices.getReconcile($scope.reconcile).then(function (res) {
                    if (res.code == 200) {
                        $scope.reconcileDataShow = 1;
                        $scope.reconcileShow = 0;
                        $scope.reconcilePayments = res.response.reconcilePayments;
                        $scope.reconcileDeposits = res.response.reconcileDeposits;
                        $scope.account = res.response.account;
                        $scope.date = res.response.date;
                    }
                })
            }
        }

        $scope.reconcileback = function () {
            $scope.reconcile = {'account_id' : '', 'endingDate' : new Date(), 'endingBalance' : 0,
                'serviceCharge': '', 'serviceChargeDate': new Date(), 'serviceChargeAccount': '',
                'interestEarned': '', 'interestEarnedDate': new Date(), 'interestEarnedAccount': '',
                'beginningBalance': 8000, 'chequePayments': 0, 'depositsPayments': 0, 'clearedBalance': 0, 'differenceBalance': 0, 'auto_adjustment': 0
            };
            $scope.reconcile.clearedBalance = parseInt($scope.reconcile.beginningBalance);
            $scope.reconcile.differenceBalance = parseInt($scope.reconcile.endingBalance) - parseInt($scope.reconcile.beginningBalance);
            $scope.reconcileDataShow = 0;
            $scope.reconcileShow = 1;
        }


        //Reconcile calculation for cleared and difference balance
        $scope.chequeCount = 0;
        $scope.depositsCount = 0;

        $scope.checkedPaymentData = [];
        $scope.depositsData = [];
        $scope.checkDepositsBalance = function () {
            $scope.depositsCheckedValue = 0;
            $scope.depositsCount = 0;
            $scope.depositsData = [];

            angular.forEach($scope.reconcileDeposits, function (reconcileDeposit, index) {
                if(reconcileDeposit.isChecked == true && reconcileDeposit.isChecked != undefined) {
                    $scope.depositsCheckedValue += parseInt(reconcileDeposit.amount);
                    $scope.depositsCount += 1;
                    $scope.depositsData.push(reconcileDeposit);
                }
            });

            $scope.reconcile.depositsPayments = $scope.depositsCheckedValue;
            $scope.addClearedBalance($scope.reconcile.depositsPayments);
            $scope.subDifferenceBalance($scope.reconcile.depositsPayments);
        }

        $scope.checkPaymentBalance = function () {
            $scope.chequePaymentsCheckedValue = 0;
            $scope.chequeCount = 0;
            $scope.checkedPaymentData = [];

            angular.forEach($scope.reconcilePayments, function (reconcilePayment, index) {
                if(reconcilePayment.isChecked == true && reconcilePayment.isChecked != undefined) {
                    $scope.chequePaymentsCheckedValue += parseInt(reconcilePayment.amount);
                    $scope.chequeCount += 1;
                    $scope.checkedPaymentData.push(reconcilePayment);
                }
            })

            $scope.reconcile.chequePayments = $scope.chequePaymentsCheckedValue;
            $scope.subClearedBalance($scope.reconcile.chequePayments);
            $scope.addDifferenceBalance($scope.reconcile.chequePayments);
        }
        
        $scope.addClearedBalance = function (depositsPayment) {
            $scope.clearedBalance = 0;
            $scope.clearedBalance =  depositsPayment;
        }
        
        $scope.subClearedBalance = function (chequePayment) {
            $scope.chequeClearedBalance = 0;
            $scope.chequeClearedBalance = chequePayment;
        }

        $scope.addDifferenceBalance = function (chequePayments) {
            $scope.chequeDifferenceBalance = 0;
            $scope.chequeDifferenceBalance = chequePayments;
        }

        $scope.subDifferenceBalance = function (depositsPayment) {
            $scope.differenceBalance = 0;
            $scope.differenceBalance = depositsPayment;
        }

        $scope.saveReconcile = function () {
            $scope.reconcile.auto_adjustment = $scope.reconcile.differenceBalance;
            if($scope.differenceBalance != undefined) {
                $scope.reconcile.auto_adjustment -= parseInt($scope.differenceBalance);
            }
            if($scope.chequeDifferenceBalance != undefined) {
                $scope.reconcile.auto_adjustment += parseInt($scope.chequeDifferenceBalance);
            }

            reconcileServices.saveReconcile($scope.reconcile, $scope.checkedPaymentData, $scope.depositsData, $scope.businessProfileId).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.reload();
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = res.message;
                }
            })
        }

        $scope.cancleReconcile = function () {
            window.location.reload();
        }
        
        $scope.getReconcileData = function () {
            reconcileServices.getReconcileData($routeParams.id).then(function (res) {
                if(res.code == 200) {
                    $scope.reconcileReportData = res.response.businessReconcile;
                }
            })

            console.log($scope.reconcileReportData.beginning_balance);
        }

    }]);