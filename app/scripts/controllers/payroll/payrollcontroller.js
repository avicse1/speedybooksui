angular.module(projectAppName)
    .controller("payrollCtrl", ["$scope", "$window", "$modal", "$filter", "$routeParams", "validator", "payrollServices", "$cookieStore", "$location", "itemServices", "$ngBootbox", "taxRateServices", "userServices", function($scope, $window, $modal, $filter, $routeParams, validator, payrollServices, $cookieStore, $location, itemServices, $ngBootbox, taxRateServices, userServices) {
        $scope.globals = $cookieStore.get('globals');
        $scope.payItems = {};
        $scope.payItemTypes = {};
        $scope.item_list = [];
        $scope.payrollTab = 1;

        $scope.employee = {'employeeNo':'EM' + Math.floor((Math.random() * 100000)), 'name':'', 'dateOfBirth' : new Date(), 'type':'', 'dateOfJoining' : new Date(), 'gender':'', 'bloodGroup':'', 'maritalStatus':'', 'address':'', 'landlineNo':'', 'mobileNo':'', 'email':'', 'contactName':'', 'contactMobile':'', 'contactRelation':''};

        $scope.payroll = {'form': 'payroll', 'payFrequency': '', 'periodFrom': new Date(), 'periodTo': new Date(), 'paymentDate': new Date(), 'status':'', 'tax_rate_id': ''};

        $scope.payroll.notes = [];
        $scope.payroll.totalAmount = 0;
        $scope.payroll.tax = 0;
        $scope.payroll.sendEmailTo = [];
        $scope.payroll.employee_pay = [];
        $scope.main.error_message = [];

        $scope.wages = 0.00;
        $scope.allowances = 0.00;
        $scope.deductions = 0.00;
        $scope.taxes = 0.00;
        $scope.nonTaxable = 0.00;
        $scope.postTaxable = 0.00;

        $('.username .text').text($scope.globals.currentUser.username);

        $scope.addPayItemsTab = function() {
            if($scope.payroll.payFrequency == "") {
                $scope.payrollTab = 1;
                $scope.main.error_message.payFrequency_error = "Please select Pay Frequency";
            } else {
                 $scope.payrollTab = 2;
                    if($scope.item_list.length == 0) {
                         $scope.item_list.push({
                            'business_pay_item_id': '',
                            'name': "",
                            "quantity" : 0,
                            "price" : 0,
                            "amount" : 0
                        });
                    }
            }
           
        }

        $scope.showReviewTab = function() {
            if($scope.payroll.employee_pay.empId == "" || $scope.payroll.employee_pay.empId == undefined || $scope.payroll.employee_pay.empId == null) {
                $scope.payrollTab = 2;
                $scope.main.error_message.employee_pay_error = "Please select Employee";
            } else {
                $scope.main.error_message.employee_pay_error = "";
                $scope.error_message = "";
                angular.forEach($scope.item_list, function(selected){
                    angular.forEach(selected,function(subSelected) {
                        if(subSelected != undefined && subSelected == "" && subSelected == "0") {
                           $scope.error_message = "Please add items with all details";
                        }
                    });
                });

                angular.forEach($scope.taxRates, function (value, key) {
                    if(value.id == $scope.payroll.tax_rate_id) {
                        $scope.payroll.tax_rate_name = value.name;
                        $scope.payroll.tax_rates = value.tax_rates;
                    }
                })

                if($scope.error_message == "") {
                    $scope.payrollTab = 3;    
                }
                
            }
        }

        $scope.getPayItemsList = function () {
            itemServices.getPayItems().then(function(res) {
                $scope.payItems = res.response.businessPayItems;
                $scope.search();
                $scope.select($scope.currentPage)
            });
        }
        
        $scope.addNewPayItem = function () {
            var modalInstance;
            modalInstance = $modal.open({
                templateUrl: "views/items/add_new_pay_item.html",
                controller: "addItemPopupCtrl",
                resolve: { // This fires up before controller loads and templates rendered
                    payItem_id : function() {
                        return null;
                    }
                }
            }), modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem
            }, function() {
                itemServices.getPayItemTypes().then(function(res) {
                    $scope.payItemTypes = res.response.businessPayItemTypes;
                });
                itemServices.getPayItems().then(function(res) {
                    $scope.payItems = res.response.businessPayItems;
                    $scope.search();
                    $scope.select($scope.currentPage)
                });
            })
        }

        $scope.editPayItem = function (id) {
            var modalInstance;
            modalInstance = $modal.open({
                templateUrl: "views/items/edit_pay_items.html",
                controller: "addItemPopupCtrl",
                resolve: { // This fires up before controller loads and templates rendered
                    payItem_id : function() {
                        return id;
                    }
                }
            }), modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem
            }, function() {
                itemServices.getPayItemTypes().then(function(res) {
                    $scope.payItemTypes = res.response.businessPayItemTypes;
                });
                itemServices.getPayItems().then(function(res) {
                    $scope.payItems = res.response.businessPayItems;
                    $scope.search();
                    $scope.select($scope.currentPage)
                });
            })
        }

        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.payItems, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.payItems, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];
        $scope.taxRates = {};

        $scope.businessProfileId = $scope.globals.currentUser.businessProfile;

        if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
            $scope.main.error_message = "Please complete your business profile before creating payroll.";
            window.location.href = "#/business_profile";
        } else {
            itemServices.getPayItems().then(function(res) {
                $scope.payItems = res.response.businessPayItems;
            });

            itemServices.getPayItemTypes().then(function(res) {
                $scope.payItemTypes = res.response.businessPayItemTypes;
            });
            taxRateServices.getTaxRatesList().then(function (res) {
                $scope.taxRates = res.response.taxRates;
            })
        }

        $scope.addNewNotes = function() {
            $scope.payroll.notes.push({'Note_no' : $scope.payroll.notes.length + 1, 'Notes':''});
        };

        $scope.addNew = function() {
            $scope.item_list.push({
                'business_pay_item_id': '',
                'name': '',
                "quantity" : 0,
                "price" : 0,
                "amount" : 0
            });
        };

        $scope.openAddEmployeePopup = function () {
            var modalInstance;
            modalInstance = $modal.open({
                templateUrl: "views/payroll/add_employee.html",
                controller: "employeePopupCtrl"
            }), modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem
            }, function () {
               payrollServices.getEmployeeList().then(function(res) {
                    $scope.employees_list = res;
                });
            })
        }

        $scope.setDueDate = function() {
            if($scope.payroll.payFrequency == 'Weekly') {
                 var periodTo = new Date($scope.payroll.periodFrom || moment($scope.payroll.periodFrom, "YYYY MM DD"));
                 var days = 7;
                 periodTo.setDate(periodTo.getDate() + days);
                 $scope.payroll.periodTo = periodTo;
            } else if($scope.payroll.payFrequency == 'Every 2 Weeks') {
                var periodTo = new Date($scope.payroll.periodFrom || moment($scope.payroll.periodFrom, "YYYY MM DD"));
                 var days = 14;
                 periodTo.setDate(periodTo.getDate() + days);
                 $scope.payroll.periodTo = periodTo;
            } else if($scope.payroll.payFrequency == 'Monthly') {
               var periodTo = new Date($scope.payroll.periodFrom || moment($scope.payroll.periodFrom, "YYYY MM DD"));
                 var days = 30;
                 periodTo.setDate(periodTo.getDate() + days);
                 $scope.payroll.periodTo = periodTo;
            } else if($scope.payroll.payFrequency == 'Custom') {

            } else {
                $scope.payroll.periodFrom = new Date();
            }
        };

        $scope.saveEmployee = function() {
            payrollServices.addNewEmployee($scope.employee).then(
                function(res) {
                    if(res.code == 200) {
                        $location.path('/dashboard');
                    } else {
                        $scope.main.error_message = "All fields are required.";
                    }
                }, function(res) {
                    $scope.main.error_message = "All fields are required.";
                });
        };

        $scope.payTab = function(empId) {
           $scope.employeeId = empId;
        };

        payrollServices.getEmployeeList().then(function(res) {
            $scope.employees_list = res;
        });

        payrollServices.getPaychecksList().then(function(res) {
            $scope.paychecks_list = res;
        });

        $scope.getPaycheckDetails = function () {
            if($routeParams.id != '' || $routeParams.id != undefined) {
                payrollServices.getPaycheckDetails($routeParams.id).then(function (res) {
                    $scope.paycheckDetail = res.response.businessPayroll;
                })
            }
        }

        $scope.calculateAmount = function(item) {
            item.amount = (item.price * item.quantity);

            $scope.payCalculation();
            $scope.calculateTotalAmount();
        }

        $scope.payCalculation = function () {
            $scope.wages = 0.00;
            $scope.allowances = 0.00;
            $scope.deductions = 0.00;
            $scope.taxes = 0.00;
            $scope.nonTaxable = 0.00;
            $scope.postTaxable = 0.00;
            itemServices.getPayItems().then(function(res) {
                $scope.payItems = res.response.businessPayItems;
            });
            angular.forEach($scope.item_list, function (item, key) {
                angular.forEach($scope.payItems, function (payItem, key) {
                    if (payItem.id == item.business_pay_item_id) {
                        if(payItem.business_pay_item_type_id == 1) {
                            $scope.wages = $scope.wages +  item.amount;
                        }
                        if(payItem.business_pay_item_type_id == 2) {
                            $scope.allowances = $scope.allowances +  item.amount;
                        }
                        if(payItem.business_pay_item_type_id == 3) {
                            $scope.deductions = $scope.deductions + item.amount;
                        }
                        if(payItem.business_pay_item_type_id == 4) {
                            $scope.taxes = $scope.taxes + item.amount;
                        }
                        if(payItem.business_pay_item_type_id == 5) {
                            $scope.nonTaxable = $scope.nonTaxable + item.amount;
                        }
                        if(payItem.business_pay_item_type_id == 6) {
                            $scope.postTaxable = $scope.postTaxable + item.amount;
                        }
                    }
                });
            })
        }

        $scope.remove = function() {
            var newDataList=[];
            $scope.selectedAll = false;
            angular.forEach($scope.item_list, function(item){
                if(!item.selected){
                    newDataList.push(item);
                } else {
                    angular.forEach($scope.payItems, function (payItem, key) {
                        if (payItem.id == item.business_pay_item_id) {
                            if(payItem.business_pay_item_type_id == 1) {
                                $scope.wages = $scope.wages -  item.amount;
                            }
                            if(payItem.business_pay_item_type_id == 2) {
                                $scope.allowances = $scope.allowances -  item.amount;
                            }
                            if(payItem.business_pay_item_type_id == 3) {
                                $scope.deductions = $scope.deductions - item.amount;
                            }
                            if(payItem.business_pay_item_type_id == 4) {
                                $scope.taxes = $scope.taxes - item.amount;
                            }
                            if(payItem.business_pay_item_type_id == 5) {
                                $scope.nonTaxable = $scope.nonTaxable - item.amount;
                            }
                            if(payItem.business_pay_item_type_id == 6) {
                                $scope.postTaxable = $scope.postTaxable - item.amount;
                            }
                        }
                    });
                }
            });
            $scope.calculateTotalAmount();
            $scope.item_list = newDataList;
        };

        $scope.calculateTotalAmount = function() {
            $scope.payroll.netPay = 0;
            $scope.payroll.netPay = $scope.wages + $scope.allowances + $scope.nonTaxable - $scope.deductions - $scope.taxes - $scope.postTaxable;

            if($scope.payroll.tax_rate_id != '') {
                angular.forEach($scope.taxRates, function (value, key) {
                    if(value.id == $scope.payroll.tax_rate_id) {
                        $scope.payroll.tax = value.tax_rates;
                    }
                })
            }

            if($scope.payroll.tax != 0 || $scope.payroll.tax != "") {
                $scope.payroll.taxAmount = $scope.payroll.netPay*($scope.payroll.tax)/100;
                $scope.payroll.totalAmount = $scope.payroll.netPay + $scope.payroll.taxAmount;
            } else {
                $scope.payroll.totalAmount = $scope.payroll.netPay;
            }
        }

        $scope.cancel = function() {
            $scope.main.error_message = "";
            window.location.href = "#/dashboard";
        };

        $scope.approve = function() {
            $scope.payroll.status = "APPROVED";
            $scope.savePayroll();
        };

        $scope.draft = function() {
            $scope.payroll.status = "DRAFT";
            $scope.savePayroll();
        };

        $scope.savePayroll = function() {
            $scope.main.error_message = "";
            if($scope.payroll.status == "") {
                $scope.payroll.status = "DRAFT";
            }
            validator.checkValidation($scope.payroll).then(function(data) {
                if($scope.payroll.status == "") {
                    $scope.payroll.status = "DRAFT";
                }
                if(Object.keys(data.error).length === 0) {
                    payrollServices.savePayroll($scope.payroll, $scope.item_list, $scope.businessProfileId).then(function(res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            $scope.afterApprovedData(res.response.businessPayroll);
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function() {
                                        //$window.scrollTo(0, 0);
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        // window.location.href = "#/dashboard";
                                    }, 500);
                                });
                        } else {
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    $scope.main.error_message = data.error;
                }
            });
        }

        $scope.getEmployeeName = function (empId) {
            if(empId != '') {
                $scope.employeeName = '';
                angular.forEach($scope.employees_list, function (employee, key) {
                    if (employee.id == empId) {
                        $scope.employeeName = employee.name;
                    }
                });
            }
        }

        $scope.getBusinessPayItemName = function (index) {
            angular.forEach($scope.payItems, function (payItem, key) {
                if (payItem.id == $scope.item_list[index].business_pay_item_id) {
                    $scope.item_list[index].name = payItem.name;
                }
            });
        }

        $scope.delete = function (id) {
            payrollServices.delete(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                // window.location.href = "#" + $location.path();
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }

        $scope.employeeDelete = function (id) {
            payrollServices.employeeDelete(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                // window.location.href = "#" + $location.path();
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }
        
        $scope.getEmployeeDetails = function () {
            payrollServices.getEmployeeDetails($routeParams.id).then(function (res) {
                if(res.code == 200) {
                    $scope.employee = res.response.employee;
                    $scope.employee.last_working_day = new Date();
                    if(res.response.employee.is_terminated == 1) {
                        $scope.employee.is_terminated = true;
                    } else {
                        $scope.employee.is_terminated = false;
                    }
                }
            })
        }

        $scope.editEmployee = function () {
            payrollServices.updateEmployee($scope.employee).then(function(res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/dashboard";
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = res.message;
                }
            });
        }

        $scope.afterApprovedData = function (businessPayroll) {
            $scope.payroll.id = businessPayroll.id;
            $scope.payroll.payFrequency = businessPayroll.pay_frequency;
            $scope.payroll.periodFrom = businessPayroll.pay_period_from;
            $scope.payroll.periodTo = businessPayroll.pay_period_to;
            $scope.payroll.business_profile_id = businessPayroll.business_profile_id;
            $scope.payroll.paymentDate = businessPayroll.payment_date;
            $scope.payroll.status = businessPayroll.status;
            $scope.payroll.tax_rate_id = businessPayroll.tax_rate_id;
            $scope.payroll.taxAmount = businessPayroll.tax_amount;
            $scope.payroll.netPay = parseInt(businessPayroll.amount) - parseInt(businessPayroll.tax_amount);
            $scope.payroll.totalAmount = businessPayroll.amount;
        }

        $scope.openEmailPopup = function () {
            $('#sendEmail').modal('show');
        }

        $scope.cancelEmail = function() {
            $scope.payroll.sendEmailTo = [];
            $("#sendEmail").modal('hide');
        }

        $scope.sendEmail = function() {
            if($scope.payroll.sendEmailTo.length === 0) {
                $scope.emailError = 1;
            } else {
                $("#sendEmail").modal('hide');
                $scope.payroll.status = 'SEND EMAIL';
                payrollServices.sendEmail($scope.payroll).then(function (res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    //$window.scrollTo(0, 0);
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.href = "#/dashboard";
                                }, 500);
                            });
                    } else {
                        $scope.main.error_message = res.message;
                    }
                });
            }
        }
        
        $scope.payrollCopy = function () {
            $scope.payrollTab = 1;
            $scope.payroll.status = '';

            itemServices.getPayItems().then(function(res) {
                $scope.payItems = res.response.businessPayItems;
            });
        }

        $scope.uploadFile = function(files) {
            if(files != null) {
                $scope.file = files[0];
                userServices.uploadFile($scope.file).then(function (res) {
                    $scope.payroll.attachment = res.attachment;
                })
            }
        };

        $scope.resendPopup = function ($id) {
            $scope.payroll.id = $id;
            $('#sendEmail').modal('show');
        }

        $scope.deleteManagePayItem = function (id) {
        console.log("Deleting");
            payrollServices.deletePayItem(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    console.log(res);
                    for(var i = 0; i<$scope.currentPageStores.length; i++){
                          if($scope.currentPageStores[i].id.toString() == id.toString()){
                                $scope.currentPageStores.splice(i, 1);
                          }  
                    }
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }
}])
.controller("employeePopupCtrl", ["$scope", "$modalInstance", "$window", "$modal", "$filter", "$routeParams", "validator", "payrollServices", "$cookieStore", "$location", "itemServices", function($scope, $modalInstance, $window, $modal, $filter, $routeParams, validator, payrollServices, $cookieStore, $location, itemServices) {
        $scope.globals = $cookieStore.get('globals');
        $scope.employee = {'employeeNo':'EM' + Math.floor((Math.random() * 100000)), 'name':'', 'dateOfBirth' : new Date(), 'type':'', 'dateOfJoining' : new Date(), 'gender':'', 'bloodGroup':'', 'maritalStatus':'', 'address':'', 'landlineNo':'', 'mobileNo':'', 'email':'', 'contactName':'', 'contactMobile':'', 'contactRelation':'', 'is_terminated':0, 'last_working_day': new Date(), 'description':''};
        $scope.saveEmployee = function() {
            payrollServices.addNewEmployee($scope.employee).then(
                function(res) {
                    if(res.code == 200) {
                        $modalInstance.dismiss("ok");
                    } else {
                        $scope.main.error_message = "All fields are required.";
                    }
                }, function(res) {
                    $scope.main.error_message = "All fields are required.";
                });
        };

        $scope.cancelEmployee = function () {
            $modalInstance.dismiss("ok");
        }
}]);