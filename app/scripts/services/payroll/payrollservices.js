'use strict';

angular.module(projectAppName)

    .factory('payrollServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};

            service.addNewEmployee = function (userdetails) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'employees',
                        method: "POST",
                        data: {employement_no: userdetails.employeeNo, name: userdetails.name, date_of_birth : userdetails.dateOfBirth, type: userdetails.type, date_of_joining : userdetails.dateOfJoining, gender:userdetails.gender, blood_group: userdetails.bloodGroup, marital_status: userdetails.maritalStatus, address: userdetails.address, landline_no: userdetails.landlineNo, mobile_no: userdetails.mobileNo, email: userdetails.email, contact_name: userdetails.contactName, contact_mobile_no: userdetails.contactMobile, contact_relation:userdetails.contactRelation},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    }).error(function(data){
                        deferred.reject(data);
                    });;
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getEmployeeList = function() {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'employees',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data.response.employees);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getEmployeeDetails = function(employee_id) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'employees/' + employee_id,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.updateEmployee = function (employee) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'employees/' + employee.id,
                        method: "PUT",
                        data: {employement_no: employee.employement_no, name: employee.name, date_of_birth : employee.date_of_birth, type: employee.type, date_of_joining : employee.date_of_joining, gender:employee.gender, blood_group: employee.blood_group, marital_status: employee.marital_status, address: employee.address, landline_no: employee.landline_no, mobile_no: employee.mobile_no, email: employee.email, contact_name: employee.contact_name, contact_mobile_no: employee.contact_mobile_no, contact_relation:employee.contact_relation, is_terminated:employee.is_terminated, last_working_day:employee.last_working_day, description:employee.description},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getPaychecksList = function() {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/payrolls',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data.response.businessPayrolls);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getPaycheckDetails = function(paycheck_id) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/payrolls/' + paycheck_id,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.savePayroll = function (payrollData, itemList, businessProfileId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    var Notes = [];
                    angular.forEach(payrollData.notes, function(selected){
                        if(selected.Notes != "") {
                            Notes.push(selected.Notes);
                        }
                    });
                    $http({
                        url: webServerUrl + 'business/payrolls',
                        method: "POST",
                        data: { pay_frequency: payrollData.payFrequency, pay_period_from: payrollData.periodFrom, pay_period_to:payrollData.periodTo, payment_date:payrollData.paymentDate,  tax_amount: payrollData.taxAmount, tax_rate_id:payrollData.tax_rate_id, status: payrollData.status, send_email_to: payrollData.sendEmailTo, Notes: Notes, amount: payrollData.totalAmount, employee_id: payrollData.employee_pay.empId, business_profile_id: businessProfileId, Items: itemList, attachment:payrollData.attachment },
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.sendEmail = function (payrollData) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/payrolls/send/email/' + payrollData.id,
                        method: "POST",
                        data: { status: payrollData.status, send_email_to: payrollData.sendEmailTo},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.delete = function (payrollId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/payrolls/' + payrollId,
                        method: "DELETE",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.deletePayItem = function (payItemId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/payItems/' + payItemId,
                        method: "DELETE",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.employeeDelete = function (employeeId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'employees/' + employeeId,
                        method: "DELETE",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            return service;
    }])
