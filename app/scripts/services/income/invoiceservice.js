'use strict';

angular.module(projectAppName)

.factory('invoiceServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	$rootScope.globals = $cookieStore.get('globals');
    	$rootScope.userLoggedIn = false;
        var service = {};       
        service.getPaymentTermList = function () {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl,
	                method: "POST",
	                data: { form: 'term_list' },
	                dataType: "json",
	                withCredentials: true,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8'
	                }
	            }).
	            success(function(data){
					$('#mask').hide();
	            	deferred.resolve(data.data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.getInvoiceList = function() {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	        	$http({
	                url: webServerUrl + 'business/invoices',
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
	            	$('#mask').hide();
					deferred.resolve(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.getInvoiceDetails = function(invoice_id) {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	        	$http({
	                url: webServerUrl + 'business/invoices/'+invoice_id,
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
	            	$('#mask').hide();
					deferred.resolve(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

        service.getInvoiceNumber = function() {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	        	$http({
	                url: webServerUrl + 'business/invoices/generate',
	                method: "GET",
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
	            	$('#mask').hide();
					deferred.resolve(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        }

         service.saveInvoice = function (invoiceData, itemList, businessProfileId) {
         	$('#mask').show();
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	        		var Notes = [];
	        		 angular.forEach(invoiceData.notes, function(selected){
	        		 	if(selected.Notes != "") {
	        		 		Notes.push(selected.Notes);
	        		 	}   
			        }); 
	              $http({
	                url: webServerUrl + 'business/invoices',
	                method: "POST",
	                data: { schedule: invoiceData.schedule, custom_days: invoiceData.customDate, type: invoiceData.type, custom_dates: invoiceData.customDateOptions, start_on: invoiceData.startOn, end_on: invoiceData.endsOn, is_never_expires: invoiceData.neverExpires, discount: invoiceData.discount, tax_rate_id:invoiceData.tax_rate_id, tax_amount: invoiceData.taxAmount, status: invoiceData.status, send_email_to: invoiceData.sendEmailTo, Notes: Notes, amount: invoiceData.totalAmount, customer_id: invoiceData.toUser, business_profile_id: businessProfileId, invoice_no: invoiceData.invoiceNo, date: invoiceData.date, reference: invoiceData.reference, due_date: invoiceData.dueTo, payment_term : invoiceData.paymentTerm, description: invoiceData.description, Items: itemList, account_id: invoiceData.account_id, attachment: invoiceData.attachment},
	                dataType: "json",
	                withCredentials: false,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8',
	                            'Accept' : 'application/vnd.speedybooks.v1+json'
	                }
	            }).
	            success(function(data){
	            	$('#mask').hide();
					deferred.resolve(data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

		service.getTypeInvoices = function(type) {
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				$http({
					url: webServerUrl + 'business/invoices/type/' + type,
					method: "GET",
					dataType: "json",
					withCredentials: false,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Accept' : 'application/vnd.speedybooks.v1+json'
					}
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				});
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		};

		service.updateInvoice = function (invoiceId, invoiceData, itemList, businessProfileId) {
			$('#mask').show();
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				var Notes = [];
				angular.forEach(invoiceData.notes, function(selected){
					if(selected.Notes != "") {
						Notes.push(selected.Notes);
					}
				});

				$http({
					url: webServerUrl + 'business/invoices/' + invoiceId,
					method: "PUT",
					data: { schedule: invoiceData.schedule, custom_days: invoiceData.customDate, type: invoiceData.type, custom_dates: invoiceData.customDateOptions, start_on: invoiceData.startOn, end_on: invoiceData.endsOn, is_never_expires: invoiceData.neverExpires, discount: invoiceData.discount, tax_rate_id:invoiceData.tax_rate_id, tax_amount: invoiceData.taxAmount, status: invoiceData.status, send_email_to: invoiceData.sendEmailTo, Notes: Notes, amount: invoiceData.totalAmount, customer_id: invoiceData.toUser, business_profile_id: businessProfileId, invoice_no: invoiceData.invoiceNo, date: invoiceData.date, reference: invoiceData.reference, due_date: invoiceData.dueTo, payment_term : invoiceData.paymentTerm, description: invoiceData.description, Items: itemList, account_id: invoiceData.account_id},
					dataType: "json",
					withCredentials: false,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Accept' : 'application/vnd.speedybooks.v1+json'
					}
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				});
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		};

		service.deleteInvoice = function (invoiceId) {
			$('#mask').show();
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				$http({
					url: webServerUrl + 'business/invoices/' + invoiceId,
					method: "DELETE",
					dataType: "json",
					withCredentials: false,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Accept' : 'application/vnd.speedybooks.v1+json'
					}
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				});
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		};

        service.getOpenDuePaidInvoices = function(type) {
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/invoices/payment/type/' + type,
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.getUpcomingPayment = function() {
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/invoices/upcoming/payment/list',
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

		service.sendInvoice = function (resendInvoice) {
			$('#mask').show();
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				$http({
					url: webServerUrl + 'business/invoices/send/' + resendInvoice.id,
					method: "POST",
					data: { send_email_to: resendInvoice.sendEmailTo},
					dataType: "json",
					withCredentials: false,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Accept' : 'application/vnd.speedybooks.v1+json'
					}
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				});
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		};

		service.receivePayment = function (paymentReceive) {
			$('#mask').show();
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				$http({
					url: webServerUrl + 'business/invoices/receive/payment/' + paymentReceive.id,
					method: "POST",
					data: { date: paymentReceive.date, payment_term: paymentReceive.paymentTerm, reference:paymentReceive.reference, account_id: paymentReceive.account_id, amount:paymentReceive.amount,
							payment_method:paymentReceive.payment_method, payment_method_reference:paymentReceive.payment_method_reference, credit_card_no: paymentReceive.credit_card_no},
					dataType: "json",
					withCredentials: false,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Accept' : 'application/vnd.speedybooks.v1+json'
					}
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				});
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		};

		service.getDueInvoices = function () {
			var deferred = $q.defer();
			if($rootScope.globals != null) {
				$http({
					url: webServerUrl + 'business/invoices/due',
					method: "GET",
					dataType: "json",
					withCredentials: false,
					headers: {
						'Content-Type': 'application/json; charset=utf-8',
						'Accept' : 'application/vnd.speedybooks.v1+json'
					}
				}).
				success(function(data){
					$('#mask').hide();
					deferred.resolve(data);
				});
			} else {
				$rootScope.userLoggedIn = false;
				window.location.href = "#/logout";
			}
			return deferred.promise;
		}

		return service;
 }])
 
