"use strict";

angular.module(projectAppName+".directives", []).directive("imgHolder", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return Holder.run({
                        images: ele[0]
                    })
                }
            }
        }]).directive('loading',   ['$http' ,function ($http)
          {
              return {
                  restrict: 'A',
                  link: function (scope, elm, attrs)
                  {
                      scope.isLoading = function () {
                          return $http.pendingRequests.length > 0;
                      };

                      scope.$watch(scope.isLoading, function (v)
                      {
                          if(v){
                              elm.show();
                          }else{
                              elm.hide();
                          }
                      });
                  }
              };

          }]).directive("customBackground", function() {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$location", function($scope, $element, $location) {
                    var addBg, path;
                    return path = function() {
                        return $location.path()
                    }, addBg = function(path) {
                        switch ($element.removeClass("body-home body-special body-tasks body-lock"), path) {
                            case "/":
                                return $element.addClass("body-home");
                            case "/404":
                            case "/pages/500":
                            case "/pages/signin":
                            case "/pages/signup":
                            case "/pages/forgot":
                                return $element.addClass("body-special");
                            case "/pages/lock-screen":
                                return $element.addClass("body-special body-lock");
                            case "/tasks":
                                return $element.addClass("body-tasks")
                        }
                    }, addBg($location.path()), $scope.$watch(path, function(newVal, oldVal) {
                        return newVal !== oldVal ? addBg($location.path()) : void 0
                    })
                }]
            }
        }).directive("uiColorSwitch", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return ele.find(".color-option").on("click", function(event) {
                        var $this, hrefUrl, style;
                        if ($this = $(this), hrefUrl = void 0, style = $this.data("style"), "loulou" === style) hrefUrl = "styles/main.css", $('link[href^="styles/main"]').attr("href", hrefUrl);
                        else {
                            if (!style) return !1;
                            style = "-" + style, hrefUrl = "styles/main" + style + ".css", $('link[href^="styles/main"]').attr("href", hrefUrl)
                        }
                        return event.preventDefault()
                    })
                }
            }
        }]).directive("toggleMinNav", ["$rootScope", function($rootScope) {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    var $content, $nav, $window, Timer, app, updateClass;
                    return app = $("#"+projectAppName), $window = $(window), $nav = $("#nav-container"), $content = $("#content"), ele.on("click", function(e) {
                        return app.hasClass("nav-min") ? app.removeClass("nav-min") : (app.addClass("nav-min"), $rootScope.$broadcast("minNav:enabled")), e.preventDefault()
                    }), Timer = void 0, updateClass = function() {
                        var width;
                        return width = $window.width(), 768 > width ? app.removeClass("nav-min") : void 0
                    }, $window.resize(function() {
                        var t;
                        return clearTimeout(t), t = setTimeout(updateClass, 300)
                    })
                }
            }
        }]).directive("collapseNav", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    var $a, $aRest, $lists, $listsRest, app;
                    return $lists = ele.find("ul").parent("li"), $a = $lists.children("a"), $listsRest = ele.children("li").not($lists), $aRest = $listsRest.children("a"), app = $("#app"), $a.on("click", function(event) {
                        var $parent, $this;
                        return app.hasClass("nav-min") ? !1 : ($this = $(this), $parent = $this.parent("li"), $lists.not($parent).removeClass("open").find("ul").slideUp(), $parent.toggleClass("open").find("ul").slideToggle(), event.preventDefault())
                    }), $aRest.on("click", function() {
                        return $lists.removeClass("open").find("ul").slideUp()
                    }), scope.$on("minNav:enabled", function() {
                        return $lists.removeClass("open").find("ul").slideUp()
                    })
                }
            }
        }]).directive("highlightActive", [function() {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$attrs", "$location", function($scope, $element, $attrs, $location) {
                    var highlightActive, links, path;
                    return links = $element.find("a"), path = function() {
                        return $location.path()
                    }, highlightActive = function(links, path) {
                        return path = "#" + path, angular.forEach(links, function(link) {
                            var $li, $link, href;
                            return $link = angular.element(link), $li = $link.parent("li"), href = $link.attr("href"), $li.hasClass("active") && $li.removeClass("active"), 0 === path.indexOf(href) ? $li.addClass("active") : void 0
                        })
                    }, highlightActive(links, $location.path()), $scope.$watch(path, function(newVal, oldVal) {
                        return newVal !== oldVal ? highlightActive(links, $location.path()) : void 0
                    })
                }]
            }
        }]).directive('convertToNumber', function() {
            return {
              require: 'ngModel',
              link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(val) {
                  return val != null ? parseInt(val, 10) : null;
                });
                ngModel.$formatters.push(function(val) {
                  return val != null ? '' + val : null;
                });
              }
            };
          }).directive("toggleOffCanvas", [function() {
            return {
                restrict: "A",
                link: function(scope, ele) {
                    return ele.on("click", function() {
                        return $("#speedyBooksApp").toggleClass("on-canvas")
                    })
                }
            }
        }]).directive("slimScroll", [function() {
            return {
                restrict: "A",
                link: function(scope, ele, attrs) {
                    return ele.slimScroll({
                        height: attrs.scrollHeight || "100%"
                    })
                }
            }
        }]).directive('dynamicController', function($compile) {
          return  {
            transclude: 'element',
            scope: {
              'dynamicController': '=' 
            },
            link: function(scope, element, attr, ctrl, transclude) {
              var el = null;
              scope.$watch('dynamicController',function() {
                if (el) {
                  el.remove();
                  el = null;
                }
                transclude(function(clone) {
                  clone.attr('ng-controller', scope.dynamicController);
                  clone.removeAttr('dynamic-controller');
                  el = $compile(clone[0])(scope.$parent)
                  element.after(el);
                });
              });
            }
          }
        }).directive("goBack", [function() {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$window", function($scope, $element, $window) {
                    return $element.on("click", function() {
                        return $window.history.back()
                    })
                }]
            }
        }]).directive('chosen', function() {
            var __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };
       var CHOSEN_OPTION_WHITELIST, NG_OPTIONS_REGEXP, isEmpty, snakeCase;

       NG_OPTIONS_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?(?:\s+group\s+by\s+(.*))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(.*?)(?:\s+track\s+by\s+(.*?))?$/;
       CHOSEN_OPTION_WHITELIST = ['noResultsText', 'allowSingleDeselect', 'disableSearchThreshold', 'disableSearch', 'enableSplitWordSearch', 'inheritSelectClasses', 'maxSelectedOptions', 'placeholderTextMultiple', 'placeholderTextSingle', 'searchContains', 'singleBackstrokeDelete', 'displayDisabledOptions', 'displaySelectedOptions', 'width'];
       snakeCase = function(input) {
         return input.replace(/[A-Z]/g, function($1) {
           return "_" + ($1.toLowerCase());
         });
       };
       isEmpty = function(value) {
         var key;

         if (angular.isArray(value)) {
           return value.length === 0;
         } else if (angular.isObject(value)) {
           for (key in value) {
             if (value.hasOwnProperty(key)) {
               return false;
             }
           }
         }
         return true;
       };
       return {
         restrict: 'A',
         require: '?ngModel',
         terminal: true,
         link: function(scope, element, attr, ngModel) {
           var chosen, defaultText, disableWithMessage, empty, initOrUpdate, match, options, origRender, removeEmptyMessage, startLoading, stopLoading, valuesExpr, viewWatch;

           element.addClass('localytics-chosen');
           options = scope.$eval(attr.chosen) || {};
           angular.forEach(attr, function(value, key) {
             if (__indexOf.call(CHOSEN_OPTION_WHITELIST, key) >= 0) {
               return options[snakeCase(key)] = scope.$eval(value);
             }
           });
           startLoading = function() {
             return element.addClass('loading').attr('disabled', true).trigger('chosen:updated');
           };
           stopLoading = function() {
             return element.removeClass('loading').attr('disabled', false).trigger('chosen:updated');
           };
           chosen = null;
           defaultText = null;
           empty = false;
           initOrUpdate = function() {
             if (chosen) {
               return element.trigger('chosen:updated');
             } else {
               chosen = element.chosen(options).data('chosen');
               return defaultText = chosen.default_text;
             }
           };
           removeEmptyMessage = function() {
             empty = false;
             return element.attr('data-placeholder', defaultText);
           };
           disableWithMessage = function() {
             empty = true;
             return element.attr('data-placeholder', chosen.results_none_found).attr('disabled', true).trigger('chosen:updated');
           };
           if (ngModel) {
             origRender = ngModel.$render;
             ngModel.$render = function() {
               origRender();
               return initOrUpdate();
             };
             if (attr.multiple) {
               viewWatch = function() {
                 return ngModel.$viewValue;
               };
               scope.$watch(viewWatch, ngModel.$render, true);
             }
           } else {
             initOrUpdate();
           }
           attr.$observe('disabled', initOrUpdate);
           if (attr.ngOptions && ngModel) {
             match = attr.ngOptions.match(NG_OPTIONS_REGEXP);
             valuesExpr = match[7];
             return scope.$watch(valuesExpr, function(newVal, oldVal) {
               if (angular.isUndefined(newVal)) {
                 return startLoading();
               } else {
                 if (empty) {
                   removeEmptyMessage();
                 }
                 stopLoading();
                 if (isEmpty(newVal)) {
                   return disableWithMessage(options.no_results_text || 'No values available');
                 }
               }
             });
           }
         }
       };
     });