'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Account Controller
 * @description
 * #accountCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("accountCtrl", ["$scope", "$modal", "$log", "accountServices","$cookieStore", "$filter", "$ngBootbox", "$routeParams", function($scope, $modal, $log, accountServices, $cookieStore, $filter, $ngBootbox, $routeParams) {
        $('.username .text').text($scope.globals.currentUser.username);
        $scope.currentUserId = $scope.globals.currentUser.userId;
        $scope.defaultId = 0;
        //console.log("currentUser :: ", $scope.globals.currentUser);
        //console.log("currentUserId :: ", $scope.currentUserId);
        $scope.accounts = {};
        $scope.parentAccountLists = {};
        $scope.detailTypeListsData = {};
        $scope.default_user = 0;
        $scope.current_user = $scope.globals.currentUser.userId;

        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.accounts, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.accounts, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];

        accountServices.getAccountList().then(function (res) {
            $scope.accounts = res.response.accounts;
            $scope.search();
            $scope.select($scope.currentPage);
        })

        accountServices.getParentAccountList().then(function (res) {
            $scope.parentAccountLists = res.response.parentAccounts;
        })

        $scope.account = {'parent_id': '', 'name': '', 'code': '', 'description': '', 'detail_type_id': '', 'is_subaccount': false, 'balance': '',
            'track_depreciation': false, 'original_cost': '', 'depreciation_cost': '', 'depreciation_asof': new Date(), 'depreciation_at': ''};

        $scope.addAccount = function () {
            $scope.account.user_id = $scope.currentUserId;
            accountServices.addAccount($scope.account).then(function(res) {
                if(res.code == 200) {
                    $scope.alert_message = "Account added successfully!!";
                    setTimeout(function() {
                        $scope.alert_message = "";
                        window.location.reload();
                    }, 500);
                }
            }, function(res) {
                $scope.error_message = res.message;
            });
        }
        
        $scope.getAccountDetails = function () {
            accountServices.getAccountDetails($routeParams.id).then(function (res) {
                $scope.account.parent_id = parseInt(res.response.account.parent_id);
                $scope.account.name = res.response.account.name;
                $scope.account.code = res.response.account.code;
                $scope.account.description = res.response.account.description;
                $scope.account.detail_type_id = res.response.account.detail_type;
                $scope.account.balance = res.response.account.account_balance.balance;
            })
        }
        
        $scope.getDetailTypeList = function (parentAccountId) {
            if(parentAccountId == 6) {
                $('.account-form').find('div.modal-content').removeClass('account-current-assets').addClass('account-fixed-assets');
            } else {
                $('.account-form').find('div.modal-content').removeClass('account-fixed-assets').addClass('account-current-assets');
            }
            accountServices.getDetailTypeList(parentAccountId).then(function (res) {
                if(res.code == 200) {
                     $scope.detailTypeLists = [];
                    $scope.detailTypeListsData = res.response.detailTypeLists;
                    for(var i=0; i<$scope.detailTypeListsData.length;i++){
                        if($scope.detailTypeListsData[i].user_id == $scope.default_user || $scope.detailTypeListsData[i].user_id == $scope.current_user)
                            $scope.detailTypeLists.push($scope.detailTypeListsData[i]);
                    }
                }
            })
        }
        
        $scope.setAccountName = function (detailTypeId) {
            accountServices.getAccountDetails(detailTypeId).then(function (res) {
                $scope.account.name = res.response.account.name;
            })
        }
        
        $scope.updateAccount = function () {
            accountServices.updateAccount($routeParams.id, $scope.account).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = '#/accounts'
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = res.message;
                }
            })
        }

        $scope.deleteAccount = function (accountId) {
            accountServices.deleteAccount(accountId).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.reload();
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = res.message;
                }
            })
        }
        $scope.cancel = function() {
            window.location.href = "#/accounts";
        }
        
        $scope.cancelAccountForm = function () {
            $('#accountForm').modal('hide');
        }
    }])
    .controller("changePasswordCtrl", ["$scope", "$rootScope", "$location", "$cookieStore", "accountServices", "$ngBootbox", function ($scope, $rootScope, $location, $cookieStore, accountServices, $ngBootbox) {
        $scope.main.error_message = '';
        $scope.main.alert = '';
        $rootScope.globals = $cookieStore.get('globals');
        $scope.emailError = 0;

        $('.username .text').text($scope.globals.currentUser.username);

        $scope.user = {'currentPassword' : '', 'newPassword': '', 'confirmPassword': '', 'email': $scope.globals.currentUser.email};

        $scope.changePassword = function () {
            if($scope.user.newPassword == $scope.user.confirmPassword) {
                accountServices.changePassword($scope.user).then(function(res) {
                    console.log(res);
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                $rootScope.globals.currentUser = {};
                                window.location.href = "#/logout";
                                /*setTimeout(function() {
                                    //window.scrollTo(0, 0);
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.href = "#/dashboard";
                                }, 500);*/
                            });
                    } else {
                        //$window.scrollTo(0, 0);
                        $scope.main.error_message = res.message;
                    }
                }, function(res){
                    $scope.main.error_message = res.message;
                });

            } else {
                $scope.emailError = 1;
            }
        }
    }]);