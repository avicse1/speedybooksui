/**
 * Created by root on 2/4/17.
 */
'use strict';

angular.module(projectAppName)
    .factory('journalServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};
            service.getJournalList = function() {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/journals',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getJournalDetails = function(journalId) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/journals/' + journalId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.saveJournal = function (journalData, itemList, businessProfileId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/journals',
                        method: "POST",
                        data: { name: journalData.name, status: journalData.status, amount: journalData.totalAmount, business_profile_id: businessProfileId, date: journalData.date, Items: itemList},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.delete = function (journalId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/journals/' + journalId,
                        method: "DELETE",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.updateJournal = function (journalData, itemList) {
            $('#mask').show();
            var deferred = $q.defer();
            var globals = $cookieStore.get('globals');
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/journals/' + journalData.id,
                    method: "PUT",
                    data: { name: journalData.name, status: journalData.status, amount: journalData.totalAmount, business_profile_id: globals.currentUser.businessProfile, date: journalData.date, Items: itemList},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

            return service;
        }])

