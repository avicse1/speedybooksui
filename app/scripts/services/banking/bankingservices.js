'use strict';

angular.module(projectAppName)
    .factory('bankingServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};

            service.saveBanking = function (bankAccount) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts',
                        method: "POST",
                        withCredentials: false,
                        headers: {'Content-Type': undefined,
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        },
                        transformRequest: function () {
                            var formdata = new FormData();
                            formdata.append("name", bankAccount.name);
                            formdata.append("account_id", bankAccount.account_id);
                            formdata.append("upload", bankAccount.upload);
                            return formdata;
                        },
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getTransactions = function (bankAccount, mapping) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts/upload/' + bankAccount.id,
                        method: "POST",
                        data: { header: mapping.header, date_column:mapping.dateColumn, date_format: mapping.dateFormat, description:mapping.description, first_amount:mapping.first_amount, second_amount:mapping.second_amount, amountIn: mapping.amountIn },
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.saveBankTransaction = function (id, transaction) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts/transaction/' + id,
                        method: "POST",
                        data: { transaction:transaction },
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getBankAccountList = function() {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getBankAccountDetail = function(accountId) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts/' + accountId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.changeTransactionAccount = function (transaction) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccountTransaction/change/account/' + transaction.id,
                        method: "POST",
                        data: { account_id: transaction.account_id, description: transaction.description },
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.delete = function($id) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccountTransaction/' + $id,
                        method: "DELETE",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.undoTransaction = function($id) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts/undo/transaction/' + $id,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.receivePayment = function (paymentReceive) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts/invoice/receive/payment/' + paymentReceive.id,
                        method: "POST",
                        data: { date: paymentReceive.date, payment_term: paymentReceive.paymentTerm, reference:paymentReceive.reference, account_id: paymentReceive.account_id, amount:paymentReceive.amount,
                                transaction_id: paymentReceive.transaction_id, payment_method:paymentReceive.payment_method, payment_method_reference:paymentReceive.payment_method_reference, credit_card_no:paymentReceive.credit_card_no},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.makePayment = function (makePayment) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts/expense/make/payment/' + makePayment.id,
                        method: "POST",
                        data: { date: makePayment.date, payment_term: makePayment.paymentTerm, reference:makePayment.reference, account_id: makePayment.account_id, transaction_id: makePayment.transaction_id},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.addJournal = function (journal) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'bankAccounts/create/journal/' + journal.id,
                        method: "POST",
                        data: { name:journal.name, account_id: journal.account_id, date: journal.date, description: journal.description},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };



            return service;
    }])