'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Expense Controller
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
 

.controller("expenseCtrl", ["$scope", "$window", "userServices", "$routeParams", "validator", "$modal", "$filter", "expenseServices", "$cookieStore", "accountServices", "commonServices", "$location", "itemServices", "$ngBootbox", "taxRateServices", function($scope, $window, userServices, $routeParams, validator, $modal, $filter, expenseServices, $cookieStore, accountServices, commonServices, $location, itemServices, $ngBootbox, taxRateServices) {
	$scope.selected = void 0;
    $scope.globals = $cookieStore.get('globals');
	$scope.users = {};
    $scope.accountList = {};
	$scope.payment_term_list = {};
	$scope.expense = {'payee':'', 'form': '', 'date': new Date(), 'reference' : '', 'dueTo' : new Date(), 'expenseNo': 'EXP' + Math.floor((Math.random() * 100000)), 'paymentTerm' : '', 'purchase_order_no' : 'PO' + Math.floor((Math.random() * 100000)), 'deliveryDate' : new Date(), 'customDateOptions': 'Day(s)', 'startOn' : new Date(), 'endsOn' : new Date(), 'schedule' : '', 'customDate' : '', 'status': '', 'tax_rate_id': '', 'account_id': ''};
	$scope.items = {};
    $scope.item_list = {};
    $scope.expense.totalAmount = 0;
    $scope.expense.discount = 0;
    $scope.expense.tax = 0;
    $scope.expense.notes = [];
    $scope.expense.sendEmailTo = '';
    $scope.emailError = 0;
    $scope.itemDetails = {};
    $scope.expenseType = {};
    $scope.businessProfileId = $scope.globals.currentUser.businessProfile;
    $('.username .text').text($scope.globals.currentUser.username);

    $scope.getExpenseType = function (type) {
        expenseServices.getExpenseType(type).then(function (res) {
            $scope.expenseType = res.response.businessExpenses;
            $scope.search();
            $scope.select($scope.currentPage)
        })
    }

    $scope.getExpenseDetails = function () {
        expenseServices.getExpenseDetails($routeParams.id).then(function (res) {
            $scope.expenseDetail = res.response.businessExpense;
            $scope.updateExpenseDetail($scope.expenseDetail);
        })
    }


    $scope.deleteExpense = function (id) {
        expenseServices.deleteExpense(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
    }

    $scope.deletePurchaseOrder = function (id) {
        expenseServices.deletePurchaseOrder(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
    }

    $scope.getPurchaseOrderDetails = function () {
        expenseServices.getPurchaseOrderDetails($routeParams.id).then(function (res) {
            $scope.purchaseOrderDetail = res.response.businessPurchaseOrder;
            $scope.convertToExpense($scope.purchaseOrderDetail);
        })
    }

    $scope.getPurchaseOrder = function () {
        expenseServices.getPurchaseOrder().then(function (res) {
            $scope.expenseType = res.response.businessPurchaseOrder;
            $scope.search();
            $scope.select($scope.currentPage)
        })
    }

    //listing
    $scope.searchKeywords = "";
    $scope.filteredStores = [];
    $scope.row = "";
    $scope.currentPage = 1;


    $scope.onFilterChange = function() {
        return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
    };

    $scope.onNumPerPageChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };

    $scope.onOrderChange = function() {
        return $scope.select(1), $scope.currentPage = 1
    };

    $scope.search = function() {
        return $scope.filteredStores = $filter("filter")($scope.expenseType, $scope.searchKeywords), $scope.onFilterChange()
    };

    $scope.order = function(rowName) {
        return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.expenseType, rowName), $scope.onOrderChange()) : void 0
    };

    $scope.select = function(page) {
        var end, start;
        start = (page - 1) * $scope.numPerPage;
        end = start + $scope.numPerPage;
        $scope.currentPageStores = $scope.filteredStores.slice(start, end);
    }

    $scope.numPerPageOpt = [3, 5, 10, 20];
    $scope.numPerPage = $scope.numPerPageOpt[2];
    $scope.currentPage = 1;
    $scope.currentPageStores = [];
    $scope.taxRates = {};

    $scope.resendPurchaseOrder = {'form': 'resendPurchaseOrder'};
    $scope.resendPurchaseOrder.sendEmailTo = '';

    // make Payment account List
    $scope.makePaymentAccountLists = {};
    $scope.default_user = 0;
    $scope.current_user = $scope.globals.currentUser.userId;


    if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
        $scope.main.error_message = "Please complete your business profile before creating expense.";
        window.location.href = "#/business_profile";
    } else {
        userServices.getVendorsList().then(function(res) {
            $scope.users = res.response.vendors;
        });
        itemServices.getItems().then(function(res) {
            $scope.items = res.response.businessItems;
        });

        accountServices.getAccountList().then(function(res) {
            $scope.accounts = [];
            $scope.accountList = res.response.accounts;
            for(var i=0; i<$scope.accountList.length;i++){
                if($scope.accountList[i].user_id == $scope.default_user || $scope.accountList[i].user_id == $scope.current_user)
                    $scope.accounts.push($scope.accountList[i]);
            }
        });

        accountServices.getParentAccountList().then(function (res) {
            $scope.parentAccountLists = res.response.parentAccounts;
        });

        accountServices.getReceivePaymentAccountList().then(function (res) {
            $scope.makePaymentAccountLists = res.response.receivePaymentAccountLists;
        });

        taxRateServices.getTaxRatesList().then(function (res) {
            $scope.taxRates = res.response.taxRates;
        });
    }

    $scope.formName = function (name) {
        $scope.expense.form = name;
    }
    
	$scope.item_list = [{
                'business_item_id': '',
                'name': "",
                'description': "",
                "quantity" : 0,
                "price" : 0,
                "account_id" : "",
                "discount" : 0,
                "amount" : 0
    }];

    $scope.addNew = function() {
        $scope.item_list.push({
            'business_item_id': '',
            'name': "",
            'description': "",
            "quantity" : 0,
            "price" : 0,
            "account_id" : "",
            "discount" : 0,
            "amount" : 0
        });
    };

    $scope.addNewUser = function(){
         var modalInstance;
            modalInstance = $modal.open({
                templateUrl: "views/user/add_new_vendor.html",
                controller: "userCtrlPopup"
            }), modalInstance.result.then(function(selectedItem) {
                $scope.selected = selectedItem
            }, function() {
                userServices.getVendorsList().then(function(res) {
                    $scope.users = res.response.vendors;
                });
            })
     }

    $scope.setTypes = function(type) {
        $scope.expense.type = type;
    };

    $scope.remove = function(){
        var newDataList=[];
        $scope.selectedAll = false;
        angular.forEach($scope.item_list, function(selected){
            if(!selected.selected){
                newDataList.push(selected);
            }
        }); 
        $scope.item_list = newDataList;
    };

    $scope.saveExpense = function() {
        $scope.main.error_message = "";
        validator.checkValidation($scope.expense).then(function(data) {
            if($scope.expense.status == "") {
               $scope.expense.status = "DRAFT"; 
            }
            if(Object.keys(data.error).length === 0) {
                expenseServices.saveExpense($scope.expense, $scope.item_list, $scope.businessProfileId).then(function(res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        var $id = res.response.businessExpense.id;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.href = "#/expenses/view/" + $id;
                                }, 500);
                            });
                    } else {
                        $scope.main.error_message = res.message;
                    }
                }); 
            } else {
                $scope.main.error_message = data.error;
            }
        });
    }

    $scope.savePurchaseOrder = function() {
        $scope.main.error_message = "";
        validator.checkValidation($scope.expense).then(function(data) {
            if($scope.expense.status == "") {
                $scope.expense.status = "DRAFT";
            }

            if(Object.keys(data.error).length === 0) {
                expenseServices.savePurchaseOrder($scope.expense, $scope.item_list, $scope.businessProfileId).then(function(res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        var $id = res.response.businessPurchaseOrder.id;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    //$window.scrollTo(0, 0);
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.href = "#/purchase_order_details/" + $id;
                                }, 500);
                            });
                    } else {
                        $scope.main.error_message = res.message;
                    }
                });
            } else {
                //$window.scrollTo(0, 0);
                $scope.main.error_message = data.error;
            }
        });
    }

    $scope.cancel = function() {
        $scope.main.error_message = "";
        window.location.href = "#/dashboard";
    };

    $scope.approve = function(type) {
         $scope.expense.status = "APPROVED";
         if(type == 'Expense') {
             $scope.saveExpense();
         }

         if(type == 'PurchaseOrder') {
             $scope.savePurchaseOrder();
         }
    };

    $scope.openEmailPopup = function () {
        validator.checkValidation($scope.expense).then(function(data) {
            if(Object.keys(data.error).length === 0) {
                $scope.expense.status = "SEND EMAIL";
                $('#sendEmail').modal('show');
            } else {
                $scope.main.error_message = data.error;
                $scope.expense.status = "";
            }
        });
    }

    $scope.cancelEmail = function() {
        $scope.main.error_message = "";
        $scope.expense.sendEmailTo = "";
        $scope.resendPurchaseOrder.sendEmailTo = "";
        $scope.expense.status = "";
        $("#sendEmail").modal('hide');
    }

    $scope.sendEmail = function(type) {
        if($scope.expense.sendEmailTo.length === 0) {
            $scope.emailError = 1;
        } else {
            $("#sendEmail").modal('hide');
            $scope.expense.status = "SEND EMAIL";
            if(type == 'Expense') {
                $scope.saveExpense();
            }
            if(type == 'PurchaseOrder') {
                $scope.savePurchaseOrder();
            }
        }
    }

    $scope.calculateAmount = function(item) {
        item.amount = (item.price * item.quantity);
        if(item.discount != 0 || item.discount != "") {
            item.amount = item.amount - ((item.amount * item.discount) / 100);
        }
        $scope.calculateTotalAmount();
    }

    $scope.calculateTotalAmount = function() {
         $scope.expense.totalAmount = 0;
         angular.forEach($scope.item_list, function(selected) {
            $scope.expense.totalAmount = $scope.expense.totalAmount + selected.amount;
        }); 
        if($scope.expense.discount != 0 || $scope.expense.discount != "") {
            $scope.expense.totalAmount = $scope.expense.totalAmount - ($scope.expense.totalAmount*($scope.expense.discount)/100);
        }

        if($scope.expense.tax_rate_id != '') {
            angular.forEach($scope.taxRates, function (value, key) {
                if(value.id == $scope.expense.tax_rate_id) {
                    $scope.expense.tax = value.tax_rates;
                }
            })
        }

        if($scope.expense.tax != 0 || $scope.expense.tax != "") {
            $scope.expense.taxAmount = $scope.expense.totalAmount*($scope.expense.tax)/100;
            $scope.expense.totalAmount = $scope.expense.totalAmount + $scope.expense.taxAmount;
        }
    }

    $scope.setDueDate = function() {
        if($scope.expense.paymentTerm == "Paid") {
            $scope.expense.dueTo =  moment($scope.expense.date, "YYYY MM DD");
        } else if($scope.expense.paymentTerm == "Due on Invoice") {
            var endDate = new Date($scope.expense.date || moment($scope.expense.date, "YYYY MM DD"));
            var days = 0;
            endDate.setDate(endDate.getDate() + days);
            $scope.expense.dueTo = endDate;
        } else if($scope.expense.paymentTerm == "Due 4 Weeks") {
            var endDate = new Date($scope.expense.date || moment($scope.expense.date, "YYYY MM DD"));
            var days = 27;
            endDate.setDate(endDate.getDate() + days);
            $scope.expense.dueTo = endDate;
        } else if($scope.expense.paymentTerm == "Due 8 Weeks") {
            var endDate = new Date($scope.expense.date || moment($scope.expense.date, "YYYY MM DD"));
            var days = 55;
            endDate.setDate(endDate.getDate() + days);
            $scope.expense.dueTo = endDate;
        } else {
            $scope.expense.dueTo = new Date();
        }
    };

    $scope.addNewNotes = function() {
        $scope.expense.notes.push({'Notes':''});
    };

    $scope.getItemList = function (index) {
        if($scope.item_list[index].business_item_id != '') {
            itemServices.getItemDetails($scope.item_list[index].business_item_id).then(function (res) {
                $scope.itemDetails = res.response.businessItem;
                $scope.item_list[index].description = $scope.itemDetails.description;
                $scope.item_list[index].quantity = 1;
                $scope.item_list[index].price = $scope.itemDetails.selling_price;
                $scope.item_list[index].account_id = parseInt($scope.itemDetails.account_id);
                $scope.calculateAmount($scope.item_list[index]);
            });
        }
    }

    $scope.updateExpenseDetail = function (expenseDetail) {
        $scope.ItemEmpty();
        $scope.expense.notes = [];
        $scope.expense.id = expenseDetail.id;
        $scope.expense.expenseNo = expenseDetail.expense_no;
        $scope.expense.account_id = parseInt(expenseDetail.account_id);
        $scope.expense.payee = expenseDetail.vendor[0].id;
        $scope.expense.date = expenseDetail.date;
        $scope.expense.reference = expenseDetail.reference;
        $scope.expense.paymentTerm = expenseDetail.payment_term;
        $scope.expense.status = expenseDetail.status;
        $scope.expense.type = expenseDetail.type;
        $scope.expense.tax_rate_id = expenseDetail.tax_rate_id;
        $scope.expense.tax_rate = expenseDetail.tax_rate;
        $scope.itemLength = expenseDetail.business_expense_item.length;

        angular.forEach(expenseDetail.business_expense_item, function (business_item, index) {
            $scope.item_list[index].business_item_id = parseInt(business_item.business_item_id);
            $scope.item_list[index].name = business_item.item_name;
            $scope.item_list[index].description = business_item.description;
            $scope.item_list[index].quantity = business_item.quantity;
            $scope.item_list[index].price = business_item.price;
            $scope.item_list[index].amount = business_item.amount;
            $scope.item_list[index].discount = parseInt(business_item.discount);
            $scope.item_list[index].account_id = business_item.account_id;
            $scope.item_list[index].account_name = business_item.account_name;

            if ($scope.itemLength > (index + 1)) {
                $scope.addNew();
            }
        });

        if(expenseDetail.notes.length > 0) {
            angular.forEach(expenseDetail.notes, function (note, index) {
                $scope.expense.notes.push({'Notes': note.title});
            })
        } else {
            $scope.expense.notes.push({'Notes':''});
        }

        if($scope.expense.tax_rate_id != '') {
            angular.forEach($scope.taxRates, function (value, key) {
                if(value.id == $scope.expense.tax_rate_id) {
                    $scope.expense.tax = value.tax_rates;
                }
            })
        }

        $scope.expense.discount = parseInt(expenseDetail.discount);
        $scope.expense.taxAmount = parseInt(expenseDetail.tax_amount);
        $scope.expense.totalAmount = parseInt(expenseDetail.amount);
    }
    
    $scope.update = function () {
        $scope.main.error_message = "";
        validator.checkValidation($scope.expense).then(function(data) {
            if($scope.expense.status == "") {
                $scope.expense.status = "DRAFT";
            }

            if(Object.keys(data.error).length === 0) {
                expenseServices.updateExpense($scope.expense.id, $scope.expense, $scope.item_list, $scope.businessProfileId).then(function(res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    //$window.scrollTo(0, 0);
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.href = "#/dashboard";
                                }, 500);
                            });
                    } else {
                        //$window.scrollTo(0, 0);
                        $scope.main.error_message = res.message;
                    }
                });
            } else {
                $scope.main.error_message = data.error;
            }
        });
    }

    $scope.ItemEmpty = function () {
        $scope.item_list = {};
        $scope.item_list = [{
            'business_item_id': '',
            'name': "",
            'description': "",
            "quantity" : 0,
            "price" : 0,
            "account_id" : "",
            "discount" : 0,
            "amount" : 0
        }];
    }

    $scope.neverExpires = function () {
        if($scope.expense.neverExpires == true) {
            $('#ends_on .ng-datepicker-input.form-control').attr('disabled', true);
        } else {
            $('#ends_on .ng-datepicker-input.form-control').attr('disabled', false);
        }
    }

    $scope.uploadFile = function(files) {
        if(files != null) {
            $scope.file = files[0];
            userServices.uploadFile($scope.file).then(function (res) {
                $scope.expense.attachment = res.attachment;
            })
        }
    };

    $scope.openResend = function (expenseId) {
        $scope.resendPurchaseOrder.id = expenseId;
        $('#sendEmail').modal('show');
    }
    
    $scope.sendPurchaseOrder = function () {
        if($scope.resendPurchaseOrder.sendEmailTo.length === 0) {
            $scope.emailError = 1;
        } else {
            $("#sendEmail").modal('hide');
            expenseServices.sendPurchaseOrder($scope.resendPurchaseOrder).then(function(res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/purchase_order";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            });
        }
    }

    $scope.convertToExpense = function (expenseDetail) {
        $scope.ItemEmpty();
        $scope.expense.notes = [];
        $scope.expense.id = expenseDetail.id
        $scope.expense.payee = expenseDetail.vendor.id;
        $scope.expense.date = expenseDetail.date;
        $scope.expense.reference = expenseDetail.reference;
        $scope.expense.paymentTerm = expenseDetail.payment_term;
        $scope.expense.status = expenseDetail.status;
        $scope.expense.type = 'EXPENSE';
        $scope.expense.tax_rate_id = expenseDetail.tax_rate_id;
        $scope.expense.tax_rate = expenseDetail.tax_rate;
        $scope.itemLength = expenseDetail.business_purchase_order_item.length;

        angular.forEach(expenseDetail.business_purchase_order_item, function (business_item, index) {
            $scope.item_list[index].business_item_id = parseInt(business_item.business_item_id);
            $scope.item_list[index].name = business_item.item_name;
            $scope.item_list[index].description = business_item.description;
            $scope.item_list[index].quantity = business_item.quantity;
            $scope.item_list[index].price = business_item.price;
            $scope.item_list[index].amount = business_item.amount;
            $scope.item_list[index].discount = parseInt(business_item.discount);
            $scope.item_list[index].account_id = business_item.account_id;
            $scope.item_list[index].account_name = business_item.account_name;

            if ($scope.itemLength > (index + 1)) {
                $scope.addNew();
            }
        });

        if(expenseDetail.notes.length > 0) {
            angular.forEach(expenseDetail.notes, function (note, index) {
                $scope.expense.notes.push({'Notes': note.title});
            })
        } else {
            $scope.expense.notes.push({'Notes':''});
        }

        if($scope.expense.tax_rate_id != '') {
            angular.forEach($scope.taxRates, function (value, key) {
                if(value.id == $scope.expense.tax_rate_id) {
                    $scope.expense.tax = value.tax_rates;
                }
            })
        }

        $scope.expense.discount = parseInt(expenseDetail.discount);
        $scope.expense.taxAmount = parseInt(expenseDetail.tax_amount);
        $scope.expense.totalAmount = parseInt(expenseDetail.amount);
    }
    
    $scope.updatePurchaseOrder = function () {
        $scope.main.error_message = "";
        validator.checkValidation($scope.expense).then(function(data) {
            if($scope.expense.status == "") {
                $scope.expense.status = "DRAFT";
            }

            if(Object.keys(data.error).length === 0) {
                expenseServices.updatePurchaseOrder($scope.expense, $scope.item_list, $scope.businessProfileId).then(function(res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    //$window.scrollTo(0, 0);
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.href = "#/dashboard";
                                }, 500);
                            });
                    } else {
                        //$window.scrollTo(0, 0);
                        $scope.main.error_message = res.message;
                    }
                });
            } else {
                $scope.main.error_message = data.error;
            }
        });
    }

    //Make payment
    $scope.makePayment = function () {
        expenseServices.getExpenseDetails($routeParams.id).then(function (res) {
            $scope.expense = res.response.businessExpense;
            $scope.expense.paymentTerm = $scope.expense.payment_term;
        })
    }

    $scope.sendPayment = function () {
        if($scope.expense.account_id == '' && $scope.expense.account_id != undefined) {
            $scope.emailError = 1;
        } else {
            $scope.expense.paymentTerm = 'Paid';
            expenseServices.makePayment($scope.expense).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/dashboard";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            })
        }
    }
}]);