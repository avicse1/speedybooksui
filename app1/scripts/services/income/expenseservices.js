'use strict';

angular.module(projectAppName)

.factory('expenseServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	$rootScope.globals = $cookieStore.get('globals');
    	$rootScope.userLoggedIn = false;
        var service = {};       
        service.getPaymentTermList = function () {
        	var deferred = $q.defer();
	        	if($rootScope.globals != null) {
	              $http({
	                url: webServerUrl,
	                method: "POST",
	                data: { form: 'term_list' },
	                dataType: "json",
	                withCredentials: true,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8'
	                }
	            }).
	            success(function(data){
	                $('#mask').hide();
                    deferred.resolve(data.data);
	            });
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/logout";
        	}
        	 return deferred.promise;
        };

         service.saveExpense = function (expenseData, itemList, businessProfileId) {
             $('#mask').show();
             var deferred = $q.defer();
                if($rootScope.globals != null) {
                    var Notes = [];
                    angular.forEach(expenseData.notes, function(selected){
                        if(selected.Notes != "") {
                            Notes.push(selected.Notes);
                        }   
                    });
                    $http({
                        url: webServerUrl + 'business/expenses',
                        method: "POST",
                        data: { schedule: expenseData.schedule, custom_days: expenseData.customDate, type: expenseData.type, custom_dates: expenseData.customDateOptions, start_on: expenseData.startOn, end_on: expenseData.endsOn, is_never_expires: expenseData.neverExpires, discount: expenseData.discount, tax_amount: expenseData.taxAmount, tax_rate_id: expenseData.tax_rate_id, status: expenseData.status, send_email_to: expenseData.sendEmailTo, Notes: Notes, amount: expenseData.totalAmount, vendor_id: expenseData.payee, business_profile_id: businessProfileId, invoice_no: expenseData.invoiceNo, date: expenseData.date, reference: expenseData.reference, due_date: expenseData.dueTo, payment_term : expenseData.paymentTerm, expense_no: expenseData.expenseNo ,Items: itemList, attachment: expenseData.attachment, account_id: expenseData.account_id},
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                $rootScope.userLoggedIn = false;
                 window.location.href = "#/logout";
            }
             return deferred.promise;
        };

        service.savePurchaseOrder = function (PurchaseOrderData, itemList, businessProfileId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                var Notes = [];
                angular.forEach(PurchaseOrderData.notes, function(selected){
                    if(selected.Notes != "") {
                        Notes.push(selected.Notes);
                    }
                });
                $http({
                    url: webServerUrl + 'business/purchaseOrder',
                    method: "POST",
                    data: { discount: PurchaseOrderData.discount, tax_amount: PurchaseOrderData.taxAmount, tax_rate_id: PurchaseOrderData.tax_rate_id, status: PurchaseOrderData.status, send_email_to: PurchaseOrderData.sendEmailTo, Notes: Notes, amount: PurchaseOrderData.totalAmount, vendor_id: PurchaseOrderData.payee, business_profile_id: businessProfileId, purchase_order_no: PurchaseOrderData.purchase_order_no, delivery_date: PurchaseOrderData.deliveryDate, date: PurchaseOrderData.date, reference: PurchaseOrderData.reference, Items: itemList, attachment: PurchaseOrderData.attachment},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.getExpenseType = function(type) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/expenses/type/' + type,
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.getPurchaseOrder = function(type) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/purchaseOrder',
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.getExpenseDetails = function(expense_id) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/expenses/'+expense_id,
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.getPurchaseOrderDetails = function(purchaseOrder_id) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/purchaseOrder/'+purchaseOrder_id,
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.sendPurchaseOrder = function(resendPurchaseOrder) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/purchaseOrder/send/'+resendPurchaseOrder.id,
                    method: "POST",
                    data: { send_email_to: resendPurchaseOrder.sendEmailTo},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.deleteExpense = function (expenseId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/expenses/' + expenseId,
                    method: "DELETE",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data) {
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.deletePurchaseOrder = function (purchaseOrderId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/purchaseOrder/' + purchaseOrderId,
                    method: "DELETE",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.updateExpense = function (expenseId, expenseData, itemList, businessProfileId) {
            $('#mask').show();
            var deferred = $q.defer();

            if($rootScope.globals != null) {
                var Notes = [];
                angular.forEach(expenseData.notes, function(selected){
                    if(selected.Notes != "") {
                        Notes.push(selected.Notes);
                    }
                });
                $http({
                    url: webServerUrl + 'business/expenses/' + expenseId,
                    method: "PUT",
                    data: { schedule: expenseData.schedule, custom_days: expenseData.customDate, type: expenseData.type, custom_dates: expenseData.customDateOptions, start_on: expenseData.startOn, end_on: expenseData.endsOn, is_never_expires: expenseData.neverExpires, discount: expenseData.discount, tax_amount: expenseData.taxAmount,  expense_no: expenseData.expenseNo, tax_rate_id: expenseData.tax_rate_id, status: expenseData.status, send_email_to: expenseData.sendEmailTo, Notes: Notes, amount: expenseData.totalAmount, vendor_id: expenseData.payee, business_profile_id: businessProfileId, invoice_no: expenseData.invoiceNo, date: expenseData.date, reference: expenseData.reference, due_date: expenseData.dueTo, payment_term : expenseData.paymentTerm, Items: itemList, account_id: expenseData.account_id},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.updatePurchaseOrder = function (PurchaseOrderData, itemList, businessProfileId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                var Notes = [];
                angular.forEach(PurchaseOrderData.notes, function(selected){
                    if(selected.Notes != "") {
                        Notes.push(selected.Notes);
                    }
                });
                $http({
                    url: webServerUrl + 'business/purchaseOrder/' + PurchaseOrderData.id,
                    method: "PUT",
                    data: { discount: PurchaseOrderData.discount, tax_amount: PurchaseOrderData.taxAmount, tax_rate_id: PurchaseOrderData.tax_rate_id, status: PurchaseOrderData.status, send_email_to: PurchaseOrderData.sendEmailTo, Notes: Notes, amount: PurchaseOrderData.totalAmount, vendor_id: PurchaseOrderData.payee, business_profile_id: businessProfileId, purchase_order_no: PurchaseOrderData.purchase_order_no, delivery_date: PurchaseOrderData.deliveryDate, date: PurchaseOrderData.date, reference: PurchaseOrderData.reference, Items: itemList, attachment: PurchaseOrderData.attachment},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.makePayment = function (makePayment) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/expenses/make/payment/' + makePayment.id,
                    method: "POST",
                    data: { date: makePayment.date, payment_term: makePayment.paymentTerm, reference:makePayment.reference, account_id: makePayment.account_id, amount:makePayment.amount},
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.getDueExpenses = function () {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/expenses/due',
                    method: "GET",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        return service;
        
 }])
 
