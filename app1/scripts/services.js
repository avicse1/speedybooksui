'use strict';
 
angular.module('projectangApp')
 .factory('SignUpService', ['Base64', '$http', '$cookieStore', '$rootScope', '$timeout', 'AuthenticationService',
    function (Base64, $http, $cookieStore, $rootScope, $timeout, AuthenticationService) {
        var service = {};
        service.signup = function (username, password, email, phNo) {
             $http({
                url: 'http://webserver.dev/services.php',
                method: "POST",
                data: { username: username, password: password, email: email, phNo: phNo,  form: 'signUp' },
                dataType: "json",
                withCredentials: true,
                headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                }
            }).
            success(function(data){
                $rootScope.status = data.status;
                if(data.status == 0) {
                    $rootScope.error = data.Message;
                }  else {
                	$rootScope.error = "Account Created Succesfully. Please login..!! ";
                    //AuthenticationService.SetCredentials($rootScope.username, $rootScope.password);
                    window.location.href = "#/";
                }
               
            });

        }
        return service;
 }])
.factory('ProfileServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	$rootScope.globals = $cookieStore.get('globals');
    	$rootScope.userLoggedIn = false;
        var service = {};
        var deferred = $q.defer();
        service.getProfileData = function () {
	        	if($rootScope.globals != null) {
	              $http({
	                url: 'http://webserver.dev/services.php',
	                method: "POST",
	                data: { userId: $rootScope.globals.currentUser.userId, form: 'profile' },
	                dataType: "json",
	                withCredentials: true,
	                headers: {
	                            'Content-Type': 'application/json; charset=utf-8'
	                }
	            }).
	            success(function(data){
	                $rootScope.status = data.status;
	                if(data.status == 0) {
	                	$rootScope.userLoggedIn = false;
	                    window.location.href = "#/";
	                }  else {
	                	$rootScope.userLoggedIn = true;
	                	deferred.resolve(data.data);
	                }
	               
	            });
	            return deferred.promise;
        	} else {
        		$rootScope.userLoggedIn = false;
        		 window.location.href = "#/";
        	}
        }
        return service;
        
 }])
