'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:userCtrl
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("dashboardCtrl", ["$scope", "$window", "validator", "$modal", "$cookieStore", "$location", "$ngBootbox", "dashboardServices", function($scope, $window, validator, $modal, $cookieStor, $location, $ngBootbox, dashboardServices) {

        $scope.yearKey = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $scope.activityInvoice = true;
        $scope.activityEstimates = false;
        $scope.activityExpense = false;
        $scope.showAllActivity = false;


        $scope.getTotalUser = function () {
            dashboardServices.getTotalUsers().then(function (res) {
                $scope.totalUser = res.response.user;
            })
        }

        $scope.getInvoiceTotal = function () {
            dashboardServices.getInvoiceTotal().then(function (res) {
                $scope.invoiceTotal = res.response.invoices;
            })
        }
        
        $scope.getExpenseReportChart = function () {
            dashboardServices.getExpenseReportChart().then(function(res) {
                $scope.expenseReportChart = res.response.report;
                var $value = [{
                    data: $scope.expenseReportChart.year.value
                }];
                $scope.showChart('expenseReport', 'Expense Report','area', $scope.yearKey, $value);
            });
        }

        $scope.changeExpenseChart = function (event,type) {
            if(type == 'month') {
                var $key =  $scope.expenseReportChart.month.key;
                var $value = [{
                    data: $scope.expenseReportChart.month.value
                }];
            }
            if(type == 'year') {
                var $key =  $scope.yearKey;
                var $value = [{
                    data: $scope.expenseReportChart.year.value
                }];
            }

            if(type == 'week') {
                var $key =  $scope.expenseReportChart.week.key;
                var $value = [{
                    data: $scope.expenseReportChart.week.value
                }];
            }
            $scope.showChart('expenseReport', 'Expense Report', 'area', $key, $value);
            event.stopPropagation();
        }

        $scope.getProfitandLossChart = function () {
            dashboardServices.getProfitandLossChart().then(function (res) {
                $scope.profitandLossChart = res.response.report;
                var $value = $scope.profitandLossChart.year.value;
                $scope.showChart('profitandLossReport', 'Invoice and Expense Report', 'column', $scope.yearKey, $value)
            })
        }
        
        $scope.changeProfitLossChart = function (event, type) {
            if(type == 'month') {
                var $key =  $scope.profitandLossChart.month.key;
                var $value = $scope.profitandLossChart.month.value;
            }
            if(type == 'year') {
                var $key =  $scope.yearKey;
                var $value = $scope.profitandLossChart.year.value;
            }
            if(type == 'week') {
                var $key =  $scope.profitandLossChart.week.key;
                var $value = $scope.profitandLossChart.week.value;
            }
            $scope.showChart('profitandLossReport', 'Invoice and Expense Report', 'column', $key, $value)
            event.stopPropagation();
        }

        $scope.showChart = function (element, title, type, key, value) {

            if(type == 'area') {
                Highcharts.chart(element, {
                    chart: {
                        type: type
                    },
                    title: {
                        text: title
                    },
                    xAxis: {
                        categories: key,
                    },
                    tooltip: {
                        pointFormat: 'Expense Total <b>{point.y}</b>'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Amount'
                        },
                    },
                    series: value
                });
            } else {
                Highcharts.chart(element, {
                    chart: {
                        type: type
                    },
                    title: {
                        text: title
                    },
                    xAxis: {
                        categories: key,
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Amount'
                        },
                    },
                    series: value
                });
            }
        }
        
        $scope.getActivity = function () {
            dashboardServices.getActivity().then(function (res) {
               $scope.invoiceActivity = res.response.invoices;
               $scope.estimatesActivity = res.response.estimates;
               $scope.expensesActivity = res.response.expenses;
            });
        }

        $scope.getUpcomingPayment = function () {
            dashboardServices.getUpcomingPayment().then(function (res) {
               $scope.invoiceUpcomingPayment = res.response.invoices;
            });
        }
        
        $scope.changeActivity = function (type) {
            if(type == 'INVOICE') {
                $scope.activityInvoice = true;
                $scope.activityEstimates = false;
                $scope.activityExpense = false;
                $scope.showAllActivity = false;
            }

            if(type == 'ESTIMATES') {
                $scope.activityInvoice = false;
                $scope.activityEstimates = true;
                $scope.activityExpense = false;
                $scope.showAllActivity = false;
            }

            if(type == 'EXPENSES') {
                $scope.activityInvoice = false;
                $scope.activityEstimates = false;
                $scope.activityExpense = true;
                $scope.showAllActivity = false;
            }
        }

        $scope.getAllActivity = function () {
            $scope.showAllActivity = true;
            dashboardServices.getAllActivity().then(function (res) {
                $scope.invoiceActivity = res.response.invoices;
                $scope.estimatesActivity = res.response.estimates;
                $scope.expensesActivity = res.response.expenses;
            });
        }
        
    }]);