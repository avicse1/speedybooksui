'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Cheque Controller
 * @description
 * #chequeCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("chequeCtrl", ["$scope", "$window", "$filter", "$routeParams", "validator", "$modal", "chequeServices", "$cookieStore", "accountServices", "$location", "$ngBootbox", "userServices", function($scope, $window, $filter, $routeParams, validator, $modal, chequeServices, $cookieStore, accountServices, $location, $ngBootbox, userServices) {

        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);

        $scope.businessCheques = {};
        $scope.accounts = {};
        $scope.businessProfileId = $scope.globals.currentUser.businessProfile;
        $scope.cheque = {'form': 'cheque', 'date': new Date(), 'cheque_no' : '', 'status': 'APPROVED', 'payment_term': 'Paid', 'description': '', 'account_id': '', 'amount': '', 'vendor_id': '', 'item_account_id': ''};
        $scope.receivePaymentAccountLists = {};
        $scope.writeChequeShow = 0;
        $scope.writeChequePaidShow = 1;
        $scope.chequeAccountError = 0;

        // Cheques Details Listings Start
        chequeServices.getChequeList().then(function(res){
            $scope.businessCheques = res.response.businessCheques;
            $scope.search();
            $scope.select($scope.currentPage)
        });

        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.businessCheques, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.businessCheques, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];

        // Journal Details Listings End
        if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
            $scope.main.error_message = "Please complete your business profile before creating cheques.";
            window.location.href = "#/business_profile";
        } else {
            accountServices.getAccountList().then(function(res) {
                $scope.accounts = res.response.accounts;
            });

            accountServices.getParentAccountList().then(function (res) {
                $scope.parentAccountLists = res.response.parentAccounts;
            })

            userServices.getVendorsList().then(function(res) {
                $scope.users = res.response.vendors;
            });

            accountServices.getReceivePaymentAccountList().then(function (res) {
                $scope.receivePaymentAccountLists = res.response.receivePaymentAccountLists;
            })
        }


        $scope.cancel = function() {
            $scope.main.error_message = "";
            window.location.href = "#/dashboard";
        };

        $scope.next = function () {
            if($scope.cheque.account_id != '') {
                $scope.writeChequeShow = 1;
                $scope.writeChequePaidShow = 0;
                $scope.chequeAccountError = 0;
            } else {
                $scope.chequeAccountError = 1;
            }
        }

        $scope.back = function () {
            $scope.writeChequeShow = 0;
            $scope.writeChequePaidShow = 1;
        }

        $scope.saveCheque = function () {
            $scope.main.error_message = "";
            validator.checkValidation($scope.cheque).then(function(data) {
                if(Object.keys(data.error).length === 0) {
                    chequeServices.saveCheque($scope.cheque, $scope.businessProfileId).then(function(res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            var $id = res.response.businessCheque.id;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function(){
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.href = "#/check_details/" + $id;
                                    }, 500);
                                });
                        } else {
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    $scope.main.error_message = data.error;
                }
            });
        }

        // Get Journal Details
        $scope.getChequeDetails = function () {
            chequeServices.getChequeDetails($routeParams.id).then(function (res) {
                $scope.chequeDetail = res.response.businessCheque;
            })
        }

        //Delete Cheque
        $scope.delete = function (id) {
            chequeServices.delete(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = res.message;
                }
            })
        }

        //print write cheque
        $scope.saveChequePrint = function () {
            validator.checkValidation($scope.cheque).then(function(data) {
                if(Object.keys(data.error).length === 0) {
                    chequeServices.saveCheque($scope.cheque, $scope.businessProfileId).then(function(res)  {
                        if(res.code == 200) {
                            var $id = res.response.businessCheque.id;
                            window.location.href = "#/check_details/" + $id;
                            var innerContents = document.getElementById('cheque-print').innerHTML;
                            var popupWinindow = window.open('', '_blank', 'width=600,height=700,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                            popupWinindow.document.open();
                            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
                            popupWinindow.document.close();
                        } else {
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    $scope.main.error_message = data.error;
                }
            });
        }
    }]);
