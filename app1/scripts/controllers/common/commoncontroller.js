'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:userCtrl
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("commonCtrl", ["$scope", "userServices", "$location", function($scope, userServices, $location) {
    }])
    .controller("reportsCtrl", ["$scope", "userServices", "$location", "$cookieStore", function($scope, userServices, $location, $cookieStore) {
        $scope.oneAtATime = !0;
        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);

        $scope.groups = [{
            title: "Profit & Loss – Standard Report",
            content: "A standard  reporting option is the profit & loss report (P&L), which is also known as the income statement. This report provides details on income and expenses, answering one of the most important questions – is this business profitable or is it losing money?"
        }, {
            title: "Profit & Loss Class Report",
            content: "Where standard  reports for Profit & Loss provide an overall summary, the class report allows users to divide a business into segments (by department, office, location, or other meaningful groups) and view income statements for each each segment or division individually."
        }, {
            title: "Balance Sheet Standard Report",
            content: "The balance sheet report is also a popular  reporting tool. This tool subtracts liabilities from assets to show how much a business is worth."
        }, {
            title: "Balance Sheet Class Report",
            content: "Like the Profit & Loss Class Report, this  reporting option allows users to divide a business into segments and get a balance sheet for each segment. This report displays figures in columns so users can easily compare across classes."
        }, {
            title: "Statement of Cash Flows",
            content: "This report allows users to keep track of money coming in and going out. It breaks cash flow into cash from operating activities, investing activities, and financing activities."
        }, {
            title: "Open Invoices",
            content: "This  reporting option will show all charges and unpaid invoices. The information is grouped by customer and job."
        }, {
            title: "Customer Balance Detail",
            content: "Use this report to keep track of each customer’s account balance and to quickly find out how much customers owe."
        }, {
            title: "Accounts Receivable Aging Summary Report",
            content: "This reporting tool can help keep track of past due amounts. For each customer, the user will see the amount owed in the current billing period and any unpaid balance from previous billing periods."
        }, {
            title: "Vendor Balance Detail Report",
            content: "This reporting system makes it simple to keep track of all transactions with vendors. The report provides information for each vendor, showing current unpaid balances for each."
        }, {
            title: "Purchases by Vendor Summary Report",
            content: "This report allows users to view all purchases made from individual vendors, allowing users to track spending with each vendor."
        }, {
            title: "Unpaid Bills Detail Report",
            content: "This reporting option shows both the bills and the payments to each vendor. It helps users recognize when balances are unpaid and includes a special column that shows the number of days past due."
        }, {
            title: "Transaction Detail by Account",
            content: "This report can be used to keep track of each individual transaction processed by a business. Users can also get subtotals for each account."
        }, {
            title: "Payroll Summary",
            content: "This reporting tool allows users to keep track of employee payroll. For each employee, this report will display total wages, taxes, deductions, and other important figures as specified."
        }];

        $scope.items = ["Item 1", "Item 2", "Item 3"], $scope.addItem = function() {
            var newItemNo;
            newItemNo = $scope.items.length + 1, $scope.items.push("Item " + newItemNo)
        }
}])