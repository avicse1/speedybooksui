var app = angular.module('speedyBooksDetails', ["ngRoute"])
var webServerUrl = "http://api.speedybooks.scriptninja.in/";
app.controller("detailCtrl", ["$scope", "$route", "$routeParams", "$location", "$http", function($scope, $route, $routeParams, $location, $http) {
    $scope.param = $location.search().id;
    $scope.invoice = {};
    $scope.customer = {};
    $scope.getInvoice = function () {
        $http({
            url: webServerUrl + 'invoices/details/'+ $scope.param,
            method: "GET",
            dataType: "json",
            withCredentials: false,
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Accept' : 'application/vnd.speedybooks.v1+json'
            }
        }).
        success(function(data){
            $scope.invoice = data.response.businessInvoice;
            $scope.customer = $scope.invoice.customer[0];
        });
    }

}]);