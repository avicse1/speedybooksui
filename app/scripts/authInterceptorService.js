﻿'use strict';

angular.module(projectAppName)

.factory('authInterceptorService', ['$q', '$injector', '$rootScope', '$cookieStore', '$location', function ($q, $injector, $rootScope, $cookieStore, $location) {
    var authInterceptorServiceFactory = {};
    $rootScope.globals = $cookieStore.get('globals');
    var _request = function (config) {
        if(config.dataType != 'json') {
            $('#mask').show();
        }
        config.headers = config.headers || {};
        if($rootScope.globals != null) {
            var authData = $rootScope.globals.currentUser.authdata;
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData;
            }
        }
        return config;
    }

    var _responseError = function (rejection) {
        $('#mask').hide();
        if (rejection.status === 401 || rejection.status === 500 || rejection.status === 0) {
            
            var authData = $rootScope.globals.currentUser.authdata;
            $location.path('/logout');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);