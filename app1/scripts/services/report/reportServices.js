'use strict';

angular.module(projectAppName)

    .factory('reportServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};

            service.cusomterInvoiceReport = function (invoice) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/customer/invoice?from=' + invoice.from + '&to=' + invoice.to + '&date_type=' + invoice.date_type + '&status=' + invoice.status + '&order_by=' + invoice.order_by + '&type=' + invoice.user_type + '&business_profile_id=' + invoice.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.saleByItems = function (items) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/sales-by/items?from=' + items.from + '&to=' + items.to + '&type=' + items.user_type + '&basis=' + items.basis + '&business_profile_id=' + items.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.profitandLoss = function (profitandLoss) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/profit-and-loss?from=' + profitandLoss.from + '&to=' + profitandLoss.to + '&type=' + profitandLoss.user_type + '&basis=' + profitandLoss.basis + '&business_profile_id=' + profitandLoss.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getBalanceSheet = function (balanceSheet) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/balance-sheet?from=' + balanceSheet.from + '&to=' + balanceSheet.to + '&type=' + balanceSheet.user_type + '&basis=' + balanceSheet.basis + '&business_profile_id=' + balanceSheet.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getBalanceSheetDetails = function (type, balanceSheet) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/balance-sheet-details/' + type +'?from=' + balanceSheet.from + '&to=' + balanceSheet.to + '&type=' + balanceSheet.user_type + '&basis=' + balanceSheet.basis + '&business_profile_id=' + balanceSheet.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }


            service.getJournalReport = function (journalReport) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/journal-report?from=' + journalReport.from + '&to=' + journalReport.to + '&type=' + journalReport.user_type + '&basis=' + journalReport.basis + '&business_profile_id=' + journalReport.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getAgedReceviables = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/aged-receivables',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getAgedPayables = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/aged-payables',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getTrailBalance = function (trailBalance) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/trail-balance?from=' + trailBalance.from + '&to=' + trailBalance.to + '&basis=' + trailBalance.basis +'&type=' + trailBalance.user_type + '&business_profile_id=' + trailBalance.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getAccountingTransaction = function (accountingTransaction) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/accounting-transaction?from=' + accountingTransaction.from + '&to=' + accountingTransaction.to + '&basis=' + accountingTransaction.basis + '&type=' + accountingTransaction.user_type + '&business_profile_id=' + accountingTransaction.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getInvoiceReceivables = function (invoiceReceivables) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/invoice-receivables?from=' + invoiceReceivables.from + '&to=' + invoiceReceivables.to + '&type=' + invoiceReceivables.user_type + '&business_profile_id=' + invoiceReceivables.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getTaxReport = function (taxReport) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/tax-report?from=' + taxReport.from + '&to=' + taxReport.to + '&basis=' + taxReport.basis + '&type=' + taxReport.user_type + '&business_profile_id=' + taxReport.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getCashSummary = function (cashSummary) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/cash-summary?from=' + cashSummary.from + '&to=' + cashSummary.to + '&type=' + cashSummary.user_type + '&business_profile_id=' + cashSummary.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getCashAccountReport = function (cashAccount) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/cash-account?from=' + cashAccount.from + '&to=' + cashAccount.to + '&type=' + cashAccount.user_type + '&business_profile_id=' + cashAccount.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getGeneralLedger = function (generalLedger) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/general-ledger?from=' + generalLedger.from + '&to=' + generalLedger.to + '&type=' + generalLedger.user_type + '&basis=' + generalLedger.basis + '&business_profile_id=' + generalLedger.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getCashFlow = function (cashFlow) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'report/cash-flow?from=' + cashFlow.from + '&to=' + cashFlow.to + '&type=' + cashFlow.user_type + '&basis=' + cashFlow.basis + '&business_profile_id=' + cashFlow.businessProfileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            return service;
        }])

