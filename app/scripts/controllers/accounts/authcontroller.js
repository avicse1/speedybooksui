"use strict";

angular.module(projectAppName)

.controller("AppCtrl", ["$scope", "$rootScope", "validator", "$window", "$cookieStore", "$location", "AuthenticationService", "commonServices", "$routeParams", "manageUserService",
    function($scope, $rootScope, validator, $window, $cookieStore, $location, AuthenticationService, commonServices, $routeParams, manageUserService) {
        $rootScope.globals = $cookieStore.get('globals');
        $scope.main = {
            brand: "Speedy Books",
            name: '',
            username: "",
            email: "",
            password: "",
            error_message: "",
            alert: ""
        };

        $scope.inviteUserPermission = ['Read Only', 'Basic', 'Intermediate', 'Advance'];

        $scope.checkUserLoggedIn = function(){
            if(!$scope.isSpecificPage() && $rootScope.globals == undefined) {
                $location.path('/signin');
            }
        };
                        
        $scope.topMenuItems = [{name:'Home', class:'', url:'/signin'},{name:'Small Business', class:'', url:'/'},{name:'Accountants', class:'', url:'/signup_accountant'},{name:'Free Trial', class:'', url:'/signup'},{name:'Login', class:'login btn btn-default', url:'/signin'}];
            
        $scope.getLocation = function() {
            //$ window.scrollTo(0, 0);
            if($location.path() != '/signin') {
                return $location.path();
            } else {
                return '';
            }
        }

        $scope.loggedIn = false;

        commonServices.getCountries().then(function(res) {
            $scope.countries = res;
        });

        if($rootScope.globals != undefined) {
            $scope.main.name = $rootScope.globals.currentUser.username;
            $scope.userPermission = $rootScope.globals.currentUser.userPermission;
        }

        $scope.$watch('signup.country_id', function(changed) {
            if(changed != undefined && changed != "") {
                $scope.states = null;
                $scope.cities = null;
                commonServices.getStates(changed).then(function(res) {
                    $scope.states = res;
                });
            }
        });

        $scope.$watch('signup.state_id', function(changed) {
            if(changed != undefined && changed != "") {
                $scope.cities = null;
                commonServices.getCities(changed).then(function(res) {
                    $scope.cities = res;
                });
            }
        });

        return $scope.isSpecificPage = function() {
            var path;
            return path = $location.path(), _.contains(["/404", "/createpassword", "/privacy-policy", "/signup_accountant", "/pages/500", "/pages/login", "/signin", "/pages/signin1", "/pages/signin2", "/signup", "/pages/signup1", "/pages/signup2", "/forgot", "/pages/lock-screen"], path)
        },

        $scope.hideSideNav = function() {
            var path;
            return path = $location.path(), _.contains(["/business_profile"], path)
        },

        $scope.signup = {},

        $scope.lang = "English",
            $scope.getFlag = function() {
            var lang;
            switch (lang = $scope.lang) {
                case "English":
                    return "flags-american";
                case "Español":
                    return "flags-spain";
                case "日本語":
                    return "flags-japan";
                case "中文":
                    return "flags-china";
                case "Deutsch":
                    return "flags-germany";
                case "français":
                    return "flags-france";
                case "Italiano":
                    return "flags-italy";
                case "Portugal":
                    return "flags-portugal";
                case "Русский язык":
                    return "flags-russia";
                case "한국어":
                    return "flags-korea"
            }
        },

        $scope.login = function() {
            $scope.main.error_message = '';
            $scope.main.alert = '';
            AuthenticationService.Login($scope.main.username, $scope.main.password).then(function(res) {
                if(res.code == 200) {
                    AuthenticationService.SetCredentials(res.response.user);
                    $scope.main.error_message = '';
                    $scope.main.alert = '';
                    $scope.loggedIn = true;
                    $scope.main.name = res.response.user.firstName + ' ' + res.response.user.lastName,
                    $('.username .text').text($scope.main.name);

                    if($rootScope.globals != undefined) {
                        $scope.userPermission = $rootScope.globals.currentUser.userPermission;
                    }

                    $scope.inviteUserPermissionShowHide($scope.userPermission);
                    if($scope.userPermission != null && $scope.userPermission.cpa_permission == 1) {
                        $location.path('/accountant_dashboard');
                    } else {
                        $location.path('/dashboard');
                    }
                } else {
                    $scope.main.error_message = res.message;
                }
            }, function(res) {
                $scope.main.error_message = res.message;
            });
        },

        $scope.logout = function() {
            $scope.loggedIn = false;
            AuthenticationService.ClearCredentials();
            $scope.clearFields();
            $location.path('/');
        },

        $scope.getUsername = function() {
            if($rootScope.globals != undefined) {
                $scope.main.name =  $rootScope.globals.currentUser.username;
            } else {
                $scope.main.name = '';
            }
        },

        $scope.getInviteUser = function () {
            if($location.search().token) {
                $scope.token = $location.search().token;
                AuthenticationService.getInviteUser($scope.token).then(function (res) {
                    if(res.code == 200) {
                        $scope.inviteUser = res.response.inviteUser;
                        $scope.signup.first_name = $scope.inviteUser.first_name;
                        $scope.signup.last_name = $scope.inviteUser.last_name;
                        $scope.signup.email = $scope.inviteUser.email;
                    }
                    if(res.code == 400) {
                        window.location.href = "#/404";
                    }
                })
            }
        },

        $scope.signupSubmit = function() {
            $scope.main.error_message = '';
            $scope.main.alert = '';
            AuthenticationService.SignUp($scope.signup).then(function(res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $location.path('/');
                } else {
                    if (res.name != undefined) {
                        $scope.main.error_message = res.name;
                    } else if(res.email != undefined) {
                        $scope.main.error_message = res.email;
                    } else if(res.message != undefined) {
                        $scope.main.error_message = res.message;
                    }
                }
            }, function(res) {
                $scope.main.error_message = res.message;
            });
        },

        $scope.sendForgotPasswordRequest = function() {
            $scope.main.error_message = '';
            $scope.main.alert = '';
            AuthenticationService.sendForgotPasswordRequest($scope.main.email).then(function(res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $location.path('/');
                } else {
                    $scope.main.error_message = res.message;
                }
            }, function(res){
                $scope.main.error_message = res.message;
            });
        },

        $scope.clearFields = function() {
            $rootScope.globals = $cookieStore.get('globals');
            if($rootScope.globals != null) {
                $rootScope.userLoggedIn = true;
                window.location.href = "#/dashboard";
            }
            $scope.main.username        = "";
            $scope.main.name            = "";
            $scope.main.email           = "";
            $scope.main.password        = "";
            $scope.main.error_message   = "";
            $scope.signup = {};
            $scope.signup.country_id = 0;
            $scope.states = null;
            $scope.cities = null;
        },

        $scope.inviteUserPermissionShowHide = function (userPermission) {
            if(userPermission != null) {
                $scope.invoicePermission = userPermission.invoice;
                $scope.expensePermission = userPermission.expense;
                $scope.reportsPermission = userPermission.reports;
                $scope.payrllPermission = userPermission.payroll;
                $scope.manageUserPermission = userPermission.manage_users;
                $scope.bankingPermission = userPermission.banking;
            }
        }
    }]).controller("NavCtrl", ["$scope", "$rootScope", "$cookieStore", function($scope, $rootScope, $cookieStore) {
            $rootScope.globals = $cookieStore.get('globals');
            $scope.inviteUserPermission = ['Read Only', 'Basic', 'Intermediate', 'Advance'];

            if($rootScope.globals != undefined) {
                $scope.userPermission = $rootScope.globals.currentUser.userPermission;
            }

    }]).controller("createPasswordCtrl", ["$scope", "$location", "AuthenticationService", function($scope, $location, AuthenticationService) {
        $scope.passwordToken = $location.search().token;
        $scope.passwordEmail = $location.search().email;
        $scope.main.error_message = '';
        $scope.main.alert = '';

        return $scope.createpassword = function() {
             AuthenticationService.createPassword($scope.main.password, $scope.passwordToken, $scope.passwordEmail).then(function(res) {
                if(res.code == 200) {
                    $location.search('token', null);
                    $location.search('email', null);
                    $scope.main.alert = res.message;
                    $location.path('/');
                } else {
                    $scope.main.error_message = res.message;
                }
             }, function(res){
                 $scope.main.error_message = res.message;
             });
        }
    }]);