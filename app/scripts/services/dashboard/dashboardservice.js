'use strict';

angular.module(projectAppName)
    .factory('dashboardServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};

            service.getTotalUsers = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'dashboard/users/count',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getInvoiceTotal = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'dashboard/invoices/count',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getExpenseReportChart = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'dashboard/expense/report',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getProfitandLossChart = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'dashboard/profit-loss/report',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getActivity = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'dashboard/recent/activity',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getAllActivity = function () {
                var deferred = $q.defer();

                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'dashboard/recent/activity?type=All' ,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            service.getUpcomingPayment = function () {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'dashboard/invoices/upcoming/payment',
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data) {
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            }

            return service;
    }]);

