'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:bankingCrtl
 * @description
 * #bankingCrtl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("bankingCtrl", ["$scope", "$window", "userServices", "$route", "$routeParams", "$modal", "$filter", "validator", "$cookieStore", "$location", "itemServices", "$ngBootbox", "accountServices", "bankingServices", "invoiceServices", "expenseServices", function($scope, $window, userServices, $route, $routeParams, $modal, $filter, validator, $cookieStore, $location, itemServices, $ngBootbox, accountServices, bankingServices, invoiceServices, expenseServices) {

        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);

        $scope.bankingTab = 1;
        $scope.banking = {'name': '', 'account_id': '', 'upload': ''};
        $scope.accountData = {'parent_id': '', 'name': '', 'code': '', 'description': ''};
        $scope.mapping = {'header': true, 'dateColumn': '', 'dateFormat':'', 'description': '', 'amountIn':1,};
        $scope.accountLists = {};
        $scope.mainAccountLists = {};
        $scope.subAccountLists = {};
        $scope.transactions = {};
        $scope.import = {};
        $scope.bankAccountDetail = {};
        $scope.bankAccountLists = {};
        $scope.bankAccount = {};
        $scope.account_name = '';
        $scope.bankTransaction = {};
        $scope.speedybooksBalance = 0;

        $scope.match = 1;
        $scope.inSpeedybooks = 0;

        if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
            $scope.main.error_message = "Please complete your business profile before creating invoice.";
            window.location.href = "#/business_profile";
        } else {
            accountServices.getReceivePaymentAccountList().then(function (res) {
                $scope.accountLists = res.response.receivePaymentAccountLists;
            })

            accountServices.getParentAccountList().then(function (res) {
                $scope.parentAccountLists = res.response.parentAccounts;
            });

            accountServices.getAccountList().then(function(res) {
                $scope.accounts = res.response.accounts;
            });

            invoiceServices.getDueInvoices().then(function (res) {
                $scope.dueInvoices = res.response.businessDueInvoices;
            })

            expenseServices.getDueExpenses().then(function (res) {
                $scope.dueExpenses = res.response.businessDueExpenses;
            })
        }

        $scope.getBankAccountList = function () {
            bankingServices.getBankAccountList().then(function (res) {
                $scope.bankAccountLists = res.response.bankAccount;
                $scope.bankTransaction = $scope.bankAccountLists.match;
                $scope.speedybooksBalance = res.response.balance;
                $scope.search();
                $scope.select($scope.currentPage)
            });
        }

        $scope.getBankAccountDetail = function (accountId, accountName) {
            $scope.account_name = accountName;
            bankingServices.getBankAccountDetail(accountId).then(function (res) {
                $scope.bankAccountLists = res.response.bankAccount;
                if($scope.inSpeedybooks == 1) {
                    $scope.bankTransaction = $scope.bankAccountLists.in_speedybooks;
                } else {
                    $scope.bankTransaction = $scope.bankAccountLists.match;
                }
                $scope.speedybooksBalance = res.response.balance;
                $scope.search();
                $scope.select($scope.currentPage);
            })
        }

        $scope.getSpeedybookTransaction = function () {
            $scope.bankTransaction = $scope.bankAccountLists.in_speedybooks;
            $scope.inSpeedybooks = 1;
            $scope.match = 0;
            $scope.search();
            $scope.select($scope.currentPage);
        }

        $scope.getMatchTransaction = function () {
            window.location.reload();
        }

        /* start data table code */
        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.bankTransaction, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.bankTransaction, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];
        /* end data table code */

        /*Start bank account form code*/

        $scope.uploadFile = function(files) {
            if(files != null) {
                $scope.banking.upload = files[0];
                $scope.banking.fileName = files[0].name;
                $scope.file = files[0];
            }
        };
        
        $scope.saveBanking = function () {
            bankingServices.saveBanking($scope.banking).then(function (res) {
                if(res.code == 200) {
                    $scope.bankAccount = res.response.bankAccount;
                    $scope.bankingTab = 2;
                }
            })
        }

        $scope.getTransactions = function () {
            bankingServices.getTransactions($scope.bankAccount, $scope.mapping).then(function (res) {
                if(res.code == 200) {
                    $scope.transactions = res.response.transactions;
                    $scope.bankingTab = 3;
                }
            })
        }

        $scope.saveTransactionData = function () {
            bankingServices.saveBankTransaction($scope.bankAccount.id, $scope.transactions).then(function (res) {
                if(res.code == 200) {
                    $ngBootbox.alert(res.message)
                        .then(function() {
                            setTimeout(function(){
                                //$window.scrollTo(0, 0);
                                window.location.href = "#/banking";
                            }, 500);
                        });
                }
            })

        }
        $scope.goBack = function () {
            if($scope.bankingTab == 2) {
                $scope.bankingTab = 1;
            }
            if($scope.bankingTab == 3) {
                $scope.bankingTab = 2;
            }
        }

        /*End bank account form code*/


        /* Change Transacion Account form */
        $scope.addtransaction = {'account_id' : '', 'description': ''};
        
        $scope.openTransactionAccount = function (id) {
            $scope.addtransaction.id = id;
            $('#addTransactionAccount').modal('show');
        }

        $scope.cancelTransactionAccount = function () {
            $('#addTransactionAccount').modal('hide');
        }
        
        $scope.addTransactionAccount = function () {
            $('#addTransactionAccount').modal('hide');
            bankingServices.changeTransactionAccount($scope.addtransaction).then(function (res) {
                if(res.code == 200) {
                    $ngBootbox.alert(res.message)
                        .then(function() {
                            setTimeout(function() {
                                window.location.reload();
                            }, 500);
                        });
                }
            })
        }
        /* Change Transacion Account form */

        /* Delete Bank account transaction */
        $scope.delete = function (id) {
            bankingServices.delete(id).then(function (res) {
                if(res.code == 200) {
                    $ngBootbox.alert(res.message)
                        .then(function() {
                            setTimeout(function() {
                                window.location.reload();
                            }, 500);
                        });
                }
            })
        }

        /* Undo Bank account transaction */
        $scope.undoTransaction = function ($id) {
            bankingServices.undoTransaction($id).then(function (res) {
                if(res.code == 200) {
                    $ngBootbox.alert(res.message)
                        .then(function() {
                            setTimeout(function() {
                                window.location.reload();
                            }, 500);
                        });
                }
            })
        }


        /* Invoice Receive payment Start*/
        $scope.invoice = {'id': '', 'transaction_id': '','payment_term':'','reference': '', 'date': '', 'payment_method': '', 'payment_method_reference': '', 'credit_card_no':''};
        $scope.emailError = 0;

        $scope.selectDueInvoice = function (invoiceId) {
            angular.forEach($scope.dueInvoices, function (invoice, key) {
                if(invoice.id == invoiceId) {
                    $scope.invoice.id = invoice.id;
                    $scope.invoice.date = invoice.date;
                    $scope.invoice.reference = invoice.reference;
                }
            })
        }

        $scope.markReceivePayment = function (transactionId) {
            $scope.invoice.transaction_id = transactionId;
            $('#dueInvoice').modal('show');
        }

        $scope.cancelDueInvoice = function () {
            $('#dueInvoice').modal('hide');
        }

        $scope.receivePayment = function () {
            if($scope.invoice.account_id == '' && $scope.invoice.account_id != undefined) {
                $scope.emailError = 1;
            } else {
                $scope.invoice.paymentTerm = 'Paid';
                $('#dueInvoice').modal('hide');
                bankingServices.receivePayment($scope.invoice).then(function (res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.reload();
                                }, 500);
                            });
                    } else {
                        $scope.main.error_message = res.message;
                    }
                })
            }
        }
        /* Invoice Receive payment End*/



        /* Expense Make payment Start*/
        $scope.expense = {'id': '', 'transaction_id': '', 'payment_term':'','reference': '', 'date': ''};
        $scope.emailError = 0;

        $scope.selectDueExpense = function (expenseId) {
            angular.forEach($scope.dueExpenses, function (expense, key) {
                if(expense.id == expenseId) {
                    $scope.expense.id = expense.id;
                    $scope.expense.date = expense.date;
                    $scope.expense.reference = expense.reference;
                }
            })
        }

        $scope.markMakePayment = function (transactionId) {
            $scope.expense.transaction_id = transactionId;
            $('#dueExpense').modal('show');
        }

        $scope.cancelDueExpense = function () {
            $('#dueExpense').modal('hide');
        }

        $scope.makePayment = function () {
            if($scope.expense.account_id == '' && $scope.expense.account_id != undefined) {
                $scope.emailError = 1;
            } else {
                $scope.expense.paymentTerm = 'Paid';
                $('#dueExpense').modal('hide');
                bankingServices.makePayment($scope.expense).then(function (res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.reload();
                                }, 500);
                            });
                    } else {
                        $scope.main.error_message = res.message;
                    }
                })
            }
        }
        /* Expense Make payment End*/


        /* Create Journal entry code start */
        $scope.journal = {'id': '', 'account_id': '', 'name': '', 'date': new Date(), 'description': '', 'amount': ''};
        $scope.journalError = 0;

        $scope.openJournalForm = function (transactionId, amount) {
            $scope.journal.id = transactionId;
            $scope.journal.amount = amount;
            $('#addJournal').modal('show');
        }

        $scope.cancelAddJournal = function () {
            $('#addJournal').modal('hide');
        }

        $scope.addJournal = function () {
            if($scope.journal.account_id == '' && $scope.journal.account_id != undefined) {
                $scope.journalError = 1;
            } else {
                $('#addJournal').modal('hide');
                bankingServices.addJournal($scope.journal).then(function (res) {
                    if(res.code == 200) {
                        $scope.main.alert = res.message;
                        $ngBootbox.alert($scope.main.alert)
                            .then(function() {
                                setTimeout(function(){
                                    $scope.main.alert = "";
                                    $scope.main.error_message = "";
                                    window.location.reload();
                                }, 500);
                            });
                    } else {
                        $scope.main.error_message = res.message;
                    }
                })
            }
        }
        /* Create Journal entry code End */

    }]);
