'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:inviteUserCtrl
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("inviteUserCtrl", ["$scope", "$modal", "$rootScope", "userServices", "$routeParams", "$cookieStore", "$location", "commonServices", "$filter", "manageUserService", "$ngBootbox", function($scope, $modal, $rootScope, userServices, $routeParams, $cookieStore, $location, commonServices, $filter, manageUserService, $ngBootbox) {
        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);

        $scope.inviteUserTab = 1;
        $scope.inviteUser = {'form': 'inviteUser', 'first_name': '', 'last_name': '', 'email': '', 'permissions': 'Read Only', 'invoice': '', 'expense': '', 'setting': '', 'reports': '', 'payroll': '', 'manageUsers': '', 'banking': '', 'message': '', 'cpa_permission': false};
        $scope.readOnlyDisabled = false;
        $scope.basicDisabled = false;
        $scope.intermediateDisabled = false;
        $scope.advanceDisabled = false;
        $scope.main.error_message = '';
        $scope.emailAddress = 0;
        
        $scope.validateEmail = function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        $scope.showEmailTab = function () {
            if($scope.inviteUser.firstName != '' && $scope.inviteUser.lastName != '' && $scope.inviteUser.email != '') {
                if ($scope.validateEmail($scope.inviteUser.email)) {
                    $scope.inviteUserTab = 2;
                } else {
                    $scope.emailAddress = 1;
                }
            }
        }

        $scope.getInviteUserList =function () {
            manageUserService.getInviteUserList().then(function (res) {
                $scope.inviteUsers = res.response.inviteUsers;
                $scope.search();
                $scope.select($scope.currentPage)
            })
        }


        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.inviteUsers, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.inviteUsers, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];
        
        $scope.addNewInviteUser = function () {
            manageUserService.addNewInviteUser($scope.inviteUser).then(function(res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function() {
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/user/manage";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            });
        }
        
        $scope.inviteUserInactive = function (id) {
            $ngBootbox.confirm('Are you sure whether you want to de-activate the access of this user?')
                .then(function() {
                    manageUserService.inviteUserInactive(id).then(function (res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function() {
                                        //$window.scrollTo(0, 0);
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.reload();
                                    }, 500);
                                });
                        } else {
                            //$window.scrollTo(0, 0);
                            $scope.main.error_message = res.message;
                        }
                    })
                },
                function() {

                });
        }
        
        $scope.resend = function (id) {
            $ngBootbox.confirm('Are you sure whether you want to resend invitation of this user?')
                .then(function() {
                    manageUserService.resendInvitation(id).then(function (res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function() {
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.reload();
                                    }, 500);
                                });
                        } else {
                            $scope.main.error_message = res.message;
                        }
                    })
                },
                function() {

                });
        }

        $scope.changePermission = function (type) {
            if(type == 'Basic') {
                $scope.inviteUser.intermediate_invoice = '';
                $scope.inviteUser.intermediate_expense = '';
                $scope.inviteUser.intermediate_reports = '';
                $scope.inviteUser.intermediate_payroll = '';
                $scope.inviteUser.advance_invoice = '';
                $scope.inviteUser.advance_expense = '';
                $scope.inviteUser.advance_reports = '';
                $scope.inviteUser.advance_payroll = '';
                $scope.inviteUser.advance_setting = '';
                $scope.inviteUser.advance_manageUsers = '';
                $scope.inviteUser.advance_banking = '';
            }

            if(type == 'Intermediate') {
                $scope.inviteUser.basic_invoice = '';
                $scope.inviteUser.basic_expense = '';
                $scope.inviteUser.basic_reports = '';
                $scope.inviteUser.advance_invoice = '';
                $scope.inviteUser.advance_expense = '';
                $scope.inviteUser.advance_reports = '';
                $scope.inviteUser.advance_payroll = '';
                $scope.inviteUser.advance_setting = '';
                $scope.inviteUser.advance_manageUsers = '';
                $scope.inviteUser.advance_banking = '';
            }

            if(type == 'Advance') {
                $scope.inviteUser.basic_invoice = '';
                $scope.inviteUser.basic_expense = '';
                $scope.inviteUser.basic_reports = '';
                $scope.inviteUser.intermediate_invoice = '';
                $scope.inviteUser.intermediate_expense = '';
                $scope.inviteUser.intermediate_reports = '';
                $scope.inviteUser.intermediate_payroll = '';
            }
        }
        
        $scope.getInviteUserDetails = function () {
            manageUserService.getInviteUserDetails($routeParams.id).then(function (res) {
                $scope.inviteUser = res.response.inviteUser;
                if($scope.inviteUser.permissions == 'Basic') {
                    $scope.inviteUser.basic_invoice = res.response.inviteUser.invoice;
                    $scope.inviteUser.basic_expense = res.response.inviteUser.expense;
                    $scope.inviteUser.basic_reports = res.response.inviteUser.reports;
                }

                if($scope.inviteUser.permissions == 'Intermediate') {
                    $scope.inviteUser.intermediate_invoice = res.response.inviteUser.invoice;
                    $scope.inviteUser.intermediate_expense = res.response.inviteUser.expense;
                    $scope.inviteUser.intermediate_reports = res.response.inviteUser.reports;
                    $scope.inviteUser.intermediate_payroll = res.response.inviteUser.payroll;
                }

                if($scope.inviteUser.permissions == 'Advance') {
                    $scope.inviteUser.advance_invoice = res.response.inviteUser.invoice;
                    $scope.inviteUser.advance_expense = res.response.inviteUser.expense;
                    $scope.inviteUser.advance_reports = res.response.inviteUser.reports;
                    $scope.inviteUser.advance_payroll = res.response.inviteUser.payroll;
                    $scope.inviteUser.advance_setting = res.response.inviteUser.setting;
                    $scope.inviteUser.advance_manageUsers = res.response.inviteUser.manage_users;
                    $scope.inviteUser.advance_banking = res.response.inviteUser.banking;
                }

                if($scope.inviteUser.cpa_permission == 1) {
                    $scope.inviteUser.basic_invoice = '';
                    $scope.inviteUser.basic_expense = '';
                    $scope.inviteUser.basic_reports = '';
                    $scope.inviteUser.intermediate_invoice = '';
                    $scope.inviteUser.intermediate_expense = '';
                    $scope.inviteUser.intermediate_reports = '';
                    $scope.inviteUser.intermediate_payroll = '';
                    $scope.inviteUser.advance_invoice = '';
                    $scope.inviteUser.advance_expense = '';
                    $scope.inviteUser.advance_reports = '';
                    $scope.inviteUser.advance_payroll = '';
                    $scope.inviteUser.advance_setting = '';
                    $scope.inviteUser.advance_manageUsers = '';
                    $scope.inviteUser.advance_banking = '';
                }
            })
        }
        
        $scope.update = function () {
            manageUserService.updateInviteUser($scope.inviteUser).then(function(res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function() {
                                //$window.scrollTo(0, 0);
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.href = "#/user/manage";
                            }, 500);
                        });
                } else {
                    //$window.scrollTo(0, 0);
                    $scope.main.error_message = res.message;
                }
            });
        }

    }]);

