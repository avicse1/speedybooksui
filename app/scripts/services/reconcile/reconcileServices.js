/**
 * Created by root on 5/4/17.
 */
'use strict';

angular.module(projectAppName)
    .factory('reconcileServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
        function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
            $rootScope.globals = $cookieStore.get('globals');
            $rootScope.userLoggedIn = false;
            var service = {};

            service.getReconcileList = function(accountId) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reconcile?account_id=' + accountId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };


            service.saveReconcile = function (reconcileData, checkedPaymentData, depostisData, businessProfileId) {
                $('#mask').show();
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reconcile',
                        method: "POST",
                        data: { business_profile_id: businessProfileId, account_id: reconcileData.account_id, ending_date: reconcileData.endingDate, ending_balance:reconcileData.endingBalance,
                                auto_adjustment:reconcileData.auto_adjustment, checkPaymentData:checkedPaymentData, depostisData:depostisData,
                                beginning_balance:reconcileData.beginningBalance, cheque_payments:reconcileData.chequePayments, deposite_payments:reconcileData.depositsPayments
                        },
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getReconcile = function(reconcile) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reconcile/account/' + reconcile.account_id,
                        method: "POST",
                        data: { ending_date: reconcile.endingDate, ending_balance:reconcile.endingBalance,
                                serviceCharge:reconcile.serviceCharge, serviceChargeDate:reconcile.serviceChargeDate, serviceChargeAccount:reconcile.serviceChargeAccount,
                                interestEarned:reconcile.interestEarned, interestEarnedDate:reconcile.interestEarnedDate, interestEarnedAccount:reconcile.interestEarnedAccount
                        },
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getReconcileLatest = function(accountId) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reconcile/latest/' + accountId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            service.getReconcileData = function(reconcileId) {
                var deferred = $q.defer();
                if($rootScope.globals != null) {
                    $http({
                        url: webServerUrl + 'business/reconcile/' + reconcileId,
                        method: "GET",
                        dataType: "json",
                        withCredentials: false,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                            'Accept' : 'application/vnd.speedybooks.v1+json'
                        }
                    }).
                    success(function(data){
                        $('#mask').hide();
                        deferred.resolve(data);
                    });
                } else {
                    $rootScope.userLoggedIn = false;
                    window.location.href = "#/logout";
                }
                return deferred.promise;
            };

            return service;
        }])

