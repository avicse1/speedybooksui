'use strict';

/**
 * @ngdoc function
 * @name speedyBooksApp.controller:Taxrates Controller
 * @description
 * #userCtrl
 * Controller of the speedyBooksApp
 */
angular.module(projectAppName)
    .controller("taxRatesCtrl", ["$scope", "$window", "$filter", "$routeParams", "validator", "$modal", "$cookieStore", "$location", "$ngBootbox", "taxRateServices", function($scope, $window, $filter, $routeParams, validator, $modal, $cookieStore, $location, $ngBootbox, taxRateServices) {

        $scope.globals = $cookieStore.get('globals');
        $('.username .text').text($scope.globals.currentUser.username);
        $scope.taxRates = {};
        $scope.taxRateData = {'name': '', 'tax_rates':'', 'form': 'taxRate', 'component': ''};

        $scope.businessProfileId = $scope.globals.currentUser.businessProfile;

        if($scope.globals.currentUser.businessStatus == "PENDING" || $scope.globals.currentUser.businessStatus == null) {
            $scope.main.error_message = "Please complete your business profile before creating invoice.";
            window.location.href = "#/business_profile";
        } else {
            taxRateServices.getTaxRatesList().then(function (res) {
                $scope.taxRates = res.response.taxRates;
                $scope.search();
                $scope.select($scope.currentPage);
            })
        }

        $scope.onFilterChange = function() {
            return $scope.select(1), $scope.currentPage = 1, $scope.row = ""
        };
        $scope.onNumPerPageChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.onOrderChange = function() {
            return $scope.select(1), $scope.currentPage = 1
        };
        $scope.search = function() {
            return $scope.filteredStores = $filter("filter")($scope.taxRates, $scope.searchKeywords), $scope.onFilterChange()
        };
        $scope.order = function(rowName) {
            return $scope.row !== rowName ? ($scope.row = rowName, $scope.filteredStores = $filter("orderBy")($scope.taxRates, rowName), $scope.onOrderChange()) : void 0
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.currentPageStores = $scope.filteredStores.slice(start, end);
        }

        $scope.numPerPageOpt = [3, 5, 10, 20];
        $scope.numPerPage = $scope.numPerPageOpt[2];
        $scope.currentPage = 1;
        $scope.currentPageStores = [];


        $scope.saveTaxRate = function () {
            $scope.main.error_message = "";
            validator.checkValidation($scope.taxRateData).then(function(data) {
                if(Object.keys(data.error).length === 0) {
                    $('#addtax').modal('toggle');
                    taxRateServices.saveTaxRate($scope.taxRateData).then(function(res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function(){
                                        //$window.scrollTo(0, 0);
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.reload();
                                    }, 500);
                                });
                        } else {
                            //$window.scrollTo(0, 0);
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    $scope.main.error_message = data.error;
                }
            });
        }

        $scope.editTaxRate = function (taxId) {
            taxRateServices.getTaxRateDetails(taxId).then(function (res) {
                $scope.taxRateData = res.response.taxRate;
                $scope.taxRateData.form = 'taxRate';
                $('#editTax').modal('show');
            });
        }

        $scope.updateTaxRate = function () {
            $scope.main.error_message = "";
            validator.checkValidation($scope.taxRateData).then(function(data) {
                if(Object.keys(data.error).length === 0) {
                    $('#editTax').modal('toggle');
                    taxRateServices.updateTaxRate($scope.taxRateData).then(function(res) {
                        if(res.code == 200) {
                            $scope.main.alert = res.message;
                            $ngBootbox.alert($scope.main.alert)
                                .then(function() {
                                    setTimeout(function() {
                                        //$window.scrollTo(0, 0);
                                        $scope.main.alert = "";
                                        $scope.main.error_message = "";
                                        window.location.reload();
                                    }, 500);
                                });
                        } else {
                            //$window.scrollTo(0, 0);
                            $scope.main.error_message = res.message;
                        }
                    });
                } else {
                    $scope.main.error_message = data.error;
                }
            });
        }
        
        $scope.delete = function (id) {
            $scope.main.error_message = "";
            taxRateServices.delete(id).then(function (res) {
                if(res.code == 200) {
                    $scope.main.alert = res.message;
                    $ngBootbox.alert($scope.main.alert)
                        .then(function() {
                            setTimeout(function(){
                                $scope.main.alert = "";
                                $scope.main.error_message = "";
                                window.location.reload();
                            }, 500);
                        });
                } else {
                    $scope.main.error_message = res.message;
                }
            })
        }

    }]);