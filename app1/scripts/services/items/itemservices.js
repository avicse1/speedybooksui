'use strict';

angular.module(projectAppName)

.factory('itemServices', ['Base64', '$http', '$q', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $q, $cookieStore, $rootScope, $timeout) {
    	var service = {};
        service.getItems = function () {
        	var deferred = $q.defer();
            $http({
                url: webServerUrl + 'business/items',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
        	 return deferred.promise;
        };

        service.getItem = function (itemId) {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'business/items/' + itemId,
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        service.addNewItem = function(itemData) {
            var globals = $cookieStore.get('globals');
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'business/items',
                method: "POST",
                data: { business_profile_id: globals.currentUser.businessProfile, name: itemData.name, item_code: itemData.item_code, amount: itemData.amount, description: itemData.description, unit: itemData.unit, selling_price: itemData.selling_price, account_id: itemData.account_id },
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            })
            .error(function(data) {
                deferred.reject(data);
            });
             return deferred.promise;
        };

        service.getItemDetails = function(ItemId) {
            var globals = $cookieStore.get('globals');
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'business/items/' + ItemId,
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            })
            .error(function(data) {
                deferred.reject(data);
            });
            return deferred.promise;
        };

        service.getPayItems = function () {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'business/payItems',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        service.getPayItemTypes = function () {
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'business/payItemType',
                method: "GET",
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            });
            return deferred.promise;
        };

        service.addNewPayItem = function(itemData) {
            var globals = $cookieStore.get('globals');
            var deferred = $q.defer();
            $http({
                url: webServerUrl + 'business/payItems',
                method: "POST",
                data: { business_pay_item_type_id:itemData.business_pay_item_type_id, account_id:itemData.account_id, name:itemData.name, description:itemData.description },
                dataType: "json",
                withCredentials: false,
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept' : 'application/vnd.speedybooks.v1+json'
                }
            }).
            success(function(data){
                $('#mask').hide();
                deferred.resolve(data);
            })
            .error(function(data) {
                deferred.reject(data);
            });
            return deferred.promise;
        };

        service.deleteItem = function (itemId) {
            $('#mask').show();
            var deferred = $q.defer();
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/items/' + itemId,
                    method: "DELETE",
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };

        service.updateItem = function (itemData) {
            $('#mask').show();
            var deferred = $q.defer();
            var globals = $cookieStore.get('globals');
            if($rootScope.globals != null) {
                $http({
                    url: webServerUrl + 'business/items/' + itemData.id,
                    method: "PUT",
                    data: { business_profile_id: globals.currentUser.businessProfile, name: itemData.name, item_code: itemData.item_code, amount: itemData.amount, description: itemData.description, unit: itemData.unit, selling_price: itemData.selling_price, account_id: itemData.account_id },
                    dataType: "json",
                    withCredentials: false,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'Accept' : 'application/vnd.speedybooks.v1+json'
                    }
                }).
                success(function(data){
                    $('#mask').hide();
                    deferred.resolve(data);
                });
            } else {
                $rootScope.userLoggedIn = false;
                window.location.href = "#/logout";
            }
            return deferred.promise;
        };
        return service;
        
 }])
 